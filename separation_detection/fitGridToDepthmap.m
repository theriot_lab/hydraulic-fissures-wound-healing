function smoothDepthmap = fitGridToDepthmap(depthmap,tilesize,overlap,smoothness,...
                                            doPlotMap)
%%% A wrapper for the gridfit function from Epitools (Heller et al 2016
%%% Dev. Cell). Takes in a depthmap (i.e. z-position of maximum intensity
%%% value) and returns a smoothed version. Where depthmap==0 are regions to
%%% ignore when fitting a smooth version.

[y,x] = find(depthmap > 0);
z = depthmap(depthmap > 0);

xnodes = 1:size(depthmap,2);
ynodes = 1:size(depthmap,1);

[smoothDepthmap,xg1,yg1] = gridfit(x,y,z,xnodes,ynodes,...
                                   'tilesize',tilesize,...
                                   'overlap',overlap,...
                                   'smoothness',smoothness,...
                                   'interp','bilinear',...
                                   'regularizer','springs');
  
if doPlotMap
    figure,surf(xg1,yg1,smoothDepthmap)
    zlim([0,max(smoothDepthmap(:))+20])
    shading interp
    colormap(jet(256));
    camlight right
    lighting phong
end
                               
