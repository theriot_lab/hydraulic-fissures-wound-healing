function product = multiplyInFourierDomain(im1,im2)
%%% Multiply two images in the Fourier domain. Converts to doubles
%%% if they are not already

im1 = double(im1);
im2 = double(im2);

product = real(fftshift(ifft2(fft2(im1).*fft2(im2))));

