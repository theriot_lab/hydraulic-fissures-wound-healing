function imFilt = getSharpBoundariesBilatFilt(im,sigmar,sigmas)
%%% Use bilateral filtering to extract regions with sharp changes. Subtract
%%% a gaussian fit to highlight those regions and suppress regions of slow
%%% variation

imBilateral = GPA(im,sigmar,sigmas,0.1,'Gauss');
gaussianProfile = fspecial('gaussian',size(im),sigmas);
imGauss = multiplyInFourierDomain(im,gaussianProfile);
imFilt = imBilateral - imGauss;