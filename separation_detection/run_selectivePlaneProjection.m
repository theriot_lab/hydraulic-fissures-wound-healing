fileName = ['/Users/andrewkennard/Data/2019-03-12_3dpf_Np63Gal4_UASEGFP_engGFP/',...
            '20190312_40x_ebgPositive_Wounding_1/',...
            '20190312_40x_ebgPositive_Wounding_1_MMStack_Pos0.ome.tif'];
reader = bfGetReader(fileName);

sizeZ = reader.getSizeZ();
sizeT = reader.getSizeT();
sizeX = reader.getSizeX();
sizeY = reader.getSizeY();

%Constants
iT = 5;
smoothingRadius = 2;
devianceThreshold = 0.5;
tileSize = 200;
overlap = 0.25;
smoothness1 = 200;
smoothness2 = 1;
maxDifference = 2;
projectedImageStack = zeros(sizeY,sizeX,sizeT);
for iT = 5
disp('Filtering stack...')
imStack = getImageStack(reader,1,iT);
smoothedStack = lowpassImageStack(imStack,smoothingRadius);

disp('Detect background pixels...')
[maxZ, depthmap] = max(smoothedStack,[],3);
isBackground = detectBackground(smoothedStack,devianceThreshold);

disp('Adaptive thresholding...')
%Fix the Z position of background regions to a single, low value
depthmap(isBackground) = min(depthmap(:));
isCloserSurface = adaptiveOtsuThreshold(depthmap,[8,8]);

depthmapCloserSurface = depthmap .* isCloserSurface;


disp('First grid fit...')
rigidDepthmap = fitGridToDepthmap(depthmapCloserSurface,...
                                  tileSize,...
                                  overlap,...
                                  smoothness1,...
                                  0);


residualDepthmap = abs(depthmapCloserSurface - rigidDepthmap);
residualDepthmap(depthmapCloserSurface==0) = 0;

wellFittingDepthmap = depthmapCloserSurface ...
                            .* (residualDepthmap < maxDifference);

disp('Second fitting...')
finalDepthmap = fitGridToDepthmap(wellFittingDepthmap,...
                                  tileSize,...
                                  overlap,...
                                  smoothness2,...
                                  1);
projectedImageStack(:,:,iT) = projectImageWithDepthmap(imStack,finalDepthmap);
end

%%
saveFileName = initAppendFile('selectiveProjection.tif');
for iT = 1:sizeT
    imwritemulti(uint16(projectedImageStack(:,:,iT)),saveFileName);
end