function isForeground = adaptiveOtsuThreshold(im,nBlocks)
%%% Split im into blocks specified by nBlocks [nBlocksRow,nBlocksCol]
%%% and perform Otsu thresholding in each block. 

%Check for datatype and scale image appropriately
if isa(im,'uint16')
    im = (1/(2^16-1))*double(im);
elseif isa(im,'uint32')
    im = (1/2^32-1) * double(im);
elseif isa(im,'double')
    im = (1/(max(im(:)) - min(im(:)))) * (im - min(im(:)));
elseif isa(im,'uint8')
    im = im;
else
    error('im must be of type uint8, uint16, uint32, or double');
end

%Make sure blockSize divides im appropriately
originalSize = size(im);
rowMismatch = mod(size(im,1),nBlocks(1));
colMismatch = mod(size(im,2),nBlocks(2));
if (rowMismatch > 0) && (rowMismatch <= 5)
    im = im(1:(end-rowMismatch),:);
elseif rowMismatch
    error(['rowMismatch > 5, choose a number of blocks (rows) that will evenly divide',...
           '%d'],size(im,1));
end
if (colMismatch > 0) && (colMismatch <=5)
    im = im(:,1:(end-colMismatch));
elseif colMismatch
    error(['colMismatch > 5, choose a number of blocks (cols) that will evenly divide',...
           '%d'],size(im,2));
end

blockSize = size(im) ./ nBlocks;

imAsColumns = im2col(im,blockSize,'distinct');
threshAsColumns = zeros(size(imAsColumns));

for k = 1:size(imAsColumns,2)
    thresh = graythresh(imAsColumns(:,k));
    threshAsColumns(:,k) = thresh;
end

threshAsImage = col2im(threshAsColumns,blockSize,size(im),'distinct');

isForeground = imbinarize(im,threshAsImage);

if any(size(isForeground) ~= originalSize)
    isForeground = padarray(isForeground,originalSize - size(im),'post');
end


    


