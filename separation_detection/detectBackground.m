function isBackground = detectBackground(imStack,devianceThreshold)
%%% Detect (x,y) pixels that are background in an image stack. Use the idea
%%% that background pixels will show low variation across the Z direction
%%% of the image stack (max will be close to median) whereas for signal
%%% pixels the max will be far away from the median
%%% imageStack is assumed to have dimensions (sizeY,sizeX,sizeZ)

maxZ = max(imStack,[],3);
medianZ = median(imStack,3);

devianceZ = (maxZ - medianZ) ./ medianZ;

isBackground = devianceZ < devianceThreshold;

