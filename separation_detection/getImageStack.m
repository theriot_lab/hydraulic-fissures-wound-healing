function imStack = getImageStack(reader,iC,iT)
%%% Read in one image stack (in one channel
%%% and one timepoint) so that the dimensions are (nY,nX,nZ). Keep 
%%% the same data type as the raw data.
sizeY = reader.getSizeY();
sizeX = reader.getSizeX();
sizeZ = reader.getSizeZ();

im1 = bf_getFrame(reader,1,iC,iT);

imStack = zeros(sizeY,sizeX,sizeZ,class(im1));

imStack(:,:,1) = im1;

for iZ = 2:sizeZ
    imStack(:,:,iZ) = bf_getFrame(reader,iZ,iC,iT);
end
