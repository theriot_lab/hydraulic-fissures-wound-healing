function smoothedStack = lowpassImageStack(imStack,sigma)
%%% Smooth an image stack with a gaussian lowpass filter with radius sigma
%%% (in the real domain)

[sizeY,sizeX,sizeZ] = size(imStack);
smoothedStack = zeros(sizeY,sizeX,sizeZ);

gaussianProfile = fspecial('gaussian',[sizeY,sizeX],sigma);

for iZ = 1:sizeZ
    smoothedStack(:,:,iZ) = multiplyInFourierDomain(imStack(:,:,iZ),...
                                                    gaussianProfile);
end
