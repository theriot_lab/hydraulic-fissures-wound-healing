# Separation detection
There are a number of scripts in here for historical convenience, and maybe to help if you need to tweak the main script. 
The selective plane projection code in this repository is out of date and does NOT use the SURFCUT approach. I will be 
uploading that code (which should be run on the GPU on Neuron) later on.

* _Input_
    - The csv file generated from the `drawAPAxis_selectRegionForRegistration_.ijm` Macro in the [`preprocessing` folder](../preprocessing)
    - Folders of 2D images, split by timepoints (if you did registration), OR
    - OME TIFF stacks (XYT) if you did not do registration.
* _Output_
    - `.tif` and `.mat` files that probably have `FiberMetric` in their name and are stored in an `output` folder, which contain the
result of the ridge detection filter applied to the image stack or image sequence.

## Instructions
1. Make sure you have previously run `drawAPAxis_selectRegionForRegistration_.ijm` to generate the appropriate csv file and other
good stuff. Registration is not required, but is also a good idea.
2. Installation prerequisites:
    - Make sure you have [BioFormats for MATLAB](https://docs.openmicroscopy.org/bio-formats/6.1.0/users/matlab/index.html) installed and in your path
    - Make sure the [`matlab_utilities`](../matlab_utilities) folder is also in your path.
2. Open `enrichSeparation.m` in MATLAB.
3. Check the initial settings in the first portion of the script.
4. Run the script!

## Tips for algorithm optimization
This script starts by calibrating the ridge detector on the last few
frames of the dataset (where in my experience separation is generally
most extensive) and then it applies those settings to the full dataset.
You may want to play around with that depending on your data setup.

Other parameters to tune for a new dataset:
* `fiberMetricQuantile`: the array of numbers is a set of expected
    widths for ridges. YOu could adjust this depending on the
    expected thickness of your ridges
* `HIGH_IN`: When converting from 16-bit to 8-bit, I currently do not
    take the max value (which can sometimes be a noisy pixel), but
    rather a quantile (the 99.9th percentile of pixel intensities I
    set to max, saturating the rest). THis is to provide a reasonable
    level of contrast in the 8-bit image. You may want to confirm for
    your data that this quantile is appropriate, i.e. that when you apply
    this approach you get a good looking dynamic range out, and you may want to
    lower the quantile if your image looks dim.
* [Bilateral filtering](https://en.wikipedia.org/wiki/Bilateral_filter): This can be used to preserve edges while
    blurring flatter regions of the image. Recently I have stopped
    using this additional step because it didn't seem to help much.
    But it may be useful for you! The filtering steps are all
    commented out, but can be added back in. If you do, you may want
    to try the filtering on a small cropped region and get some
    parameters that work (if you try on the full image it can take
    quite a while!). You want to optimize `sigmar`, which is the
    spatial extent of smoothing (larger `sigmar`: blurrier image) and
    `sigmas`, which indicates the tolerance for what counts as a large
    or small change in intensity (small changes are blurred, large
    are not). Larger `sigmas`: larger changes in intensity are blurred.
    Please look up bilateral filtering for more information!
*  If you are still not satisfied, you may also want to try a high-pass
    filter, which will throw away a lot of the slower changes in
    intensity. Ideally you would choose a filter size such that the
    differences in brightness between two neighboring cells would be
    minimized but the gap between those cells would still be
    preserved. Easiest way to implement a high-pass filter is by
    low-pass filtering and subtracting that from the original image.
    In practice you typically want a "band-pass filter," which will
    also remove very high frequency information (i.e. camera noise).
    The ["Difference of Gaussians"](https://en.wikipedia.org/wiki/Difference_of_Gaussians) is a good way to implement this.

**NOTE: Don't forget to change parameters BOTH in the calibration
step AND the step applied to the full image!** You may want to add some
variables to make sure these are matched across the two stages of the
algorithm.
