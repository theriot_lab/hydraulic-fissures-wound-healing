function [projectedImage,zOriginMap] = projectImageWithDepthmap(imStack,depthmap)
%%% Given an image stack (z is 3rd axis) and a depthmap, use the
%%% coordinates in the depthmap to assign each pixel in (x,y) the
%%% corresponding pixel in z.

[sizeY,sizeX,~] = size(imStack);

projectedImage = zeros(sizeY,sizeX,class(imStack));
zOriginMap = zeros(sizeY,sizeX,'uint8'); %up to 256 z positions

for y = 1:sizeY
    for x = 1:sizeX
        if (depthmap(y,x) > 0)
            zCoordinate = round(depthmap(y,x));
            zOriginMap(y,x) = zCoordinate;
            projectedImage(y,x) = imStack(y,x,zCoordinate);
        end
    end
end