function idxList = findTransitionPoints(kymograph)
%%% Look at each row in a kymograph and find the column at which the signal
%%% increases dramatically along that row.
%%% Input:
%%% - kymograph: 2D array of dimension [Y,X]. 
%%% Output: 
%%% - idxList: Column coordinates of the transition points for each row.
%%%   idxList(k) is NaN if a transition point is not found in the specified
%%%   window.
%%% The transition point for a row are defined as where the signal first attains
%%% half the dynamic range along that row (i.e. 0.5 * (rowmax - rowmin)).

%Find the maxima in each row
maxVal = max(kymograph, [], 2);
%Ignore the first few points in each row for the minimum calculation)
minVal = min(kymograph(:, 5:end-1), [], 2);
dynamicRange = maxVal - minVal;

nRows = size(kymograph, 1);
idxList = nan(nRows, 1);
for r = 1:nRows
    point = find((kymograph(r, :) - minVal(r)) > dynamicRange(r)/2, ...
                      1, 'first');
    %setting transition points for rows with missing data to nan
    if isempty(point)
        idxList(r) = nan;
    else
        idxList(r) = point;
    end
end
end