%% ==== Initial Setup ====
%%% Change these parameters to set for al the subsequent sections
run_initial_setup()
UM_PER_BIN = 5;
UM_PER_PIXEL = 0.3394;
kymographUseList = [1:20,24:65,68:78]; %Choose which kymographs in the dataset you want to process
                        % e.g. if you have 5 and want to skip the 4th, this
                        % variable should be [1 2 3 5] or [1:3 5]

%% ==== Gather manual input about kymographs for guided least squares fitting ====
%%% Use this code to input the valid data for least squares fitting

%%%-------INITIAL PARAMETERS AND SETTINGS----------
%Restrict analysis to a portion of the kymograph (essentially crop out the
%time range (t) and distance range (d) you are interested in
t = (0:0.5:44.5)'; %just pick the times you want
dbins = (0:70)'; %in units of bins, can change to microns later

kymographFile = '20220128_Cal8-12_Detect4-12_separationKymographs_woundAligned_.mat';

%Load the kymograph data from scriptMakeKymographs.m
load(kymographFile);

%%%-----------SCRIPT---------------

%Ask for initials of current user
userInits = inputdlg("What are your initials?","Record user");

%Set up to store the values of the parameters, and make the struct into an
%array of the appropriate size
paramsList = struct('slope',[], 'intercept', [], 'all_points', [],...
    'valid_for_fit', [], 'points_for_fitting', []);
paramsList(numel(kymographUseList), 1).slope = [];

%Set up grids to allow all kymographs to have the exact same timing for
%easy averaging
[T, Dbins] = meshgrid(t, dbins);

%Set up a random order for viewing kymographs
kymographPermutedUseList = randperm(numel(kymographUseList));

for kRand = 1:numel(kymographPermutedUseList)
    close all
    k = kymographPermutedUseList(kRand);
    kIdx = kymographUseList(k);
    kym = sepKymographList(kIdx).kymograph;
    %Interpolate the kymograph so they all have the same coordinates.
    kym = interp2(sepKymographList(kIdx).T, double(sepKymographList(kIdx).D),...
                  kym, T, UM_PER_BIN * Dbins, 'linear', nan);
    %Make a nice figure of transition points and kymograph for annotation
    kymPointsFig = displayPointsOnKymograph(kym, T, Dbins, UM_PER_BIN); 
    
    %Store all points
    points = [kymPointsFig.Children(1).XData', kymPointsFig.Children(1).YData'];
    paramsList(k).all_points = points;
    
    %Ask user whether there is a signal that is valid to fit
    question = sprintf('Is there a signal that is valid to fit? \nAnswer "No" if all transition points are near t=0');
    validityAnswer = questdlg(question, 'Check validity', 'Yes', 'No', 'Yes');
    switch validityAnswer
        case 'Yes'
            paramsList(k).valid_for_fit = true;
        case 'No'
            paramsList(k).valid_for_fit = false;
            continue
    end
  
    %Ask user for end of propagation
    kymPointsFig.Title.String = 'Click on the furthest point in the linear propagation (max on the y axis)';
    endOfPropagation = drawpoint(kymPointsFig);
    
    %Remove (set to NaN) points that are at higher d values than the end of propagation
    endDistance = endOfPropagation.Position(2);
    eliminate = find(points(:,2)>endDistance);
    pointsForFitting = points;
    pointsForFitting(eliminate,:) = nan;
    
    %call function to eliminateOutliers (Andrew)
    %Ask user to select outlier points to exclude from least squares
    %regression analysis
    kymPointsFig.Title.String = 'Select outlier points with the brush tool';
    [kymPointsFig, outlierIdx] = markOutliers(kymPointsFig);
    pointsForFitting(outlierIdx,:) = nan;
    
    %Save the final list of points to be fit
    paramsList(k).points_for_fitting = pointsForFitting;
    
    %Perform least squares regression on the selected data points
    %%% The fit is performed "inverted," i.e. t ~ m*d + b, because for the "natural" model,
    %%% d ~ m*t + b, the data often fall along a vertical line, and therefore
    %%% are fit poorly
    linearModel = fitlm(pointsForFitting(:,2), pointsForFitting(:,1));

    %Save the info to the ParamsList struct
    invertedIntercept = linearModel.Coefficients.Estimate(1);
    invertedSlope = linearModel.Coefficients.Estimate(2);
    paramsList(k).slope = 1/invertedSlope;
    %Note: the "inverted" intercept is the t-intercept which is what we
    %want
    paramsList(k).intercept = invertedIntercept;
    
end

%save everything to a .mat file with user_inits
savename = sprintf('%s_separationParameters_%s.mat',datestr(now,'yyyymmdd'),userInits{1});
save(savename,'paramsList');
