%% ==== Initial Setup ====
%%% Change these parameters to set for al the subsequent sections
run_initial_setup()
UM_PER_BIN = 5;
UM_PER_PIXEL = 0.3394;
DO_PLOT = true; %set to false if you want to disable some of the diagnostic plots
kymographUseList = [2:4 6:8 10:12 16 20 21]; %Choose which kymographs in the dataset you want to process
                        % e.g. if you have 5 and want to skip the 4th, this
                        % variable should be [1 2 3 5] or [1:3 5]
rac1_inhib = [1:3, 5:8, 21];
                        
%% ==== Calculate Kymographs ====
%%% This script takes in a series of ridge detection images and computes a
%%% kymograph from them. For this I also defined a csv file that contained
%%% the coordinates of a line I wished to use to define the axis of the
%%% fish, along which I woZuld average the signal from the ridge detection
%%% filter to collapse the data into 1 spatial dimension (and time).

%%% This script assumes that you have saved your data according to the
%%% previous script (separationDetection), so that there is a .mat file
%%% containing the ridge detection result with the suffix
%%% '_FiberMetric.mat' in the appropriate location.

%%% --- INITIAL SETTINGS AND PARAMETERS ---
%Location of the input file with info about the dataset
inputFile = ['/Users/enorby/Thesis/Analysis/InputOutput/',...
             '20210914_Annotations_combined_fix_anteror_wound.csv'];
tvec = 0:.5:44.5; %Vector of time in minutes. May be different for your data!
tvec2 = 0:.5:44.5; %Vector of time in minutes. May be different for your data!
tvec1 = 0:1:29;
inputData = readtable(inputFile);

%Define a struct that will contain a kymograph for each dataset as well as
%other useful info.
sepKymographList = struct('kymograph',[],'T',[],'D',[],'datasetName',[], 'fibermetric_sum', []);
sepKymographList(size(inputData,1)).kymograph = [];
if DO_PLOT
    figure;
    hold on
end
for k = 1:size(inputData,1)
    close all
    %{
    if find(1:8==k)
        tvec = tvec1;
    else
        tvec = tvec2;
    end
    %}
    %Locate and load the ridge detection result, saved by separationDetection.m
    [parentDir,pathName] = fileparts(inputData.pathToImage{k});
    consensusArraySaveName = sprintf('%s_FiberMetric.mat',pathName);
    binarySaveName = sprintf('%s_BinaryCropped%s.tif',pathName, datestr(now,'yyyymmdd'));
    %consensusArraySavePath = fullfile(parentDir,'Cal18-28_Detect8-36_output',consensusArraySaveName);
    consensusArraySavePath = fullfile(parentDir,'output_Cal18-28_Detect8-36',consensusArraySaveName);
    binaryImSavePath = fullfile(parentDir,'Cal18-28_Detect8-36_output',binarySaveName);
    load(consensusArraySavePath,'consensusStack');
    %Also locate the original images that were used to compute the ridge
    %detection result - EL edited
    pathToReg = sprintf('%s_%s',inputData.pathToImage{k}(1:end-4),inputData.registration_outputSuffix{k});
    fileList = dir([pathToReg '/*.tif']);
    fileList = {fileList.name}; %get just the filenames into a cell array

    %Load the original stack. Any areas that are 0 (due to image shift
    %during registration) should not be used in the kymograph (set to nan)
    %imStack = loadImageStackFromFileList(inputData.pathToImage{k}, fileList);
    imStack = loadImageStack(pathToReg); %replaced the line above, EL
    consensusStack(imStack==0) = nan;
        
    %ask user to crop between lacerations
    figure(1)
    ax1 = imagesc(imStack(:,:,end));
    rectangle = drawrectangle;
    box = rectangle.Position;
    mask = nan(size(consensusStack));
    mask(box(2):box(2)+box(4)-1,box(1):box(1)+box(3)-1,:) = 1;
    consensusStack = consensusStack.*mask;
    

    binaryStack = zeros(size(consensusStack));
    threshold = graythresh(consensusStack);
    
    for frame = 1:size(consensusStack,3)
        %threshold = graythresh(consensusStack(:,:,frame));
        binaryStack(:,:,frame) = imbinarize(consensusStack(:,:,frame), threshold);
    end
    
    options.overwrite = true;
    saveastiff(binaryStack, binaryImSavePath, options);
    
    %get the dimensions of a single frame in this stack
    sizeSlice = [size(consensusStack, 1), size(consensusStack, 2)];

    %These coordinates define the line to be used for binning the kymograph
    x1 = inputData.apaxis_x0(k);
    y1 = inputData.apaxis_y0(k);
    
    %x0 = inputData.apaxis_x1(k);
    %y0 = inputData.apaxis_y1(k);
    x_p = inputData.apaxis_x1(k);
    y_p = inputData.apaxis_y1(k);
    ap_slope = (y_p - y1)/(x_p - x1);
    ap_intercept = y1 - ap_slope*x1;
    
    x0 = inputData.wound_anterior_x(k);
    y0 = round(ap_slope*x0 + ap_intercept);
    
    %Convert pixel indexes to the indexes of the bin that pixel is in (based 
    %(on the line you specified)
    micronBinsArray = calculateDistanceAlongLine(sizeSlice, x0, y0, x1, y1, ...
                                                 UM_PER_PIXEL,UM_PER_BIN);
    kymograph = calculateKymographFromBins(consensusStack,...
                                                        micronBinsArray);
    
    %Restrict to the part that is actually covered by this specific dataset
    %sizeT is the number of frames in the ridge detection stack
    sizeT = numel(fileList);
    %initialT is the frame corresponding to 0mpw. It is 0-indexed
    initialT = inputData.firstTrueFrame(k);
    %startT is the first frame (post-wounding) which was actually tracked
    %in the algorithm (some early frames may be dropped because the fish
    %was in the wrong position). This is 0-indexed
    startT = inputData.firstTrackedFrame(k);
    t = tvec((startT - initialT + 1) : (sizeT + (startT - initialT)));
    d = 0 : UM_PER_BIN : (UM_PER_BIN * (max(micronBinsArray(:)) - 1));
    %Create 2D arrays with the time and space positions of each point in
    %the kymograph
    [T, D] = meshgrid(t, d);
    %Save the data to the struct
    sepKymographList(k).kymograph = kymograph;
    sepKymographList(k).T = T;
    sepKymographList(k).D = D;
    sepKymographList(k).datasetName = consensusArraySaveName;
    sepKymographList(k).fibermetric_sum = nansum(nansum(consensusStack));
    sepKymographList(k).fibermetric_mean = nanmean(nanmean(consensusStack));
    sepKymographList(k).binary_sum = nansum(nansum(binaryStack));
    sepKymographList(k).binary_mean = nanmean(nanmean(binaryStack));
    
    if DO_PLOT
        figure(2)
        p = numSubplots(size(inputData, 1));
        subplot(p(1), p(2), k)
        hold on
        surf(T, D, kymograph), view(2)
        ax = gca;
        colormap(ax, magma);
        caxis([0 .11])
        shading interp
        %Display part of the save name as the title, you may want to change
        %which subset of the string is displayed. Intepreter none makes
        %sure that underscores don't cause subscripting
        %title(consensusArraySaveName(1:end-35), 'Interpreter', 'none');
        title(inputData.label(k), 'Interpreter', 'none');
        %Plot transition points on top
        idxList = findTransitionPoints(kymograph);
        idxList_toplot = idxList(~isnan(idxList));
        d_toplot = d(~isnan(idxList));
        %plot3(t(idxList_toplot), d_toplot,50*ones(numel(d_toplot)),'c.','MarkerSize',20),view(2)
    end
end
%%
%Save the data
savename = sprintf('%s_separationKymographs.mat',datestr(now,'yyyymmdd'));
save(savename,'sepKymographList');
%%
close all

figure
for k = kymographUseList
    means = squeeze(sepKymographList(k).fibermetric_mean);
    sm_means = smooth(means);
    times = sepKymographList(k).T;
    if find(rac1_inhib == k)
        plot(times,sm_means, 'Color', [1,0,1]), hold on
    else
        plot(times,sm_means, 'Color', [0,0,0]), hold on
    end
end
xlabel('mpw')
ylabel('Smoothed Separation Index')
set(gca,'FontSize',18)

figure
for k = kymographUseList
    means = squeeze(sepKymographList(k).fibermetric_mean);
    sm_means = smooth(means);
    [amp,peak_ind] = max(sm_means);
    times = sepKymographList(k).T(1,:);
    time_offset = times(peak_ind);
    times_aligned = times-time_offset;
    if find(rac1_inhib == k)
        plot(times_aligned,sm_means, 'Color', [1,0,1]), hold on
    else
        plot(times_aligned,sm_means, 'Color', [0,0,0]), hold on
    end
end
xlabel('Minutes Relative to Peak')
ylabel('Aligned Separation Index')
set(gca,'FontSize',18)

figure
for k = kymographUseList
    if k == 22
        continue
    end
    means = squeeze(sepKymographList(k).fibermetric_mean);
    sm_means = smooth(means);
    [max_sep,peak_ind] = max(sm_means);
    min_sep = min(sm_means);
    norm_means = (sm_means-min_sep)./(max_sep-min_sep);
    times = sepKymographList(k).T(1,:);
    time_offset = times(peak_ind);
    times_aligned = times-time_offset;
    if find(rac1_inhib == k)
        plot(times_aligned,norm_means, 'Color', [1,0,1]), hold on
    else
        plot(times_aligned,norm_means, 'Color', [0,0,0]), hold on
    end
end
xlabel('Minutes Relative to Peak')
ylabel('Aligned & Normalized Separation Index')
set(gca,'FontSize',18)


%% Plot extracted parameters
labels = {'control', 'rac1_inhib'};
fit_parameters = nan(2, 20);

figure
colors = magma(10);
xx = (rand(1,10)-0.5)/3;
scatter(xx+1,fit_parameter(1,:), 30, 'filled', 'MarkerFaceColor', colors(4,:)), hold on
xx = (rand(1,10)-0.5)/3;
scatter(xx+3,fit_parameter(2,:), 30, 'filled', 'MarkerFaceColor', colors(8,:)), hold on
xticks([1 3])
ylim([0,1])
xlim([0 4])
ylabel('Something I fit from the curve')
set(gca, 'XTickLabel', labels); % set x-axis labels
set(gca,'FontSize',18)

%%
close all
figure
for k = kymographUseList
    means = squeeze(sepKymographList(k).binary_mean);
    sm_means = smooth(means);
    times = sepKymographList(k).T;
    if find(rac1_inhib == k)
        plot(times,sm_means, 'Color', [1,0,1]), hold on
    else
        plot(times,sm_means, 'Color', [0,0,0]), hold on
    end
end
xlabel('mpw')
ylabel('Smoothed Separation Area')
set(gca,'FontSize',18)

figure
for k = kymographUseList
    means = squeeze(sepKymographList(k).binary_mean);
    sm_means = smooth(means);
    [amp,peak_ind] = max(sm_means);
    times = sepKymographList(k).T;
    time_offset = times(1,peak_ind);
    times_aligned = times-time_offset;
    if find(rac1_inhib == k)
        plot(times_aligned,sm_means, 'Color', [1,0,1]), hold on
    else
        plot(times_aligned,sm_means, 'Color', [0,0,0]), hold on
    end
end
xlabel('Minutes Relative to Peak')
ylabel('Aligned Separation Area')
set(gca,'FontSize',18)

figure
for k = kymographUseList
end
xlabel('mpw')
ylabel('Smoothed Separation Area')
set(gca,'FontSize',18)

figure
for k = kymographUseList
    means = squeeze(sepKymographList(k).binary_mean);
    sm_means = smooth(means);
    [amp,peak_ind] = max(sm_means);
    times = sepKymographList(k).T;
    time_offset = times(1,peak_ind);
    times_aligned = times-time_offset;
    if find(rac1_inhib == k)
        plot(times_aligned,sm_means, 'Color', [1,0,1]), hold on
    else
        plot(times_aligned,sm_means, 'Color', [0,0,0]), hold on
    end
end
xlabel('Minutes Relative to Peak')
ylabel('Aligned Separation Area')
set(gca,'FontSize',18)

figure
for k = kymographUseList
    means = squeeze(sepKymographList(k).binary_mean);
    sm_means = smooth(means);
    [amp,peak_ind] = max(sm_means);
    norm_means = sm_means./amp;
    times = sepKymographList(k).T;
    time_offset = times(1,peak_ind);
    times_aligned = times-time_offset;
    if find(rac1_inhib == k)
        plot(times_aligned,norm_means, 'Color', [1,0,1]), hold on
    else
        plot(times_aligned,norm_means, 'Color', [0,0,0]), hold on
    end

end
xlabel('Minutes Relative to Peak')
ylabel('Aligned & Normalized Separation Area')
set(gca,'FontSize',18)

