function stdR = propagateErrorOfRatio(A,B)
%%% Propagate the error for the ratio of two quantities, R = A/B.
%%% A and B are 1D arrays of the same length
%%% Return stdR, the standard deviation of the ratio A/B
assert(isvector(A),'numerator must be a vector');
assert(isvector(B),'denominator must be a vector');
stdA = std(A);
stdB = std(B);
covAB = stdA * stdB * corr(A,B);
covAB = covAB(1,2);

stdR = abs(mean(A ./ B)) * sqrt((stdA/mean(A)).^2 + (stdB/mean(B)).^2 ...
                              - 2 * (covAB / (mean(A) * mean(B))));