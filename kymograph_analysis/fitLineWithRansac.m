function [fittedParams, modeInliers] = fitLineWithRansac(x, y, nReps)
%%% Use the RANSAC procedure to fit a line to noisy data, robust to
%%% outliers. 
%%% Input: 
%%% - x,y: noisy data to fit. Must be arrays of the same dimensions
%%% - nReps: number of times to run the RANSAC procedure; because it is not
%%%     deterministic, you can get more reliable results by running the
%%%     algorithm multiple times and taking the mode of the resulting answers
%%% Output:
%%% - modelInliers: a 2-element array with the resulting parameters
%%% - modeInliers: return the indexes of points considered inliers in the
%%%   modal run of the RANSAC algorithm. IT's a boolean array (1 or 0 for
%%%   inlier or not inlier)


%Initialize some RANSAC parameters
SAMPLE_SIZE = 5; %subsample to take for each RANSAC trial
MAX_DISTANCE = 10; %distance of outliers from consensus line

assert(numel(x) == numel(y), 'x and y must have the same number of elements');
%Make sure the dimensions are correct
if (size(x, 1) == 1); x = x'; end
if (size(y, 1) == 1); y = y'; end

points = [x, y];
%Specify the line fitting function to use for RANSAC
fitLineFcn = @(points) polyfit(points(:, 1), points(:, 2), 1);
%Specify the function to evaluate residuals (just sum of squares)
evalLineFcn = ...
    @(model, points) sum((points(:, 2) - polyval(model, points(:, 1))).^2, 2);

%Run RANSAC several times
fittedParamsList = zeros(nReps,2);
inlierIdxList = false(size(points, 1), nReps);
for rep = 1:nReps
    %RANSAC identifies inlier points
    [~, inlierIdx] = ransac(points, fitLineFcn, evalLineFcn, SAMPLE_SIZE,...
                            MAX_DISTANCE);
    %Store these for later plotting
    inlierIdxList(:, rep) = inlierIdx;
    %Use same fitting function on the inliers identified by RANSA
    fittedParamsList(rep, :) = polyfit(points(inlierIdx, 1), ...
                                      points(inlierIdx, 2), ...
                                      1);
end
%Take the mode of the returned parameters. In case of multiple modes,
%choose the mode for the slope and make sure the intercept matches the
%chosen slope
modeSlope = mode(fittedParamsList(:, 1));
modeIdx = find(fittedParamsList(:, 1) == modeSlope, 1);
modeIntercept = fittedParamsList(modeIdx, 2);
%Find the points that match the mode
modeInliers = inlierIdxList(:,modeIdx);
fittedParams = [modeSlope, modeIntercept];
