%% ==== Initial Setup ====
%%% Change these parameters to set for al the subsequent sections
run_initial_setup()
UM_PER_BIN = 5;
UM_PER_PIXEL = 0.3394;
DO_PLOT = true; %set to false if you want to disable some of the diagnostic plots
kymographUseList = [1:20,24:28, 30:42,44:65,68:82]; %just removed kymograph 43 becuase it cuts off early %also kymograph 29 because it artefactually gains separation index at the end. see notebook.
kymographFile = '20220209_Cal18-28_Detect8-36_separationKymographs_woundAligned_d-v-masked.mat';
load(kymographFile)

% assemble condition lists
kymographConditionList = {[]; []; []; []; []};
kymographConditionNames = {'E3', 'Sorb', 'DMSO', 'Blebb', 'RAC1inhib'};

for k = kymographUseList
    if ~isempty(sepKymographList(k).label)
        condition = sepKymographList(k).label;
        condition_idx = find(strcmp(kymographConditionNames,condition));
        kymographConditionList{condition_idx}(end+1) = k;
    end
end

e3_rac1_fish = [kymographConditionList{1} kymographConditionList{5}];
rac1_inhib_fish = kymographConditionList{5};


%% Assemble normalized vectors with various time alignments and interpolations
close all

T_mpw = 0:45;
T_aligned = -60:40;
T_interp = 0:.05:1;
rac1_sep_mpw = [];
ctrl_sep_mpw = [];
rac1_norm_sep_mpw = [];
ctrl_norm_sep_mpw = [];
rac1_norm_sep_aligned = [];
ctrl_norm_sep_aligned = [];
rac1_norm_sep_interp = [];
ctrl_norm_sep_interp = [];
rac1_norm_sep_mpw_rise = nan(size(rac1_inhib_fish,2),45);
ctrl_norm_sep_mpw_rise = nan(size(e3_rac1_fish,2)-size(rac1_inhib_fish,2),45);
rac1_n = 0;
ctrl_n = 0;

for k = e3_rac1_fish
    % Get raw data
    separations = squeeze(sepKymographList(k).fibermetric_mean);
    times = sepKymographList(k).T(1,:);
    % Interpolate to get same time vector
    sep_T = interp1(times,separations,T_mpw,'linear',NaN);
    % Find peak separation
    sm_seps = smooth(sep_T,'moving');
    [~,peak_ind] = max(sm_seps);
    % Normalize separation values 
    min_sep = min(sep_T);
    max_sep = max(sep_T);
    norm_sep = (sep_T-min_sep)./(max_sep-min_sep);

    % Align time vectors and interpolate separation values
    time_offset = T_mpw(peak_ind);
    T_peak_at_0 = T_mpw-time_offset;
    T_0_to_1 = T_mpw(1:peak_ind)./time_offset;
    sep_aligned = interp1(T_peak_at_0,norm_sep,T_aligned,'linear',NaN);
    sep_interp = interp1(T_0_to_1,norm_sep(1:peak_ind),T_interp,'linear',NaN);
        
    if find(rac1_inhib_fish == k)
        if isempty(rac1_norm_sep_mpw)
            rac1_sep_mpw = sep_T;
            rac1_norm_sep_mpw = norm_sep;
            rac1_norm_sep_aligned = sep_aligned;
            rac1_norm_sep_interp = sep_interp;
        else
            rac1_sep_mpw = vertcat(rac1_sep_mpw, sep_T);
            rac1_norm_sep_mpw = vertcat(rac1_norm_sep_mpw, norm_sep);
            rac1_norm_sep_aligned = vertcat(rac1_norm_sep_aligned, sep_aligned);
            rac1_norm_sep_interp = vertcat(rac1_norm_sep_interp,sep_interp);
        end
        rac1_n = rac1_n + 1;
        rac1_norm_sep_mpw_rise(rac1_n,1:peak_ind) = norm_sep(1:peak_ind);
    else
        if isempty(ctrl_norm_sep_mpw)
            ctrl_sep_mpw = sep_T;
            ctrl_norm_sep_mpw = norm_sep;
            ctrl_norm_sep_aligned = sep_aligned;
            ctrl_norm_sep_interp = sep_interp;
        else
            ctrl_sep_mpw = vertcat(ctrl_sep_mpw,sep_T);
            ctrl_norm_sep_mpw = vertcat(ctrl_norm_sep_mpw, norm_sep);
            ctrl_norm_sep_aligned = vertcat(ctrl_norm_sep_aligned,sep_aligned);
            ctrl_norm_sep_interp = vertcat(ctrl_norm_sep_interp,sep_interp);
        end
        ctrl_n = ctrl_n + 1;
        ctrl_norm_sep_mpw_rise(ctrl_n,1:peak_ind) = norm_sep(1:peak_ind);
    end
end

%% Plot the original time plots

close all
figure
h = zeros(1,ctrl_n+rac1_n);
h(1) = plot(T_mpw(1), ctrl_sep_mpw(1), 'Color', [0,0,0], 'LineWidth', 1, 'DisplayName', 'hypotonic'); hold on
h(2) = plot(T_mpw(1), rac1_sep_mpw(1), 'Color', [1,0,1], 'LineWidth', 1, 'DisplayName', 'Rac1 inhibitor');
h(3:ctrl_n+1) = plot(T_mpw, ctrl_sep_mpw(2:end,:), 'Color', [0,0,0], 'LineWidth', 1, 'DisplayName', 'hypotonic');
h(ctrl_n+2:end) = plot(T_mpw, rac1_sep_mpw(2:end,:), 'Color', [1,0,1], 'LineWidth', 1, 'DisplayName', 'Rac1 inhibitor');
legend(h(1:2))
axis square
xlabel('Minutes Post Wounding')
ylabel('Normalized Separation Index')
set(gca,'FontSize',18)

figure
plot(T_mpw, rac1_norm_sep_mpw, 'Color', [1,0,1]), hold on
plot(T_mpw, ctrl_norm_sep_mpw, 'Color', [0,0,0])
xlabel('Minutes Post Wounding')
ylabel('Normalized Separation Index')
set(gca,'FontSize',18)

% Get rid of NaN's in order to plot shaded regions
avg_rac1 = mean(rac1_norm_sep_mpw);
keepIndex = ~isnan(avg_rac1);
avg_rac1 = avg_rac1(keepIndex);
std_rac1 = std(rac1_norm_sep_mpw);
std_rac1 = std_rac1(keepIndex);
T_rac = T_mpw(keepIndex);

avg_ctrl = mean(ctrl_norm_sep_mpw);
keepIndex = ~isnan(avg_ctrl);
avg_ctrl = avg_ctrl(keepIndex);
std_ctrl = std(ctrl_norm_sep_mpw);
std_ctrl = std_ctrl(keepIndex);
T_ctrl = T_mpw(keepIndex);

figure
plot(T_rac, avg_rac1, 'Color', [1,0,1], 'LineWidth', 3), hold on
fill([T_rac fliplr(T_rac)], [avg_rac1+std_rac1 fliplr(avg_rac1-std_rac1)], [0.7,0,0.7], 'FaceAlpha', 0.2, 'EdgeColor', 'none'); 
plot(T_ctrl, avg_ctrl, 'Color', [0,0,0], 'LineWidth', 3), hold on
fill([T_ctrl fliplr(T_ctrl)], [avg_ctrl+std_ctrl fliplr(avg_ctrl-std_ctrl)], [0,0,0], 'FaceAlpha', 0.2, 'EdgeColor', 'none'); 
xlabel('Minutes Post Wound')
ylabel('Normalized Separation Index')
set(gca,'FontSize',18)

%% Plot aligned time plots

close all
figure
plot(T_aligned, rac1_norm_sep_aligned, 'Color', [1,0,1]), hold on
plot(T_aligned, ctrl_norm_sep_aligned, 'Color', [0,0,0])
xlabel('Minutes Relative to Peak')
ylabel('Normalized Separation Index')
set(gca,'FontSize',18)

% Get rid of NaN's in order to plot shaded regions
avg_rac1 = mean(rac1_norm_sep_aligned);
keepIndex = ~isnan(avg_rac1);
avg_rac1 = avg_rac1(keepIndex);
std_rac1 = std(rac1_norm_sep_aligned);
std_rac1 = std_rac1(keepIndex);
T_rac = T_aligned(keepIndex);

avg_ctrl = mean(ctrl_norm_sep_aligned);
keepIndex = ~isnan(avg_ctrl);
avg_ctrl = avg_ctrl(keepIndex);
std_ctrl = std(ctrl_norm_sep_aligned);
std_ctrl = std_ctrl(keepIndex);
T_ctrl = T_aligned(keepIndex);

figure
plot(T_rac, avg_rac1, 'Color', [1,0,1], 'LineWidth', 3), hold on
fill([T_rac fliplr(T_rac)], [avg_rac1+std_rac1 fliplr(avg_rac1-std_rac1)], [0.7,0,0.7], 'FaceAlpha', 0.2, 'EdgeColor', 'none'); 
plot(T_ctrl, avg_ctrl, 'Color', [0,0,0], 'LineWidth', 3), hold on
fill([T_ctrl fliplr(T_ctrl)], [avg_ctrl+std_ctrl fliplr(avg_ctrl-std_ctrl)], [0,0,0], 'FaceAlpha', 0.2, 'EdgeColor', 'none'); 
xlim([-4 15])
ylim([0 1.2])
axis square
xlabel('Minutes Relative to Peak')
ylabel('Normalized Separation Index')
set(gca,'FontSize',18)

%% Plot interpolated time plots
close all
figure
plot(T_interp, rac1_norm_sep_interp, 'Color', [1,0,1]), hold on
plot(T_interp, ctrl_norm_sep_interp, 'Color', [0,0,0])
xlabel('Interpolated Time (Wound to Peak)')
ylabel('Normalized Separation Index')
set(gca,'FontSize',18)

% Get rid of NaN's in order to plot shaded regions
avg_rac1 = mean(rac1_norm_sep_interp);
keepIndex = ~isnan(avg_rac1);
avg_rac1 = avg_rac1(keepIndex);
std_rac1 = std(rac1_norm_sep_interp);
std_rac1 = std_rac1(keepIndex);
T_rac = T_interp(keepIndex);

avg_ctrl = mean(ctrl_norm_sep_interp);
keepIndex = ~isnan(avg_ctrl);
avg_ctrl = avg_ctrl(keepIndex);
std_ctrl = std(ctrl_norm_sep_interp);
std_ctrl = std_ctrl(keepIndex);
T_ctrl = T_interp(keepIndex);

figure
plot(T_rac, avg_rac1, 'Color', [1,0,1], 'LineWidth', 3), hold on
fill([T_rac fliplr(T_rac)], [avg_rac1+std_rac1 fliplr(avg_rac1-std_rac1)], [0.7,0,0.7], 'FaceAlpha', 0.2, 'EdgeColor', 'none'); 
plot(T_ctrl, avg_ctrl, 'Color', [0,0,0], 'LineWidth', 3), hold on
fill([T_ctrl fliplr(T_ctrl)], [avg_ctrl+std_ctrl fliplr(avg_ctrl-std_ctrl)], [0,0,0], 'FaceAlpha', 0.2, 'EdgeColor', 'none'); 
xlabel('Interpolated Time (Wound to Peak)')
ylabel('Normalized Separation Index')
set(gca,'FontSize',18)

%% Fit each fish and do stats...fall phase/exponential
rac_all_a = nan(rac1_n,1);
rac_all_b = nan(rac1_n,1);
rac_5min_post_peak = nan(rac1_n,1);
rac_10min_post_peak = nan(rac1_n,1);


figure
for fish = 1:rac1_n
    fall_rac1 = rac1_norm_sep_aligned(fish,:);
    keepIndex = ~isnan(fall_rac1);
    fall_rac1 = fall_rac1(keepIndex);
    T_rac = T_aligned(keepIndex);
    peak_T = find(T_rac==0);
    %fall_rac1 = fall_rac1(peak_T:end)';
    fall_rac1 = fall_rac1(peak_T:peak_T+12)';
    %T_rac1_fall = T_rac(peak_T:end)';
    T_rac1_fall = T_rac(peak_T:peak_T+12)';
    f = fit(T_rac1_fall,fall_rac1,'exp1');
    rac_all_a(fish) = f.a;
    rac_all_b(fish) = f.b;
    rac_5min_post_peak(fish) = fall_rac1(6);
    rac_10min_post_peak(fish) = fall_rac1(11);
    subplot(3,3,fish)
    plot(f,T_rac1_fall,fall_rac1), hold on
    title(sprintf('rac1 #%d fall phase',fish));
    xlim([0 35])
    ylim([0 1])
    if fish < rac1_n
        legend('off')
    else
        legend('Location','southwest')
    end
end


ctrl_all_a = nan(ctrl_n,1);
ctrl_all_b = nan(ctrl_n,1);
ctrl_5min_post_peak = nan(ctrl_n,1);
ctrl_10min_post_peak = nan(ctrl_n,1);


figure
for fish = 1:ctrl_n%[1:3,6:14,16:ctrl_n]
    fall_ctrl = ctrl_norm_sep_aligned(fish,:);
    keepIndex = ~isnan(fall_ctrl);
    fall_ctrl = fall_ctrl(keepIndex);
    T_rac = T_aligned(keepIndex);
    peak_T = find(T_rac==0);
    %fall_ctrl = fall_ctrl(peak_T:end)';
    fall_ctrl = fall_ctrl(peak_T:peak_T+12)';
    %T_ctrl_fall = T_rac(peak_T:end)';
    T_ctrl_fall = T_rac(peak_T:peak_T+12)';
    f = fit(T_ctrl_fall,fall_ctrl,'exp1');
    ctrl_all_a(fish) = f.a;
    ctrl_all_b(fish) = f.b;
    ctrl_5min_post_peak(fish) = fall_ctrl(6);
    ctrl_10min_post_peak(fish) = fall_ctrl(11);
    subplot(5,4,fish)
    plot(f,T_ctrl_fall,fall_ctrl), hold on
    title(sprintf('ctrl #%d fall phase',fish));
    xlim([0 35])
    ylim([0 1])
    if fish < ctrl_n
        legend('off')
    else
        legend('Location','southwest')
    end
end

%% Summary plots and t-tests

[~, decay_constant_pvalue] = ttest2(ctrl_all_b,rac_all_b)

[~, post5min_pvalue] = ttest2(ctrl_5min_post_peak,rac_5min_post_peak)

[~, post10min_pvalue] = ttest2(ctrl_10min_post_peak,rac_10min_post_peak)

figure

%%%plot decay constants
subplot(1,3,1)
hold on
%ctrl
plot([0.7,1.3],mean(ctrl_all_b)*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',3)
plot([0.75,1.25],(mean(ctrl_all_b) + std(ctrl_all_b))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
plot([0.75,1.25],(mean(ctrl_all_b) - std(ctrl_all_b))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
xx = (rand(1,numel(ctrl_all_b))-0.5)/3;
xx = xx + 1;
scatter(xx, ctrl_all_b, 40, 'filled', 'MarkerFaceColor', [0,0,0])
%rac1
plot([1.7,2.3],mean(rac_all_b)*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',3)
plot([1.75,2.25],(mean(rac_all_b) + std(rac_all_b))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
plot([1.75,2.25],(mean(rac_all_b) - std(rac_all_b))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
xx = (rand(1,numel(rac_all_b))-0.5)/3;
xx = xx + 2;
scatter(xx, rac_all_b, 40, 'filled', 'MarkerFaceColor', [1,0,1])
%labels
yline(0,'--');
ylabel('Separation Decay Constant (\lambda)');
ylim([-0.08,0])
xticks([1 2])
xlim([0 3])
set(gca, 'XTickLabel', {'hypotonic', 'rac1 inhibitor'}); % set x-axis labels
set(gca,'XTickLabelRotation',45)
ax = gca;
ax.FontSize = 14;
titleHandle = title('Exponential fit after peak');
pos  = get( titleHandle , 'position' )
pos1 = pos + [0 0.0025 0] 
set( titleHandle , 'position' , pos1 );

%%%plot 5 min after
subplot(1,3,2)
hold on
%ctrl
plot([0.7,1.3],mean(ctrl_5min_post_peak)*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',3)
plot([0.75,1.25],(mean(ctrl_5min_post_peak) + std(ctrl_5min_post_peak))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
plot([0.75,1.25],(mean(ctrl_5min_post_peak) - std(ctrl_5min_post_peak))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
xx = (rand(1,numel(ctrl_5min_post_peak))-0.5)/3;
xx = xx + 1;
scatter(xx, ctrl_5min_post_peak, 40, 'filled', 'MarkerFaceColor', [0,0,0])
%rac1
plot([1.7,2.3],mean(rac_5min_post_peak)*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',3)
plot([1.75,2.25],(mean(rac_5min_post_peak) + std(rac_5min_post_peak))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
plot([1.75,2.25],(mean(rac_5min_post_peak) - std(rac_5min_post_peak))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
xx = (rand(1,numel(rac_5min_post_peak))-0.5)/3;
xx = xx + 2;
scatter(xx, rac_5min_post_peak, 40, 'filled', 'MarkerFaceColor', [1,0,1])
%labels
ylabel('Separation Index');
ylim([0 1])
xticks([1 2])
xlim([0 3])
set(gca, 'XTickLabel', {'hypotonic', 'rac1 inhibitor'}); % set x-axis labels
set(gca,'XTickLabelRotation',45)
ax = gca;
ax.FontSize = 14;
titleHandle = title('5 minutes after peak');
pos  = get( titleHandle , 'position' )
pos1 = pos + [0 0.03 0] 
set( titleHandle , 'position' , pos1 );

%%%plot 10 min after
subplot(1,3,3)
hold on
%ctrl
plot([0.7,1.3],mean(ctrl_10min_post_peak)*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',3)
plot([0.75,1.25],(mean(ctrl_10min_post_peak) + std(ctrl_10min_post_peak))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
plot([0.75,1.25],(mean(ctrl_10min_post_peak) - std(ctrl_10min_post_peak))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
xx = (rand(1,numel(ctrl_10min_post_peak))-0.5)/3;
xx = xx + 1;
scatter(xx, ctrl_10min_post_peak, 40, 'filled', 'MarkerFaceColor', [0,0,0])
%rac1
plot([1.7,2.3],mean(rac_10min_post_peak)*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',3)
plot([1.75,2.25],(mean(rac_10min_post_peak) + std(rac_10min_post_peak))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
plot([1.75,2.25],(mean(rac_10min_post_peak) - std(rac_10min_post_peak))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
xx = (rand(1,numel(rac_10min_post_peak))-0.5)/3;
xx = xx + 2;
scatter(xx, rac_10min_post_peak, 40, 'filled', 'MarkerFaceColor', [1,0,1])
%labels
ylabel('Separation Index');
ylim([0 1])
xticks([1 2])
xlim([0 3])
set(gca, 'XTickLabel', {'hypotonic', 'rac1 inhibitor'}); % set x-axis labels
set(gca,'XTickLabelRotation',45)
ax = gca;
ax.FontSize = 14;
titleHandle = title('10 minutes after peak');
pos  = get( titleHandle , 'position' );
pos1 = pos + [0 0.03 0];
set( titleHandle , 'position' , pos1 );