%% ==== Initial Setup ====
%%% Change these parameters to set for al the subsequent sections
run_initial_setup()
%UM_PER_BIN = 5; % removing this, the parameters are saved in units of �m

%% ==== Combine/plot guided fits from multiple users ==== 
%%% After multiple users guide the fitting, use this script to combine data
%%% for plotting and stats
%Struct to store all measurements (from different users) 
%of each biological sample (kymograph)
combinedParamsList = struct('slope',[],'intercept',[],'valid_for_fit',[],'max_distance',[], 'points_for_fitting', {}, 'slope_no_extreme', [],'intercept_no_extreme', []);
%Struct to store the averaged measurments for each sample
mainParamsList = struct('slope',[],'intercept',[],'valid_for_fit',[],'max_distance',[],'slope_no_extreme', [], 'intercept_no_extreme', []);
errorParamsList = struct('slope',[],'intercept',[],'valid_for_fit',[],'max_distance',[],'slope_no_extreme', [], 'intercept_no_extreme', []);

%Input the .mat files to be combined
paramsLists_to_load = {'20220129_separationParameters_AK.mat';
                       '20220131_separationParameters_EL.mat';
                       '20220201_separationParameters_MS.mat'};
load(paramsLists_to_load{1});
nSamples = size(paramsList,1); %number of different kymographs 
nMeasurements = numel(paramsLists_to_load); %number of different measurements (from different scorers) of each kymograph
combinedParamsList(nSamples,1).slope = [];
mainParamsList(nSamples,1).slope = [];
errorParamsList(nSamples,1).slope = [];
%Combine all measurements of each sample into vectors
for measurementIdx=1:nMeasurements
    load(paramsLists_to_load{measurementIdx});
    for sampleIdx = 1:nSamples
        combinedParamsList(sampleIdx).slope = ...
           [combinedParamsList(sampleIdx).slope paramsList(sampleIdx).slope];
        combinedParamsList(sampleIdx).intercept = ...
            [combinedParamsList(sampleIdx).intercept paramsList(sampleIdx).intercept];
        combinedParamsList(sampleIdx).valid_for_fit = ...
            [combinedParamsList(sampleIdx).valid_for_fit paramsList(sampleIdx).valid_for_fit];
        if paramsList(sampleIdx).valid_for_fit
            combinedParamsList(sampleIdx).max_distance = ...
                [combinedParamsList(sampleIdx).max_distance max(paramsList(sampleIdx).points_for_fitting(:,2))];
        end
        combinedParamsList(sampleIdx).points_for_fitting{end+1} = ...
            paramsList(sampleIdx).points_for_fitting;
    end
end

%Average together the measurements for each sample
for sampleIdx = 1:nSamples
    mainParamsList(sampleIdx).slope = mean(combinedParamsList(sampleIdx).slope);
    mainParamsList(sampleIdx).intercept = mean(combinedParamsList(sampleIdx).intercept);
    mainParamsList(sampleIdx).max_distance = mean(combinedParamsList(sampleIdx).max_distance);
    %Look for any measurements that were invalid for this sample - take any
    %that were counted valid by the majority of scoreres
    mainParamsList(sampleIdx).valid_for_fit = logical(round(mean(combinedParamsList(sampleIdx).valid_for_fit)));
    
    %Calculate std error of the mean across the different paramsLists
    errorParamsList(sampleIdx).slope = std(combinedParamsList(sampleIdx).slope)/sqrt(length(combinedParamsList(sampleIdx).slope));
    errorParamsList(sampleIdx).intercept = std(combinedParamsList(sampleIdx).intercept)/sqrt(length(combinedParamsList(sampleIdx).intercept));
    errorParamsList(sampleIdx).max_distance = std(combinedParamsList(sampleIdx).max_distance)/sqrt(length(combinedParamsList(sampleIdx).max_distance));
    
    %remove extreme slopes and calculate slope&intercept again
    combinedParamsList(sampleIdx).slope_no_extreme = combinedParamsList(sampleIdx).slope;
    combinedParamsList(sampleIdx).slope_no_extreme(combinedParamsList(sampleIdx).slope<0) = nan;
    combinedParamsList(sampleIdx).slope_no_extreme(combinedParamsList(sampleIdx).slope>100) = nan;
    combinedParamsList(sampleIdx).intercept_no_extreme = combinedParamsList(sampleIdx).intercept;
    combinedParamsList(sampleIdx).intercept_no_extreme(combinedParamsList(sampleIdx).slope<0) = nan;
    combinedParamsList(sampleIdx).intercept_no_extreme(combinedParamsList(sampleIdx).slope>100) = nan;
    
    if sum(~isnan(combinedParamsList(sampleIdx).slope_no_extreme))>1
        mainParamsList(sampleIdx).slope_no_extreme = nanmean(combinedParamsList(sampleIdx).slope_no_extreme); 
        mainParamsList(sampleIdx).intercept_no_extreme = nanmean(combinedParamsList(sampleIdx).intercept_no_extreme);
        errorParamsList(sampleIdx).slope_no_extreme = nanstd(combinedParamsList(sampleIdx).slope_no_extreme)/sqrt(sum(~isnan(combinedParamsList(sampleIdx).slope_no_extreme)));
        errorParamsList(sampleIdx).intercept_no_extreme = nanstd(combinedParamsList(sampleIdx).intercept_no_extreme)/sqrt(sum(~isnan(combinedParamsList(sampleIdx).intercept_no_extreme)));
    else
        mainParamsList(sampleIdx).slope_no_extreme = nan;
        mainParamsList(sampleIdx).intercept_no_extreme = nan;
        errorParamsList(sampleIdx).slope_no_extreme = nan;
        errorParamsList(sampleIdx).intercept_no_extreme = nan;
    end

end

%% ==== Plot Different Parameters, Andrew's way ====
%%% Use this script to plot the parameters for different data sets, to get
%%% a sense for how quickly separation is proceeding
valid_fit = [mainParamsList.valid_for_fit];
slopeList = [mainParamsList.slope]; %units of bins per minute
interceptList = [mainParamsList.intercept];
distanceList = [mainParamsList.max_distance];


startTime = interceptList(valid_fit); %units of minutes, filter only valid fits

initialSlope = slopeList(valid_fit); %units of um per minute

maxDistance = distanceList(valid_fit); %units of um

figure('Position', [200, 200, 130, 137]);
%Plot the initial slope
subplot(1,3,1)
hold on
plot([-1.5,1.5],mean(initialSlope)*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',1.5)
plot([-1.2,1.2],(mean(initialSlope) + std(initialSlope))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',0.75)
plot([-1.2,1.2],(mean(initialSlope) - std(initialSlope))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',0.75)
xx = rand(1,numel(initialSlope)) - 0.5;
plot(xx, initialSlope, 'k.','MarkerSize',10);
xlim([-2,2])
%ylim([0,90]) %may need to change for your data!
xticks([])
ylabel('Propagation speed (�m/min)');
xlabel(sprintf('%0.0f�%0.0f �m/min',mean(initialSlope), std(initialSlope)))
ax = gca;
ax.FontSize = 11;

%Plot the starting time
subplot(1,3,2)
hold on
plot([-1.5,1.5],mean(startTime)*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',1.5)
plot([-1.2,1.2],(mean(startTime) + std(startTime))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',0.75)
plot([-1.2,1.2],(mean(startTime) - std(startTime))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',0.75)
xx = rand(1,numel(startTime)) - 0.5;
plot(xx, startTime, 'k.','MarkerSize',10);
xlim([-2,2])
%ylim([0,8]) %may need to change for your data!
xticks([])
ylabel('Separation initiation time (min post wound)');
xlabel(sprintf('%0.1f�%0.0f min',mean(startTime), std(startTime)))
ax = gca;
ax.FontSize = 11;


%Plot the max distance separation traveled
subplot(1,3,3)
hold on
plot([-1.5,1.5],mean(maxDistance)*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',1.5)
plot([-1.2,1.2],(mean(maxDistance) + std(maxDistance))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',0.75)
plot([-1.2,1.2],(mean(maxDistance) - std(maxDistance))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',0.75)
xx = rand(1,numel(maxDistance)) - 0.5;
plot(xx, maxDistance, 'k.','MarkerSize',10);
xlim([-2,2])
%ylim([0,8]) %may need to change for your data!
xticks([])
ylabel('Distance of linear propagation from wound (�m)');
xlabel(sprintf('%0.1f�%0.0f �m',mean(maxDistance), std(maxDistance)))
ax = gca;
ax.FontSize = 11;

%% ==== Plot Different Parameters By Condition ====
%%% Use this script to plot the parameters for different data sets, to get
%%% a sense for how quickly separation is proceeding

% assemble condition lists
load('20220128_Cal8-12_Detect4-12_separationKymographs_woundAligned_.mat');
ConditionVector = {sepKymographList.label};
ConditionVector = ConditionVector(~cellfun('isempty',ConditionVector));

ConditionLists = {[]; []; []; []; []};
ConditionNames = {'E3', 'Sorb', 'DMSO', 'Blebb', 'RAC1inhib'};

for k = 1:numel(ConditionVector)
    condition = ConditionVector{k};
    condition_idx = find(strcmp(ConditionNames,condition));
    ConditionLists{condition_idx}(end+1) = k;
end
nConditions = size(ConditionLists,1);

%% test different filters

close all

%organize measurements
valid_fit = [mainParamsList.valid_for_fit];
slopeList = [mainParamsList.slope]; %units of bins per minute
interceptList = [mainParamsList.intercept];
distanceList = [mainParamsList.max_distance];
slopeList_valid = slopeList; %already units of um per minute
slopeList_valid(~valid_fit) = NaN;
interceptList_valid = interceptList;
interceptList_valid(~valid_fit) = NaN;
distanceList_valid = distanceList;
distanceList_valid(~valid_fit) = NaN;
noextr_slopeList = [mainParamsList.slope_no_extreme];
noextr_slopeList(~valid_fit) = NaN;
noextr_interceptList = [mainParamsList.intercept_no_extreme];
noextr_interceptList(~valid_fit) = NaN;

%organize errors
slopeErrors = [errorParamsList.slope];
interceptErrors = [errorParamsList.intercept];
distanceErrors = [errorParamsList.max_distance];
noextr_slopeErrors = [errorParamsList.slope_no_extreme];
noextr_interceptErrors = [errorParamsList.intercept_no_extreme];

%figure, histogram(slopeErrors,30), title('slope errors')
%figure, histogram(interceptErrors,30), title('intercept errors')
%figure, histogram(interceptErrors,30), title('intercept errors')

figure, scatter(distanceList_valid/5,slopeList_valid,'filled', 'DisplayName', 'all fits'), hold on
scatter(distanceList_valid/5,noextr_slopeList,'filled', 'DisplayName', 'removed extreme slopes')
xlabel('propagation distance')
ylabel('average slope')
legend('show','location','Northwest')
set(gca,'FontSize', 18)

figure, subplot(2,1,1), scatter(distanceList_valid/5,slopeList_valid,'filled', 'DisplayName', 'all fits'), hold on
ylim([0 60])
xlabel('propagation distance')
ylabel('average slope')
legend('show','location','Northeast')
set(gca,'FontSize', 18)
subplot(2,1,2), scatter(distanceList_valid/5,noextr_slopeList,'filled', 'DisplayName', 'removed extreme slopes')
xlabel('propagation distance')
ylabel('average slope')
legend('show','location','Northeast')
set(gca,'FontSize', 18)

%{
figure, scatter(slopeList_valid,interceptErrors)
xlabel('slope')
ylabel('SEM of intercept')

allslopes = [combinedParamsList.slope];
figure, histogram(allslopes)
title('all slope measurements')

alldistances = [combinedParamsList.max_distance];
figure, scatter(alldistances/5,allslopes)
xlabel('number of points (assuming no outliers were removed)')
ylabel('slope')

%}

%{
figure
for rep = 1:3
    subplot(1,3,rep)
    scatter(combinedParamsList(3).points_for_fitting{rep}(:,1),combinedParamsList(3).points_for_fitting{rep}(:,2), 'filled' ), hold on
    xlim([0 45])
    ylim([0 180])
end
%}

%{
%filter for high error measurements
slopeFiltered = slopeList_valid;
slopeFiltered(slopeErrors > 10) = NaN;
interceptFiltered = interceptList_valid;
interceptFiltered(interceptErrors > 1) = NaN;
distanceFiltered = distanceList_valid;
distanceFiltered(distanceErrors > 20) = NaN;
%}

%% Plot filtered data

%figure('Position', [200, 200, 130, 137]);
figure
colors = viridis(10);
for condition = [1]%1:nConditions
    %Plot the initial slope
    subplot(1,3,1)
    hold on
    plot([-1.5,1.5],nanmean(noextr_slopeList(ConditionLists{condition}))*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',3)
    plot([-1.2,1.2],(nanmean(noextr_slopeList(ConditionLists{condition})) + nanstd(noextr_slopeList(ConditionLists{condition})))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
    plot([-1.2,1.2],(nanmean(noextr_slopeList(ConditionLists{condition})) - nanstd(noextr_slopeList(ConditionLists{condition})))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
    xx = rand(1,numel(slopeList_valid)) - 0.5;
    %plot(xx(ConditionList{condition}), slopeList_valid(ConditionList{condition}), '.','MarkerSize',10, 'color', colors(condition*2,:), 'DisplayName', ConditionNames{condition});
    %errorbar(xx(ConditionLists{condition}), noextr_slopeList(ConditionLists{condition}), noextr_slopeErrors(ConditionLists{condition}),'.','MarkerSize',20, 'color', colors(condition+4,:), 'CapSize', 4)
    scatter(xx(ConditionLists{condition}), noextr_slopeList(ConditionLists{condition}), 50, 'filled', 'MarkerFaceColor', colors(condition+4,:))
    xlim([-2,2])
    ylim([0,70]) %may need to change for your data!
    xticks([])
    ylabel('Propagation speed (�m/min)');
    ax = gca;
    ax.FontSize = 16;

    %Plot the starting time
    subplot(1,3,2)
    hold on
    plot([-1.5,1.5],nanmean(noextr_interceptList(ConditionLists{condition}))*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',3)
    plot([-1.2,1.2],(nanmean(noextr_interceptList(ConditionLists{condition})) + nanstd(noextr_interceptList(ConditionLists{condition})))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
    plot([-1.2,1.2],(nanmean(noextr_interceptList(ConditionLists{condition})) - nanstd(noextr_interceptList(ConditionLists{condition})))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
    xx = rand(1,numel(interceptList_valid)) - 0.5;
    %plot(xx(ConditionList{condition}), interceptList_valid(ConditionList{condition}), '.','MarkerSize',10, 'color', colors(condition*2,:), 'DisplayName', ConditionNames{condition});
    %errorbar(xx(ConditionLists{condition}), noextr_interceptList(ConditionLists{condition}), noextr_interceptErrors(ConditionLists{condition}),'.','MarkerSize',20, 'color', colors(condition+4,:), 'CapSize', 4)
    scatter(xx(ConditionLists{condition}), noextr_interceptList(ConditionLists{condition}), 50, 'filled', 'MarkerFaceColor', colors(condition+4,:))
    xlim([-2,2])
    ylim([0,12]) %may need to change for your data!
    xticks([])
    ylabel('Initiation time (min post wound)');
    ax = gca;
    ax.FontSize = 16;
    
    %Plot the max distance of linear propagation
    subplot(1,3,3)
    hold on
    plot([-1.5,1.5],mean(distanceList_valid(ConditionLists{condition}))*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',3)
    plot([-1.2,1.2],(mean(distanceList_valid(ConditionLists{condition})) + std(distanceList_valid(ConditionLists{condition})))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
    plot([-1.2,1.2],(mean(distanceList_valid(ConditionLists{condition})) - std(distanceList_valid(ConditionLists{condition})))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
    xx = rand(1,numel(distanceList_valid)) - 0.5;
    %plot(xx(ConditionList{condition}), distanceList_valid(ConditionList{condition}), '.','MarkerSize',10, 'color', colors(condition*2,:), 'DisplayName', ConditionNames{condition});
    %errorbar(xx(ConditionLists{condition}), distanceList_valid(ConditionLists{condition}), distanceErrors(ConditionLists{condition}),'.','MarkerSize',20, 'color', colors(condition+4,:), 'CapSize', 4)
    scatter(xx(ConditionLists{condition}), distanceList_valid(ConditionLists{condition}), 50, 'filled', 'MarkerFaceColor', colors(condition+4,:))
    xlim([-2,2])
    ylim([0,250]) %may need to change for your data!
    xticks([])
    ylabel('Propagation distance (�m)');
    ax = gca;
    ax.FontSize = 16;
end

%{
legend('Location', 'bestoutside')
subplot(1,3,2)
legend('Location', 'bestoutside')
subplot(1,3,1)
legend('Location', 'bestoutside')
%}

%% Separate groups on x axis

%figure('Position', [200, 200, 130, 137]);
figure
colors = viridis(10);
conditions_to_plot = [1,3,4];
slope_by_condition = nan(size(distanceList_valid,2),length(conditions_to_plot));
intercept_by_condition = nan(size(distanceList_valid,2),length(conditions_to_plot));
distance_by_condition = nan(size(distanceList_valid,2),length(conditions_to_plot));
for plotting_idx = [1:3]%1:nConditions
    condition = conditions_to_plot(plotting_idx);
    
    %Plot the initial slope
    slope_by_condition(ConditionLists{condition},plotting_idx) = noextr_slopeList(ConditionLists{condition});
    subplot(1,3,3)
    hold on
    plot([plotting_idx-.3,plotting_idx+.3],nanmean(noextr_slopeList(ConditionLists{condition}))*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',3)
    plot([plotting_idx-.25,plotting_idx+.25],(nanmean(noextr_slopeList(ConditionLists{condition})) + nanstd(noextr_slopeList(ConditionLists{condition})))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
    plot([plotting_idx-.25,plotting_idx+.25],(nanmean(noextr_slopeList(ConditionLists{condition})) - nanstd(noextr_slopeList(ConditionLists{condition})))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
    xx = (rand(1,numel(distanceList_valid))-0.5)/3;
    xx(ConditionLists{condition}) =  xx(ConditionLists{condition}) + plotting_idx;
    %errorbar(xx(ConditionLists{condition}), noextr_slopeList(ConditionLists{condition}), noextr_slopeErrors(ConditionLists{condition}),'.','MarkerSize',15, 'color', colors(condition+4,:), 'CapSize', 4)
    scatter(xx(ConditionLists{condition}), noextr_slopeList(ConditionLists{condition}), 40, 'filled', 'MarkerFaceColor', colors(condition+4,:))
    ylabel('Propagation speed (�m/min)');
    ylim([0,55])
    xticks([1 2 3])
    xlim([0 4])
    set(gca, 'XTickLabel', {'hypotonic', 'vehicle control', 'blebbistatin'}); % set x-axis labels
    set(gca,'XTickLabelRotation',45)
    ax = gca;
    ax.FontSize = 14;
    
    %Plot the starting time
    intercept_by_condition(ConditionLists{condition},plotting_idx) = noextr_interceptList(ConditionLists{condition});
    subplot(1,3,2)
    hold on
    plot([plotting_idx-.3,plotting_idx+.3],nanmean(noextr_interceptList(ConditionLists{condition}))*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',3)
    plot([plotting_idx-.25,plotting_idx+.25],(nanmean(noextr_interceptList(ConditionLists{condition})) + nanstd(noextr_interceptList(ConditionLists{condition})))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
    plot([plotting_idx-.25,plotting_idx+.25],(nanmean(noextr_interceptList(ConditionLists{condition})) - nanstd(noextr_interceptList(ConditionLists{condition})))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',1.5)
    xx = (rand(1,numel(distanceList_valid))-0.5)/3;
    xx(ConditionLists{condition}) =  xx(ConditionLists{condition}) + plotting_idx;
    %errorbar(xx(ConditionLists{condition}), noextr_interceptList(ConditionLists{condition}), noextr_interceptErrors(ConditionLists{condition}),'.','MarkerSize',15, 'color', colors(condition+4,:), 'CapSize', 4)
    scatter(xx(ConditionLists{condition}), noextr_interceptList(ConditionLists{condition}), 40, 'filled', 'MarkerFaceColor', colors(condition+4,:))
    ylabel('Initiation time (mpw)');
    ylim([0,14]) %may need to change for your data!
    xticks([1 2 3])
    xlim([0 4])
    set(gca, 'XTickLabel', {'hypotonic', 'vehicle control', 'blebbistatin'}); % set x-axis labels
    set(gca, 'XTickLabelRotation',45)
    ax = gca;
    ax.FontSize = 14;
    
    %Plot the max distance of linear propagation
    subplot(1,3,1)
    
    distance_by_condition(ConditionLists{condition},plotting_idx) = distanceList_valid(ConditionLists{condition});
    hold on
    plot([plotting_idx-.3,plotting_idx+.3],mean(distanceList_valid(ConditionLists{condition}),'omitnan')*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',3)
    plot([plotting_idx-.25,plotting_idx+.25],(mean(distanceList_valid(ConditionLists{condition}),'omitnan') + std(distanceList_valid(ConditionLists{condition}),'omitnan'))*ones(1,2),...
        '-','Color',[.6,.6,.6],'LineWidth',1.5)
    plot([plotting_idx-.25,plotting_idx+.25],(mean(distanceList_valid(ConditionLists{condition}),'omitnan') - std(distanceList_valid(ConditionLists{condition}),'omitnan'))*ones(1,2),...
        '-','Color',[.6,.6,.6],'LineWidth',1.5)
    xx = (rand(1,numel(distanceList_valid))-0.5)/3;
    xx(ConditionLists{condition}) =  xx(ConditionLists{condition}) + plotting_idx;
    %errorbar(xx(ConditionLists{condition}), distanceList_valid(ConditionLists{condition}), distanceErrors(ConditionLists{condition}),'.','MarkerSize',15, 'color', colors(condition+4,:), 'CapSize', 4)
    scatter(xx(ConditionLists{condition}), distanceList_valid(ConditionLists{condition}), 40, 'filled', 'MarkerFaceColor', colors(condition+4,:))
    ylabel('Propagation distance (�m)');
    ylim([0,220])
    xticks([1 2 3])
    xlim([0 4])
    set(gca, 'XTickLabel', {'hypotonic', 'vehicle control', 'blebbistatin'}); % set x-axis labels
    set(gca,'XTickLabelRotation',45)
    ax = gca;
    ax.FontSize = 18;
end

[~, hypo_dmso_slope_pvalue] = ttest2(slope_by_condition(:,1),slope_by_condition(:,2))
[~, dmso_blebb_pvalue_pvalue] = ttest2(slope_by_condition(:,3),slope_by_condition(:,2))

[~, hypo_dmso_intercept_pvalue] = ttest2(intercept_by_condition(:,1),intercept_by_condition(:,2))
[~, dmso_blebb_intecept_pvalue] = ttest2(intercept_by_condition(:,3),intercept_by_condition(:,2))

[~, hypo_dmso_dist_pvalue] = ttest2(distance_by_condition(:,1),distance_by_condition(:,2))
[~, dmso_blebb_dist_pvalue] = ttest2(distance_by_condition(:,3),distance_by_condition(:,2))


%{
figure
for condition = [1,3,4]
    scatter(distanceList_valid(ConditionLists{condition}),noextr_slopeList(ConditionLists{condition}), 60, 'filled', 'MarkerFaceColor', colors(condition*2,:),'DisplayName', ConditionNames{condition}), hold on
end
xlabel('propagation distance')
ylabel('propagation speed')
legend('show')

set(gca, 'FontName','Arial','FontSize',18,'FontWeight','Bold')
%}

%% Bar chart for number of movies with separation
countvalidity = zeros(nConditions,2);
percentvalidity = zeros(nConditions,2);
for condition = 1:nConditions
    n = numel(ConditionLists{condition});
    nValid = sum(valid_fit(ConditionLists{condition}));
    percentvalidity(condition,1) = nValid/n*100;
    percentvalidity(condition,2) = (1-nValid/n)*100;
    countvalidity(condition,1) = nValid;
    countvalidity(condition,2) = n-nValid;
end

figure
X = categorical(ConditionNames);
X = reordercats(X,ConditionNames);
bar(X, percentvalidity, 'stacked')
legend({'Could fit', 'Could not fit'},'Location','bestoutside')
set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold');
ylim([0 100])
ylabel('Percentage')


figure
bar(X, countvalidity, 'stacked')
legend({'Could fit', 'Could not fit'},'Location','bestoutside')
set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold');
%ylim([0 12])
ylabel('Count')

figure
figureConditionNames = {'hypotonic', 'isotonic (sorbitol)', 'vehicle control', 'blebbistatin', 'NSC23766'};
X = categorical(figureConditionNames(1:4));
X = reordercats(X,figureConditionNames(1:4));
bar(X, percentvalidity(1:4,:), 'stacked')
legend({'Separation Detected', 'Not Detected'},'Location','bestoutside')
set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold');
ylim([0 100])
ylabel('Percentage')
