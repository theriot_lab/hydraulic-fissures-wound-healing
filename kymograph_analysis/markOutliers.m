function [axHandle, outlierIdx] = markOutliers(axHandle)
%%% Use the brushing tool to identify outlier points on the current axes.
%%% Save the index of these points. This command will wait until you are
%%% done selecting points on the figure with the brush tool. This tool
%%% allows you to drag a box around points to select, and use the Shift key
%%% to select (or deselect) mutiple points. 
%%% To indicate you are done, put your cursor in the command window of
%%% MATLAB and hit enter.
%%% Input:
%%% - axHandle: handle of axes that contains a line to check. 
%%%   IMPORTANT: Right now it is assumed that there is only one line in the
%%%   figure to check for outliers, and there may be other elements of the figure
%%%   (e.g. a background image or heatmap) that will be ignored for this. 
%%%   This function has not been tested with multiple line objects on the
%%%   plot.
%%% Output: 
%%% - axHandle: Return the axes
%%% - outlierIdx: A logical array of the same size as the number of data
%%%   points in the line to be inspected. 1 indicates an outlier, 0 indicates
%%%   an inlier.

axes(axHandle);
brush on
pause % Will wait until you press Enter in the MATLAB command window
outlierIdx = [];
for childIdx = 1:numel(axHandle.Children)
    %Identify which child of the figure is the Line
    if isa(axHandle.Children(childIdx), 'matlab.graphics.chart.primitive.Line')
        outlierIdx = logical(axHandle.Children(childIdx).BrushData);
    end
end
brush off