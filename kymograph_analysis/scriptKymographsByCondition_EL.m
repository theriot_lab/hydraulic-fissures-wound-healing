%% ==== Combine/Average Kymographs ====
%%% Use this code to make a single kymograph by averaging a series of
%%% kymographs point-by-point.

%%%input variables
load('20220128_Cal8-12_Detect4-12_separationKymographs_woundAligned_.mat');
kymographUseList = [1:20,24:42,44:65,68:78]; %just removed kymograph 43 becuase it cuts off early

%Set the range of time (t) and space (d) you want to plot
tvec = (0:.5:29.5)'; %in minutes
dvec = (0:200)'; % in microns

% assemble condition lists
kymographConditionList = {[]; []; []; []; []};
kymographConditionNames = {'E3', 'Sorb', 'DMSO', 'Blebb', 'RAC1inhib'};

for k = kymographUseList
    condition = sepKymographList(k).label;
    condition_idx = find(strcmp(kymographConditionNames,condition));
    kymographConditionList{condition_idx}(end+1) = k;
end

%% Make average kymograph matrices

clearvars meanKymograph interpKymographList

[T, D] = meshgrid(tvec, dvec);
%Initialize a container to hold all the kymographs
interpKymographList = nan([size(T), numel(sepKymographList)]);
%Loop through kymographs
for k = kymographUseList
    %Interpolate the kymograph so it's on the same coordinates as all the
    %others
    kymograph = interp2(sepKymographList(k).T, ...
                        double(sepKymographList(k).D),...
                        sepKymographList(k).kymograph,...
                        T, D, 'linear', nan);
    interpKymographList(:, :, k) = kymograph;
end

%Average together the kymographs by condition
meanKymograph = nan([size(T), numel(kymographConditionList)]);
for condition = 1:size(kymographConditionList,1)
    indexes = kymographConditionList{condition};
    meanKymograph(:,:,condition) = mean(interpKymographList(:,:,indexes), 3);
end

%% Plot
close all
figure('Position',[200,600,1000,160]);
bottom = min(meanKymograph,[],'all');
top = max(meanKymograph,[],'all');
normedmeanKymograph = (meanKymograph-bottom)./top;
figureConditionNames = {'hypotonic', 'isotonic (sorbitol)', 'vehicle control', 'blebbistatin', 'NSC23766'};
    for condition = 1:4%size(kymographConditionList,1)
        subplot(1, 4, condition)
        imagesc('XData', T(1,:), 'YData', D(:, 1), 'CData', normedmeanKymograph(:,:,condition));
        ax = gca;
        colormap(ax, magma(256));
        caxis manual
        caxis([0 1]);
        xlim([0, 28])
        ylim([0, 125])
        yticks([0, 25, 50, 75, 100, 125])
        xticks([0, 10, 20])
        ax.FontSize = 8;
        xlabel('Time post wounding (min)', 'FontSize', 12);
        ylabel('Distance from wound (�m)', 'FontSize', 12);
        ax.XColor = [0,0,0]; %make the 'black' fonts truly black
        ax.YColor = [0,0,0];
        title(sprintf('%s, n = %d', figureConditionNames{condition}, size(kymographConditionList{condition},2)), 'FontSize', 12)
        hcb = colorbar('Ticks',[0,1], 'FontName', 'Arial', 'FontSize', 8);
        hcb.Label.String = 'Separation Index (A.U.)';
        hcb.Label.Rotation = 270;
        hcb.Label.FontName = 'Arial';
        hcb.Label.FontSize = 8;
    end
    
    %%
    set(gcf,'PaperType','A4')
    orient(gcf,'landscape')
    print(gcf, 'Fig.pdf', '-dpdf', '-loose')