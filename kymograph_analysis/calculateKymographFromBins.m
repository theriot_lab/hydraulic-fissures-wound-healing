function kymograph = calculateKymographFromBins(dataStack, binArray)
%%% Given an array of bin indexes, a 3D array of data into a 2D kymograph.
%%% Input:
%%% - dataStack: the 3D array of data. Dimensions are [Y, X, T], where
%%%     Y is # of rows
%%%     X is # of columns
%%%     T is # of images in the dataStack
%%% - binArray: an array of dimension [Y, X] where each entry contains the
%%%   bin for the corresponding position in each image in the dataStack,
%%%   i.e. binArray(i,j) = 4 means that position (i,j) in each slice of
%%%   the dataStack (i.e. dataStack(i,j,1), dataStack(i,j,2),...) should be
%%%   in bin 4.
%%% Output:
%%% - kymograph: This kymograph is size [B, T], where B is the number of
%%%   bins (i.e. B = max(binsArray(:))), and T is the number of slices in
%%%   the dataStack. kymograph(1,5) corresponds to the value in bin 1 and
%%%   frame 5. 
%%% Currently data is aggregated into bins using the nanmean function in
%%% matlab, i.e. averaged, ignoring NaN entries.

%Validation: make sure things are the right size
szData = size(dataStack);
szBins = size(binArray);
assert(all(szData(1:2) == szBins), ['binsArray must have the same size as ',...
            'each slice of dataStack']);


maxBins = max(binArray(:));
sizeT = size(dataStack, 3);
%Assign a dummy bin to any "out-of-range" bins
binArray(binArray==0) = maxBins + 1;


kymograph = zeros(maxBins + 1, sizeT);
%Generate the kymograph one column at a time
for iT = 1:sizeT
    data = dataStack(:, :, iT);
    kymograph(:, iT) = accumarray(binArray(:), data(:), [], @nanmean);
end
%Remove data in the dummy bin which was for "out-of-range" data.
kymograph = kymograph(1:end-1, :);
end