function handle = displayPointsOnKymograph(kymograph, times, bins, UM_PER_BIN)
%%% Create figure/handle for easy user annotation
%%% Input:
%%% - kymograph: 2D array of dimension [Y,X]
%%% - times: ???
%%% - bins: ???
%%% - UM_PER_BIN
%%% Output:
        
    %Find column coordinates of the transition points for each row
    idxList = findTransitionPoints(kymograph);
    idxNotNan = find(~isnan(idxList));
    idxList = idxList(idxNotNan);
    
    %Lookup the corresponding t and d values
    t = times(1,:)';
    dbins = bins(:,1);
    points = [t(idxList), dbins(idxNotNan)];

    %Make the figure 
    figure, 
    hold on
    %Plot the kymograph
    imagesc('XData', times(1, :), ...
            'YData', bins(:, 1)*UM_PER_BIN, ...
            'CData', kymograph);
    ax = gca;
    ax.YDir = 'normal';
    colormap(ax, magma(256));
    caxis([min(kymograph(:)), max(kymograph(:))]); %set dynamic range for colors
    %Plot the inlier points
    plot(points(:,1), UM_PER_BIN * points(:, 2), ...
         'g.', 'MarkerSize', 10);

handle = gca;
end

