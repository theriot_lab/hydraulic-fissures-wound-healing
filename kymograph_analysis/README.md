# Kymograph analysis
Scripts in this folder are for taking the result of ridge detection and consolidating them into a kymograph.

This has been broken into two subscripts: 
* `measureKymographSlope_LeastSquares.m` which can be used to measure the slope and intercept of the linear propagation of separation in the kymograph. Please see the notes below on how to do this (there are some manual steps involved).
* `plotKymographMeasurments.m` can be used to combine the measurements made by different individuals on the same kymographs and then plot the slope and intercept of each kymograph to see how quickly separatino propagates through the tissue (slope) and when separation begins (intercept). This is more automatic, though you do need to double-check the bin size setting at the beginning of the script, as well as specify the different measurement files to combine.

Another main script is `scriptKymographAnalysis.m`. This contains code to make kymographs from ridge detection images, as well as older versions of code that do the measuring and plotting tasks described above. It is broken up into sections which are pretty explicitly commented. 

There are a few things at the very beginning that you may want to change, such as the microns per bin in your kymograph (`UM_PER_BIN`).

Additionally, there are a few parameters _within_ each section that you will want to review and tweak, mostly related to which
portion of the kymograph do you want to analyze or plot. So look at each one before running the whole thing.

## How to make measurements on kymographs
1. Run the first two sections of `scriptKymographAnalysis.m` to get kymographs for different datasets. Read the comments in those portions of the code and make sure parameters are set for your data. This should give you a file with `<date>_separationKymographs.mat` that includes the kymographs with some metadata.
2. Run `measureKymographSlope_LinearRegression.m,` with your kymograph file in your working directory. User input is required at a few steps in this procedure, as described here:
    1. The script will pop up a plot of the kyomograph, with regions of large incrase in signal going along a row (i.e. along time) will be marked in green. These are a guide to the eye of where separation is presumed to be occurring. 
    2. First the script will ask if this is a valid kymograph for fitting. Sometimes no separation occurs, and in these cases all the green dots will be lined up along the left hand side (because there is no significant increase in separation signal later on in the movie). So if the green dots are mostly lined up on the left, say 'No', otherwise if there is a region in which the separation signal has increased at later times, say 'Yes.' If you select 'No' you will automatically move on to the next kymograph in the file.
    3. Next the script will ask you to identify the furthest extent of linear propagation. Usually separation propagates linearly up to a point, after which it slows down or stops altogether. Click the point at which the linear propagation appears to end. Note, if the separation signal does not propagate, but shows up as a vertical line, you can mark the end of this vertical line, i.e. the furthest extent into the tissue in which separation appears.
    4.  Finally, the script will activate the "brush" tool and ask you to mark outliers. This can be done by dragging the mouse, which will create a box to select different green points that seem to be significant outliers from the linear propagation. You can also use the Shift key to select multiple points. Only points within the linearly propagating region are relevant to consider as possible outliers, since points beyond this region will already be discarded. Once you are done selecting outliers, or if there are not outliers to report, activate your cursor in the Matlab terminal window by clicking and then press 'Enter' (the program will wait to proceed until the Enter key has been specifically entered in the terminal/console portion of the Matlab software). Then a new kymograph will appear -- go back to step 1.

## To-Do
* Figure out how to plot the slope from "vertical" kymographs - maybe use population median instead of mean? Or a threshold?
* Do we want to have all the kymographs in the same file? Or will we need to adjust this to plot kymographs from different data files?
