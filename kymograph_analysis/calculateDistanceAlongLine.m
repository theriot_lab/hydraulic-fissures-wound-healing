function binArray = calculateDistanceAlongLine(sz, x0, y0, x1, y1, umPerPixel, umPerBin);
%%%Bin each pixel in an image based on its projected distance along a
%%%specified line.
%%%Inputs:
%%% sz: size of an image (in (rows, columns))
%%% x0,y0: the (spatial) coordinates of the start point/origin of the line
%%% in PIXELS
%%% x1,y1: the (spatial) coordinates of a second point along the line in
%%% PIXELS
%%% umPerPixel: define number of microns per pixel
%%% umPerBin: width of bins used, measured in projected distance along the
%%% line from the origin (x0,y0).
%%%Returns:
%%% binArray: an integer matrix same size as sz with labels of the bin
%%% index assigned to each pixel. Index of 0 are for pixels not to be
%%% counted (e.g. outside of the frame or wrong side of the origin)

%Define the coordinates to be manipulated (in spatial coordinates)
sizeY = sz(1);
sizeX = sz(2);
[X,Y] = meshgrid(0.5:(sizeX-0.5), 0.5:(sizeY-0.5));

%Subtract the origin point from the coordinate array
X0 = X - x0;
Y0 = Y - y0;
%Define the vector describing the axis and the projection matrix P used to
%map each pixel coordinate onto this vector
v = [x1 - x0;
     y1 - y0];
P = (v*v')/(v'*v);

%Get the projection of each pixel coordinate onto the line
Xp = P(1,1) * X0 + P(1,2) * Y0;
Yp = P(2,1) * X0 + P(2,2) * Y0;
%Calculate the signed distance in microns, i.e. distance along the line to the origin,
%negative if a point is in the opposite direction as the vector defined by
%(x0,y0),(x1,y1)
d = umPerPixel * sign(v(1)*Xp + v(2) * Yp).* sqrt(Xp.^2 + Yp.^2);

%Define bins
edges = 0:umPerBin:(max(d(:))+umPerBin);
[~,~,binIndex] = histcounts(d(:),edges);
binArray = uint16(reshape(binIndex,sz(1),sz(2)));

