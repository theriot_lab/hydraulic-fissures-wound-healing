%% ==== Initial Setup ====
%%% Change these parameters to set for al the subsequent sections
run_initial_setup()
UM_PER_BIN = 5;
UM_PER_PIXEL = 0.3394;
DO_PLOT = true; %set to false if you want to disable some of the diagnostic plots
kymographUseList = 1:3; %Choose which kymographs in the dataset you want to process
                        % e.g. if you have 5 and want to skip the 4th, this
                        % variable should be [1 2 3 5] or [1:3 5]

%% ==== Calculate Kymographs ====
%%% This script takes in a series of ridge detection images and computes a
%%% kymograph from them. For this I also defined a csv file that contained
%%% the coordinates of a line I wished to use to define the axis of the
%%% fish, along which I would average the signal from the ridge detection
%%% filter to collapse the data into 1 spatial dimension (and time).

%%% This script assumes that you have saved your data according to the
%%% previous script (separationDetection), so that there is a .mat file
%%% containing the ridge detection result with the suffix
%%% '_FiberMetric.mat' in the appropriate location.

%%% --- INITIAL SETTINGS AND PARAMETERS ---
%%%Specify the fibermetric version to be analyzed
inputSubfolder = 'output_Cal18-28_Detect8-36'; 

%%%Optionally exclude certain movies (more important with wide thickness
%%%detection, as these are more sensitive to surfcut and unevenness issues)
conditional_exclusion = false;

%%%Choose whether to manually perform dorsal-ventral cropping (also more
%%%important with wide thickness detection, which is more variable near wound)
dv_mask = false;

%%%Choose whether to align kymographs by the anterior wound edge  (this does
%%%not affect "fibermetric_mean"
wound_aligned = true; %otherwise the entire field of view is included

%%%Location of the input file with info about the dataset
inputFiles = {['/Users/enorby/Thesis/Analysis/InputOutput/',...
             'Annotations_for_MS-CP_added20220126.csv'],...
             ['/Users/enorby/Thesis/Analysis/InputOutput/',...
             'Annotations_for_AK_2020-02-08.csv'],...
             ['/Users/enorby/Thesis/Analysis/InputOutput/',...
             'Annotations_20210914_combined_fix_anteror_wound.csv'],...
             ['/Users/enorby/Thesis/Analysis/InputOutput/',...
             'Annotations-Added-ForRac1_asof20220111.csv'],...
             ['/Users/enorby/Thesis/Analysis/InputOutput/',...
             'Annotations_for_AK_2020-09-20.csv']
             };
         
         
%%% ------------ END PARAMETERS ------------------
%Read in the input data   
inputFile = inputFiles{1};
opts = detectImportOptions(inputFile,'Delimiter',',');
inputData = readtable(inputFile, opts);
         
for f = 2:numel(inputFiles)
    inputFile = inputFiles{f};
    inputData_new = readtable(inputFile);
    inputData = vertcat(inputData,inputData_new);
end
%%
%%% --- Kymograph calculations ---

%Define a struct that will contain a kymograph for each dataset as well as
%other useful info.
sepKymographList = struct('kymograph',[],'T',[],'D',[],'label',[],...
    'datasetName',[],'fibermetric_mean', []);
sepKymographList(size(inputData,1)).kymograph = [];

for k = 1:size(inputData,1)
    %Determine if movie is to be excluded
    if inputData.exclude_always(k) || (conditional_exclusion && inputData.exclude_wide_params(k))
        continue
    end

    %Locate and load the ridge detection result, saved by separationDetection.m
    [parentDir,pathName] = fileparts(inputData.pathToImage{k});
    consensusArraySaveName = sprintf('%s_FiberMetric.mat',pathName);
    sepKymographList(k).datasetName = consensusArraySaveName;
    consensusArraySavePath = fullfile(parentDir,inputSubfolder,consensusArraySaveName);
    load(consensusArraySavePath,'consensusStack');
    %Also locate the original images that were used to compute the ridge
    %detection result - EL edited
    pathToReg = sprintf('%s_%s',inputData.pathToImage{k}(1:end-4),inputData.registration_outputSuffix{k});
    fileList = dir([pathToReg '/*.tif']);
    fileList = {fileList.name}; %get just the filenames into a cell array

    %Load the original stack. Any areas that are 0 (due to image shift
    %during registration) should not be used in the kymograph (set to nan)
    %imStack = loadImageStackFromFileList(inputData.pathToImage{k}, fileList);
    imStack = loadImageStack(pathToReg); %replaced the line above, EL
    consensusStack(imStack==0) = nan;
    %get the dimensions of a single frame in this stack
    sizeSlice = [size(consensusStack, 1), size(consensusStack, 2)];
    
    
    %frame 1 subtraction to remove stable structures
    original_frame1 = consensusStack(:,:,1);
    for slice = 1:size(consensusStack,3)
        consensusStack(:,:,slice) = consensusStack(:,:,slice) - original_frame1;
    end
    consensusStack(consensusStack<0) = 0;
    
    
    if dv_mask
        %ask user to crop between lacerations
        figure(1); 
        ax1 = imagesc(imStack(:,:,end));
        rectangle = drawrectangle;
        box = rectangle.Position;
        mask = nan(size(consensusStack));
        mask(box(2):box(2)+box(4)-1,box(1):box(1)+box(3)-1,:) = 1;
        consensusStack = consensusStack.*mask;
        consensus8 = uint8(255*consensusStack);
        
        maskedSaveName = sprintf('%s_Masked%s.tif',pathName, datestr(now,'yyyymmdd'));
        maskedImSavePath = fullfile(parentDir,inputSubfolder,maskedSaveName);

        options.overwrite = true;
        saveastiff(consensus8, maskedImSavePath, options);
    end

    %These coordinates define the line to be used for binning the kymograph
    x1 = inputData.apaxis_x0(k);
    y1 = inputData.apaxis_y0(k);
    
    if wound_aligned
        x_p = inputData.apaxis_x1(k);
        y_p = inputData.apaxis_y1(k);
        ap_slope = (y_p - y1)/(x_p - x1);
        ap_intercept = y1 - ap_slope*x1;

        x0 = inputData.wound_anterior_x(k);
        y0 = round(ap_slope*x0 + ap_intercept);
    else
        x0 = inputData.apaxis_x1(k);
        y0 = inputData.apaxis_y1(k);
    end
    
    %Convert pixel indexes to the indexes of the bin that pixel is in (based 
    %(on the line you specified)
    micronBinsArray = calculateDistanceAlongLine(sizeSlice, x0, y0, x1, y1, ...
                                                 UM_PER_PIXEL,UM_PER_BIN);
    kymograph = calculateKymographFromBins(consensusStack,...
                                                        micronBinsArray);

    %Restrict to the part that is actually covered by this specific dataset
    %sizeT is the number of frames in the ridge detection stack
    tInt = inputData.time_interval_s(k)/60; %input is in seconds, plotting is in minutes
    if tInt == 0
        tvec = [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,11.5,12,12.5,13,13.5,14,14.5,15,16,17,18,19,20,22,24,26,28,30];
    else
        tvec = 0:tInt:44.5;
    end
    sizeT = numel(fileList);
    %initialT is the frame corresponding to 0mpw. It is 0-indexed
    initialT = inputData.firstTrueFrame(k);
    %startT is the first frame (post-wounding) which was actually tracked
    %in the algorithm (some early frames may be dropped because the fish
    %was in the wrong position). This is 0-indexed
    startT = inputData.firstTrackedFrame(k);
    t = tvec((startT - initialT + 1) : (sizeT + (startT - initialT)));
    d = 0 : UM_PER_BIN : (UM_PER_BIN * (max(micronBinsArray(:)) - 1));
    %Create 2D arrays with the time and space positions of each point in
    %the kymograph
    [T, D] = meshgrid(t, d);
    %Save the data to the struct
    sepKymographList(k).kymograph = kymograph;
    sepKymographList(k).T = T;
    sepKymographList(k).D = D;
    sepKymographList(k).label = inputData.label{k};
    sepKymographList(k).datasetName = consensusArraySaveName;
    sepKymographList(k).fibermetric_mean = nanmean(nanmean(consensusStack));
    
    if DO_PLOT
        figure(2)
        p = numSubplots(size(inputData, 1));
        subplot(p(1), p(2), k)
        hold on
        surf(T, D, kymograph), view(2)
        ax = gca;
        colormap(ax, magma);
        shading interp
        %Display part of the save name as the title, you may want to change
        %which subset of the string is displayed. Intepreter none makes
        %sure that underscores don't cause subscripting
        title(consensusArraySaveName(1:end-35), 'Interpreter', 'none');
        %Plot transition points on top
        idxList = findTransitionPoints(kymograph);
        %plot3(t(idxList), d,50*ones(numel(d)),'c.','MarkerSize',20),view(2)
    end
end

%Save the data
if dv_mask
    mask_suffix = 'd-v-masked';
else
    mask_suffix = '';
end
if wound_aligned
    savename = sprintf('%s_%s_separationKymographs_woundAligned_%s.mat',...
        datestr(now,'yyyymmdd'), inputSubfolder(8:end), mask_suffix);
else
    savename = sprintf('%s_%s_separationKymographs_notAligned_%s.mat',...
        datestr(now,'yyyymmdd'), inputSubfolder(8:end), mask_suffix);
end
save(savename,'sepKymographList');
%% ==== Fit Line to Kymographs using RANSAC ====

%%% Use this script to fit a line to the first portion of the kymograph
%%% using a robust algorithm. 

%%%-------INITIAL PARAMETERS AND SETTINGS----------
%Because the fitting algorithm is not deterministic, specify the number of
%times to run it to get a consensus value for the parameters.
nReps = 100;
%Restrict analysis to a portion of the kymograph (essentially crop out the
%time range (t) and distance range (d) you are interested in
t = (0:0.5:29)'; %just pick the times you want
dbins = (0:20)'; %in units of bins, can change to microns later

%%%-----------SCRIPT---------------
%Load the kymograph data from scriptMakeKymographs.m
load('20210208_separationKymographs.mat');

%Set up to store the values of the parameters, and make the struct into an
%array of the appropriate size
paramsList = struct('slope',[], 'intercept', []);
paramsList(numel(kymographUseList), 1).slope = [];

%Set up grids to allow all kymographs to have the exact same timing for
%easy averaging
[T, Dbins] = meshgrid(t, dbins);

for k = 1:numel(kymographUseList)
    kIdx = kymographUseList(k);
    kym = sepKymographList(kIdx).kymograph;
    %Interpolate the kymograph so they all have the same coordinates.
    kym = interp2(sepKymographList(kIdx).T, double(sepKymographList(kIdx).D),...
                  kym, T, UM_PER_BIN * Dbins, 'linear', nan);
    %Find column coordinates of the transition points for each row
    idxList = findTransitionPoints(kym);
    idxNotNan = find(~isnan(idxList));
    idxList = idxList(idxNotNan);
    [fittedParams, inlierIdx] = fitLineWithRansac(t(idxList), dbins(idxNotNan), nReps);
    paramsList(k).slope = fittedParams(1);
    paramsList(k).intercept = fittedParams(2);
    %CODE FOR PLOTTING, JUST FIRST KYMOGRAPH AS AN EXAMPLE
    if k == 1 && DO_PLOT
        figure, 
        hold on
        %Run Ransac one more time just to get some inliers for plotting
        %NOTE: these inliers may not match those used to get the parameters
        %above! If it looks weird, try running the script again :P
        points = [t(idxList), dbins(idxNotNan)];
        inlierPoints = points(inlierIdx, :);
        %Plot the kymograph
        imagesc('XData', sepKymographList(kIdx).T(1, :), ...
                'YData', sepKymographList(kIdx).D(:, 1), ...
                'CData', sepKymographList(kIdx).kymograph);
        ax = gca;
        ax.YDir = 'normal';
        colormap(ax, magma(256));
        caxis([min(kym(:)), max(kym(:))]); %set dynamic range for colors
        %Plot the inlier points
        plot(inlierPoints(:, 1), UM_PER_BIN * inlierPoints(:, 2), ...
             'g.', 'MarkerSize', 10);
        %Plot the outlier points
        plot(points(~inlierIdx, 1), UM_PER_BIN * points(~inlierIdx, 2), ...
            'x', 'MarkerSize', 12, 'Color', [1,1,1]);
        %Plot the line fit by RANSAC
        x = min(inlierPoints(:, 1)) : max(inlierPoints(:, 1));
        y = UM_PER_BIN * (fittedParams(1) * x + fittedParams(2));
        plot(x, y, 'c-', 'LineWidth', 2.5)
        
        %Plotting details (may need to change for your plot!
        xlim([0, 37])
        ylim([0, 350])
        xlabel('Time post wounding (min)');
        ylabel('Distance from wound (�m)');
        xticks([0,10,20,30]);
        ax.FontSize = 10;
    end
end
%% ==== Gather manual input about kymographs for guided least squares fitting ====
%%% Use this code to input the valid data for least squares fitting

%%%-------INITIAL PARAMETERS AND SETTINGS----------
%Restrict analysis to a portion of the kymograph (essentially crop out the
%time range (t) and distance range (d) you are interested in
t = (0:0.5:44.5)'; %just pick the times you want
dbins = (0:60)'; %in units of bins, can change to microns later

kymographFile = '20210826_separationKymographs.mat';

%Load the kymograph data from scriptMakeKymographs.m
load(kymographFile);

%%%-----------SCRIPT---------------

%Ask for initials of current user
userInits = inputdlg("What are your initials?","Record user");

%Set up to store the values of the parameters, and make the struct into an
%array of the appropriate size
paramsList = struct('slope',[], 'intercept', [], 'all_points', [],...
    'valid_for_fit', [], 'points_for_fitting', []);
paramsList(numel(kymographUseList), 1).slope = [];

%Set up grids to allow all kymographs to have the exact same timing for
%easy averaging
[T, Dbins] = meshgrid(t, dbins);

%Set up a random order for viewing kymographs
kymographPermutedUseList = randperm(numel(kymographUseList));

for kRand = 1:numel(kymographPermutedUseList)
    close all
    k = kymographPermutedUseList(kRand);
    kIdx = kymographUseList(k);
    kym = sepKymographList(kIdx).kymograph;
    %Interpolate the kymograph so they all have the same coordinates.
    kym = interp2(sepKymographList(kIdx).T, double(sepKymographList(kIdx).D),...
                  kym, T, UM_PER_BIN * Dbins, 'linear', nan);
    %Make a nice figure of transition points and kymograph for annotation
    kymPointsFig = displayPointsOnKymograph(kym, T, Dbins, UM_PER_BIN); 
    
    %Store all points
    points = [kymPointsFig.Children(1).XData', kymPointsFig.Children(1).YData'];
    paramsList(k).all_points = points;
    
    %Ask user whether there is a signal that is valid to fit
    question = sprintf('Is there a signal that is valid to fit? \nAnswer "No" if all transition points are near t=0');
    validityAnswer = questdlg(question, 'Check validity', 'Yes', 'No', 'Yes');
    switch validityAnswer
        case 'Yes'
            paramsList(k).valid_for_fit = true;
        case 'No'
            paramsList(k).valid_for_fit = false;
            continue
    end
  
    %Ask user for end of propagation
    kymPointsFig.Title.String = 'Click on the furthest point in the linear propagation (max on the y axis)';
    endOfPropagation = drawpoint(kymPointsFig);
    
    %Remove (set to NaN) points that are at higher d values than the end of propagation
    endDistance = endOfPropagation.Position(2);
    eliminate = find(points(:,2)>endDistance);
    pointsForFitting = points;
    pointsForFitting(eliminate,:) = nan;
    
    %call function to eliminateOutliers (Andrew)
    %Ask user to select outlier points to exclude from least squares
    %regression analysis
    kymPointsFig.Title.String = 'Select outlier points with the brush tool';
    [kymPointsFig, outlierIdx] = markOutliers(kymPointsFig);
    pointsForFitting(outlierIdx,:) = nan;
    
    %Save the final list of points to be fit
    paramsList(k).points_for_fitting = pointsForFitting;
    
    %Perform least squares regression on the selected data points
    %%% The fit is performed "inverted," i.e. t ~ m*d + b, because for the "natural" model,
    %%% d ~ m*t + b, the data often fall along a vertical line, and therefore
    %%% are fit poorly
    linearModel = fitlm(pointsForFitting(:,2), pointsForFitting(:,1));

    %Save the info to the ParamsList struct
    invertedIntercept = linearModel.Coefficients.Estimate(1);
    invertedSlope = linearModel.Coefficients.Estimate(2);
    paramsList(k).slope = 1/invertedSlope;
    %Note: the "inverted" intercept is the t-intercept which is what we
    %want
    paramsList(k).intercept = invertedIntercept;
    
end

%save everything to a .mat file with user_inits
savename = sprintf('%s_separationParameters_%s.mat',datestr(now,'yyyymmdd'),userInits{1});
save(savename,'paramsList');

%% ==== Combine/plot guided fits from multiple users ==== 
%%% After multiple users guide the fitting, use this script to combine data
%%% for plotting and stats
%Struct to store all measurements (from different users) 
%of each biological sample (kymograph)
combinedParamsList = struct('slope',[],'intercept',[],'valid_for_fit',[]);
%Struct to store the averaged measurments for each sample
mainParamsList = struct('slope',[],'intercept',[],'valid_for_fit',[]);

%Input the .mat files to be combined
paramsLists_to_load = {'20210912_separationParameters_AK.mat';
                       '20210912_separationParameters_AK2.mat';
                       '20210912_separationParameters_AK3.mat';
                       '20210912_separationParameters_AK4.mat'};
load(paramsLists_to_load{1});
nSamples = size(paramsList,1); %number of different kymographs 
nMeasurements = numel(paramsLists_to_load); %number of different measurements (from different scorers) of each kymograph
combinedParamsList(nSamples,1).slope = [];
mainParamsList(nSamples,1).slope = [];
%Combine all measurements of each sample into vecotrs
for measurementIdx=1:nMeasurements
    load(paramsLists_to_load{measurementIdx});
    for sampleIdx = 1:nSamples
        combinedParamsList(sampleIdx).slope = ...
           [combinedParamsList(sampleIdx).slope paramsList(sampleIdx).slope];
        combinedParamsList(sampleIdx).intercept = ...
            [combinedParamsList(sampleIdx).intercept paramsList(sampleIdx).intercept];
        combinedParamsList(sampleIdx).valid_for_fit = ...
            [combinedParamsList(sampleIdx).valid_for_fit paramsList(sampleIdx).valid_for_fit];
    end
end
%Average together the measurements for each sample
for sampleIdx = 1:nSamples
    mainParamsList(sampleIdx).slope = mean(combinedParamsList(sampleIdx).slope);
    mainParamsList(sampleIdx).intercept = mean(combinedParamsList(sampleIdx).intercept);
    %Look for any measurements that were invalid for this sample
    mainParamsList(sampleIdx).valid_for_fit = all(combinedParamsList(sampleIdx).valid_for_fit);
end

%%% load the outputs from the previous section with matching initials


% ==== Plot Different Parameters ====
%%% Use this script to plot the parameters for different data sets, to get
%%% a sense for how quickly separation is proceeding
valid_fit = [mainParamsList.valid_for_fit];
slopeList = [mainParamsList.slope]; %units of bins per minute
interceptList = [mainParamsList.intercept];

startTime = interceptList(valid_fit); %units of minutes, filter only valid fits


initialSlope = UM_PER_BIN * slopeList(valid_fit); %units of um per minute

figure('Position', [200, 200, 130, 137]);
%Plot the initial slope
subplot(1,2,1)
hold on
plot([-1.5,1.5],mean(initialSlope)*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',1.5)
plot([-1.2,1.2],(mean(initialSlope) + std(initialSlope))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',0.75)
plot([-1.2,1.2],(mean(initialSlope) - std(initialSlope))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',0.75)
xx = rand(1,numel(initialSlope)) - 0.5;
plot(xx, initialSlope, 'k.','MarkerSize',10);
xlim([-2,2])
%ylim([0,90]) %may need to change for your data!
xticks([])
ylabel('Propagation speed (�m/min)');
xlabel(sprintf('%0.0f�%0.0f �m/min',mean(initialSlope), std(initialSlope)))
ax = gca;
ax.FontSize = 11;

%Plot the starting time
subplot(1,2,2)
hold on
plot([-1.5,1.5],mean(startTime)*ones(1,2),'-','Color',[.4,.4,.4],'LineWidth',1.5)
plot([-1.2,1.2],(mean(startTime) + std(startTime))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',0.75)
plot([-1.2,1.2],(mean(startTime) - std(startTime))*ones(1,2),...
     '-','Color',[.6,.6,.6],'LineWidth',0.75)
xx = rand(1,numel(startTime)) - 0.5;
plot(xx, startTime, 'k.','MarkerSize',10);
xlim([-2,2])
%ylim([0,8]) %may need to change for your data!
xticks([])
ylabel('Separation initiation time (min post wound)');
xlabel(sprintf('%0.1f�%0.0f min',mean(startTime), std(startTime)))
ax = gca;
ax.FontSize = 11;
 
%% ==== Combine/Average Kymographs ====
%%% Use this code to make a single kymograph by averaging a series of
%%% kymographs point-by-point.

%%% ------INITIALIZATION--------
%Set the range of time (t) and space (d) you want to plot
tvec = (0:.5:29)'; %in minutes
dvec = (0:350)'; % in microns

[T, D] = meshgrid(tvec, dvec);
%Initialize a container to hold all the kymographs
meanKymographList = nan([size(T), numel(kymographUseList)]);
%Loop through kymographs
for k = 1:numel(kymographUseList)
    kIdx = kymographUseList(k);
    %Interpolate the kymograph so it's on the same coordinates as all the
    %others
    kymograph = interp2(sepKymographList(kIdx).T, ...
                        double(sepKymographList(kIdx).D),...
                        sepKymographList(kIdx).kymograph,...
                        T, D, 'linear', nan);
    meanKymographList(:, :, k) = kymograph;
end
%Average together all the kymographs
meanKymograph = nanmean(meanKymographList, 3);

if DO_PLOT
    figure('Position',[200,200,144,108]);
    imagesc('XData', T(1,:), 'YData', D(:, 1), 'CData', meanKymograph);
    ax = gca;
    colormap(ax, magma(256));
    %May need to change these plotting details for your data
    xlim([0, 29])
    ylim([0, 350])
    yticks([0, 100, 200, 300])
    xlabel('Time post wounding (min)');
    ylabel('Distance from wound (�m)');
    ax.FontSize = 11;
    ax.XColor = [0,0,0]; %make the 'black' fonts truly black
    ax.YColor = [0,0,0];
end
                             
