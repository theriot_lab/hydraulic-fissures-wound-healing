function run_initial_setup()
%%% Set up (mostly just make sure the folder is on your path)
pathCell = regexp(path, pathsep, 'split');
onPath = any(strcmp('numSubplots', pathCell));
if ~onPath
    addpath('./numSubplots');
end