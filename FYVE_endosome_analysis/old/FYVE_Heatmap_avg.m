% FYVE_heatmap_avg
clear all;
% close all;
%load the files
fldname ='E3_pooled/';
fldname1 = 'E3'; %for graphing
infld = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'raw distribution_crop/'];
infldlist = dir(infld);
if ~(isfolder(infldlist(3).name))
    infldlist = infldlist(3:end);
else
    infldlist = infldlist(4:end);
end

% Endosome parameters
pixelsize = 0.3394^2; %xy resolution of the image
interval = 0.5/pixelsize; % 0.5�m interval
%% old bins
databin = 0:interval:130; %15 �m biggest endosome possible

% %% new bins
databin1 = 1:3; % 0-1 �m2
databin2 = 4:11; % 1-5 �m2
% databin2 = 4:30; % 1-everything
databin3 = 12:21; % 5-10 �m2
databin4 = 22:30; % >10 �m2
databin = [databin1, databin2, databin3, databin4];

%% bin parameters
databin_um = databin.*pixelsize;
bininterval = 1:length(databin);
times = 1:30;

%%%Notes
% E3 Fish 8 is 1 min interval, Rest of the E3 is 0.5m interval so i
% sub-sampled the first 7 E3 fishes every other frame

%load the normalized excel files
cntr = 0; before_mat =[]; fish_avg = [];after_mat_0 =[]; after_mat_5 =[]; after_mat_10 =[];
after_mat_15 =[]; after_mat_20 =[]; after_mat_25 =[];

for i =1:length(infldlist)/2 %[1:3,8]
    cntr = cntr+1;
    %% POST WOUNDING
    fish_after = readmatrix(fullfile(infld,infldlist(i).name));
    %take avg for 5 min window for all size bins
    fish_after0 = mean(fish_after(bininterval,1:5),2); % Avg over  0-5 minutes for all the bins
    fish_after5 = mean(fish_after(bininterval,6:10),2); % Avg over  5-10 minutes for all the bins
    fish_after10 = mean(fish_after(bininterval,11:15),2); % Avg over  11-15 minutes for all the bins
    fish_after15 = mean(fish_after(bininterval,16:20),2); % Avg over 16-20 minutes for all the bins
    fish_after20 = mean(fish_after(bininterval,21:25),2); % Avg over  21-25 minutes for all the bins
    fish_after25 = mean(fish_after(bininterval,25:30),2); % Avg over  25-30 minutes for all the bins
    
    %storing all the data fishwise for all size bins
    after_mat_0 = [after_mat_0, fish_after0];
    after_mat_5 = [after_mat_5, fish_after5];
    after_mat_10 = [after_mat_10,fish_after10];
    after_mat_15 = [after_mat_15,fish_after15];
    after_mat_20 = [after_mat_20,fish_after20];
    after_mat_25 = [after_mat_25,fish_after25];
    
    
    %% BEFORE WOUNDING
    fish_before = readmatrix(fullfile(infld,infldlist(i+6).name));
    fish_before = fish_before(bininterval,:); % before movie frames with first 30 bins
    fish_before_mean = mean(fish_before,2);
    before_mat  = [before_mat, fish_before_mean];
    %     after_over_before = fish_after./fish_before_mean;
    %     fish_foldchange(bininterval,1:length(times),cntr) =  after_over_before;
    
end


%% Graphing Endosome number histogram with different size and time bins
% Databin1 for before and all the after time bins (fish is rows)
firstsizebin = [sum(before_mat(databin1,:),1); sum(after_mat_0(databin1,:),1); sum(after_mat_5(databin1,:),1); sum(after_mat_10(databin1,:),1);....
    sum(after_mat_15(databin1,:),1); sum(after_mat_20(databin1,:),1); sum(after_mat_25(databin1,:),1)];

firstsizebin_mean = mean(firstsizebin, 2);

%Databin2 for all the after time bins
secondsizebin = [sum(before_mat(databin2,:),1);sum(after_mat_0(databin2,:),1);sum(after_mat_5(databin2,:),1);sum(after_mat_10(databin2,:),1);....
    sum(after_mat_15(databin2,:),1); sum(after_mat_20(databin2,:),1); sum(after_mat_25(databin2,:),1)];

secondsizebin_mean = mean(secondsizebin, 2);
%Databin3 for all the after time bins
thirdsizebin = [sum(before_mat(databin3,:),1); sum(after_mat_0(databin3,:),1);sum(after_mat_5(databin3,:),1);sum(after_mat_10(databin3,:),1);....
    sum(after_mat_15(databin3,:),1); sum(after_mat_20(databin3,:),1); sum(after_mat_25(databin3,:),1)];

thirdsizebin_mean = mean(thirdsizebin, 2);

%Databin4 for all the after time bins
fourthsizebin = [sum(before_mat(databin4,:),1); sum(after_mat_0(databin4,:),1);sum(after_mat_5(databin4,:),1);sum(after_mat_10(databin4,:),1);....
    sum(after_mat_15(databin4,:),1); sum(after_mat_20(databin4,:),1); sum(after_mat_25(databin4,:),1)];

fourthsizebin_mean = mean(fourthsizebin,2);


%% Box Scatter plots with normalization
figure; cm = colormap(viridis(8));

%dummy variable to plot new graph for each databins no normalization
dummybin = firstsizebin(2:end,:); %now columns are the fishes
dummymean = firstsizebin_mean(2:end);
d = 10;
%plotting each time point
plot(1, dummybin(1,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(1,:), 'MarkerEdgeColor', 'k', 'LineWidth',2), hold on
plot(2, dummybin(2,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(2,:), 'MarkerEdgeColor', 'k', 'LineWidth',2),
plot(3, dummybin(3,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(3,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(4, dummybin(4,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(4,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(5, dummybin(5,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(5,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(6, dummybin(6,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(6,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(7, dummybin(7,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(7,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
%plotting mean trend line
plot(dummymean','h-','MarkerSize',d, 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'k', 'LineWidth', 3)
hold off
%graph beautification
% Set the axes properties
set(0,'DefaultAxesFontName', 'Cambria Math');
xlim(gca, [0.5 7.5])
set(gca,'FontSize',40,'LineWidth',2,'XTick',[0 1 2 3 4 5 6 7 8],...
    'XTickLabel',...
    {'','(-5)to0','1to5','6to10','11to15','16to20','20to25','26to30',''});
% Create ylabel
ylabel('Endosome Number','FontSize',50,'FontName','Cambria Math');

% Create xlabel
xlabel('Minutes Post Wounding');

% Create title
title({'>1 �m^2 Endosome Number ','Over Time (E3 Medium)'},'FontSize',50);

keyboard




%% Normalised line plots
% %dummy variable to plot new graph for each databins
% dummybin = fourthsizebin'; %now columns are the different time points for a current databin and rows are the fishes

%now we will plot the datapoints normalized to the unwounded
% figure;
% d = 30;
% % iterate through the fishes
% plot(dummybin(1,:)/dummybin(1,1),'-s', 'MarkerSize',d, 'MarkerFaceColor', cm(1,:), 'MarkerEdgeColor', 'k', 'LineWidth',2,'Color','k'), hold on
% plot(dummybin(2,:)/dummybin(2,1),'-s', 'MarkerSize',d, 'MarkerFaceColor', cm(2,:), 'MarkerEdgeColor', 'k', 'LineWidth',2, 'Color','k')
% plot(dummybin(3,:)/dummybin(3,1),'-s', 'MarkerSize',d, 'MarkerFaceColor', cm(3,:), 'MarkerEdgeColor', 'k', 'LineWidth',2, 'Color','k')
% plot(dummybin(4,:)/dummybin(4,1),'-s', 'MarkerSize',d, 'MarkerFaceColor', cm(4,:), 'MarkerEdgeColor', 'k', 'LineWidth',2, 'Color','k')
% plot(dummybin(5,:)/dummybin(5,1),'-s', 'MarkerSize',d, 'MarkerFaceColor', cm(5,:), 'MarkerEdgeColor', 'k', 'LineWidth',2, 'Color','k')
% plot(dummybin(6,:)/dummybin(6,1),'-s', 'MarkerSize',d, 'MarkerFaceColor', cm(6,:), 'MarkerEdgeColor', 'k', 'LineWidth',2, 'Color','k')
% plot(dummybin(7,:)/dummybin(7,1),'-s', 'MarkerSize',d, 'MarkerFaceColor', cm(7,:), 'MarkerEdgeColor', 'k', 'LineWidth',2, 'Color','k')
% plot(dummybin(8,:)/dummybin(8,1),'-s', 'MarkerSize',d, 'MarkerFaceColor', cm(8,:), 'MarkerEdgeColor', 'k', 'LineWidth',2, 'Color','k')
% hold off
% %graph beautification
% xt = {'','Unwounded','1-5 mpw', '6-10 mpw', '11-15 mpw', '16-20 mpw', '20-25 mpw','26-30 mpw',''};
% set(0,'DefaultAxesFontName', 'Cambria Math');
% set(gca,{'FontSize','FontWeight','TickLabelInterpreter'},{22,'bold','tex'});
% xlim([0.5, 7.5]);
% set(gca,'XTickLabel',xt,'FontSize',20, 'XTick',(0:8))
% title(('>10 �m Endosome Number Over Time (E3 Medium)'), 'FontSize', 30);
% ylabel('Fold Change of Endosome Number','FontSize',40)


%% Graphing Endosome number histogram

% cm = colormap(viridis(7));
% 1. plot before movie
meanbefore = mean(before_mat(:,1),2);
errbefore = (std(before_mat,1,2))./sqrt(size(before_mat,2));
errorbar(meanbefore,errbefore,'--s','LineWidth',3,'MarkerSize',3, 'Color', cm(1,:));
hold on

%now plot all the 5 min chunks of the after movie
% 2. 0-5 mins post wounding
meanafter0 = mean(after_mat_0,2);
errafter0 = (std(after_mat_0,1,2))./sqrt(size(after_mat_0,2));
errorbar(meanafter0,errafter0,'-s','LineWidth',3, 'MarkerSize',3, 'Color', cm(2,:));

% 3. 6-10 mins post wounding
meanafter5 = mean(after_mat_5,2);
errafter5 = (std(after_mat_5,1,2))./sqrt(size(after_mat_5,2));
errorbar(meanafter5,errafter5,'-s','LineWidth',3,'MarkerSize',3, 'Color', cm(3,:));

% 4. 11-15 mins post wounding
meanafter10 = mean(after_mat_10,2);
errafter10 = (std(after_mat_10,1,2))./sqrt(size(after_mat_10,2));
errorbar(meanafter10,errafter10,'-s','LineWidth',3, 'MarkerSize',3, 'Color', cm(4,:));

% 5. 16-20 mins post wounding
meanafter15 = mean(after_mat_15,2);
errafter15 = (std(after_mat_15,1,2))./sqrt(size(after_mat_15,2));
errorbar(meanafter15,errafter15,'-s','LineWidth',3, 'MarkerSize',3, 'Color', cm(5,:));

% 6. 21-25 mins post wounding
meanafter20 = mean(after_mat_20,2);
errafter20 = (std(after_mat_20,1,2))./sqrt(size(after_mat_20,2));
errorbar(meanafter20,errafter20,'-s','LineWidth',3, 'MarkerSize',3, 'Color', cm(6,:));

% 7. 26-30 mins post wounding
meanafter25 = mean(after_mat_25,2);
errafter25 = (std(after_mat_25,1,2))./sqrt(size(after_mat_25,2));
errorbar(meanafter25,errafter25,'-s','LineWidth',3, 'MarkerSize',3, 'Color', cm(7,:));

%% Rounding off bininteval_um for graphing
legend([fldname1 ' Before Wounding'], [fldname1 ' 0-5min After Wounding'],[fldname1 ' 6-10min After Wounding'], ....
    [fldname1 ' 11-15min After Wounding'], [fldname1 ' 16-20min After Wounding'], [fldname1 ' 21-25min After Wounding'],...
    [fldname1 ' 26-30min After Wounding'],'FontSize',24);
%         set(gca,'XTick',bininterval);
set(gca,'XTick',bininterval(1:2:end));
xaxislabel = string(databin_um);
%         xaxislabel = string(databin_um);
set(gca,'XTickLabel',xaxislabel(1,bininterval(1:2:end)),'FontSize',22, 'YScale', 'log')
grid on;
xlabel('Endosome Area (um^2)','FontSize',28); ylabel('Endosome Number','FontSize',40)
title([' Not Wounded Area Histogram of Endosome Size Before and After Wounding (N(fish)) = ' num2str(length(infldlist)/2) ], 'FontSize', 30, 'FontWeight', 'normal')
keyboard



%% Heat Map
kymograph = mean(fish_foldchange,3);
figure; imagesc('XData', times(1,:), 'YData', (databin(bininterval)+(interval/2))*pixelsize, 'CData', log10(kymograph));
ax = gca;
ax.YDir = 'normal'; % default Values increase from bottom to top (2-D view) or front to back (3-D view).
colormap(ax, redblue(256)); %this was downloaded from mathworks https://www.mathworks.com/matlabcentral/fileexchange/51986-perceptually-uniform-colormaps?s_tid=mwa_osa_a
% overalllimit = max(abs(max(kymograph(:))),abs(min(kymograph(:))));
% caxis([(min(kymograph(:))), (max(kymograph(:)))]);
% caxis([-overalllimit,overalllimit]); %Set dynamin range for colours %Set dynamin range for colours
ylim ([databin(bininterval(1))*pixelsize, databin(bininterval(end))*pixelsize])
xlim ([0 30])
xlabel('Time (min)','FontSize',16)
ylabel('Mean Convex Area Bins �m^2 ','FontSize',16)
title(' E3 log 10 Mean Convex Area Binned ', 'FontSize',12);
caxis([-1,1])
colorbar;

%% Box Plot
% plot box plot (lot of stuff taken from stackoverflow)
% figure; boxplot(dummybin, xt)
% ax = gca;
% set(0,'DefaultAxesFontName', 'Cambria Math');
% set(ax,{'FontSize','FontWeight','TickLabelInterpreter'},{24,'bold','tex'});
% bx = findobj('tag','boxplot');
% set(bx.Children,'LineWidth',3);
% set(bx.Children,'Color','k');