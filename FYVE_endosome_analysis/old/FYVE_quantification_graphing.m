% FYVE_quantification_graph
clear all; close all;
%load the files
folderList = {['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
    'endosome_quantification/raw distribution/',...
    'Raw_ConvexArea_Binned_20210924_p63-gal4_UAS-2x-FYVE_UAS-mCh_3dpf_laceration_1_MMStack.xlsx'],...
    ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
    'endosome_quantification/raw distribution/',...
    'Raw_ConvexArea_Binned_20210924_p63-gal4_UAS-2x-FYVE_UAS-mCh_3dpf_laceration_2_MMStack.xlsx'],...
    ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
    'endosome_quantification/raw distribution/',...
    'Raw_ConvexArea_Binned_20210924_p63-gal4_UAS-2x-FYVE_UAS-mCh_3dpf_laceration_3_MMStack.xlsx'],...
    ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
    'endosome_quantification/raw distribution/',...
    'Raw_ConvexArea_Binned_20210924_p63-gal4_UAS-2x-FYVE_UAS-mCh_3dpf_laceration_4_MMStack.xlsx']};

%saving directory
outfilename = {'20210924_p63-gal4_UAS-2x-FYVE_UAS-mCh_3dpf_laceration_1_MMStack',...
    '20210924_p63-gal4_UAS-2x-FYVE_UAS-mCh_3dpf_laceration_2_MMStack',...
    '20210924_p63-gal4_UAS-2x-FYVE_UAS-mCh_3dpf_laceration_3_MMStack',...
    '20210924_p63-gal4_UAS-2x-FYVE_UAS-mCh_3dpf_laceration_4_MMStack'};

outpath = '/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/';


%%% read the file
%list of folders
foldersToUse = 1:numel(folderList);
for i = foldersToUse
    fish = readmatrix(folderList{i});
    
    %normalize everything to first frame
    fish_normalized =[];
    for j = 2:size(fish,2)
        fish_normalized = [fish_normalized, ((fish(:,j)-fish(:,1)))];
    end
    
    
    %% Parameter from FYVE quantification
    interval = 20;
    databin = 0:interval:600;
    pixelsize = 0.3394^2; %xy resolution of the image
    times = (2:size(fish,2)); %time resolution is 30s (E3) [(2:size(fish,2)) * 0.5], 1 min (Rac1) [(2:size(fish,2))]
    
    %% Display small endosomes
    savename1 = ['SmallEndosomeArea_', outfilename{i}];
    kymograph_small = fish_normalized(1:5,:);
    figure; imagesc('XData', times(:,1), 'YData', (databin(1:5)+(interval/2))*pixelsize, 'CData', kymograph_small);
    ax = gca;
    ax.YDir = 'normal'; % default Values increase from bottom to top (2-D view) or front to back (3-D view).
    colormap(ax, redblue(256)); %this was downloaded from mathworks https://www.mathworks.com/matlabcentral/fileexchange/51986-perceptually-uniform-colormaps?s_tid=mwa_osa_a
    overalllimit = max(abs(max(kymograph_small(:))),abs(min(kymograph_small(:))));
    caxis([-overalllimit,overalllimit]); %Set dynamin range for colours
    ylim([databin(1)*pixelsize, databin(5)*pixelsize])
    xlabel('Time (min)','FontSize',16)
    ylabel('Convex Area Bins �m^2 [Small Endosomes] ','FontSize',16)
    title(['ConvexArea_Binned ', outfilename{i}], 'FontSize',12);
    colorbar;
    saveas(gca,[outpath,'normalized to first time', savename1]);
    
    %% Display big endosomes
    savename2 = ['LargeEndosomeArea_', outfilename{i}];
    kymograph_large = fish_normalized(6:20,:);
    figure; imagesc('XData', times(:,1), 'YData', (databin(6:20)+(interval/2))*pixelsize, 'CData', kymograph_large);
    ax = gca;
    ax.YDir = 'normal'; % default Values increase from bottom to top (2-D view) or front to back (3-D view).
    colormap(ax, redblue(256)); %this was downloaded from mathworks https://www.mathworks.com/matlabcentral/fileexchange/51986-perceptually-uniform-colormaps?s_tid=mwa_osa_a
    overalllimit = max(abs(max(kymograph_large(:))),abs(min(kymograph_large(:))));
    caxis([-overalllimit,overalllimit]); %Set dynamin range for colours %Set dynamin range for colours
    ylim ([databin(6)*pixelsize, databin(20)*pixelsize])
    xlabel('Time (min)','FontSize',16)
    ylabel('Convex Area Bins �m^2 [Large Endosomes] ','FontSize',16)
    title(['ConvexArea_Binned ', outfilename{i}], 'FontSize',12);
    colorbar;
    saveas(gca,[outpath,'normalized to first time',savename2]);
end