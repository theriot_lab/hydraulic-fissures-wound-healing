%%%% GFP-FYVE 40x wounding endosome quantification
%% initialize
clear all; close all;

%%%Notes
% E3 Fish 8 is 1 min interval, Rest of the E3 is 0.5m interval
%%% add bfmatlab in your MATLAB path and


%% Define the folders to work on
fldname ='BLEBB/';
infolder = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/20211026_p63-gal4_UAS-2x-FYVE_UAS-mCh_15minpreincubate-Blebb/',  fldname];
folderList = dir(infolder);
folderList = folderList(3:end);
pixelsize = 0.3394^2; %xy resolution of the image
interval = 0.5/pixelsize; % 0.5�m interval
databin = 0:interval:130; %15 �m biggest endosome possible
databin_um = databin.*pixelsize;
%%% read the file
%list of folders
foldersToUse = 1:(numel(folderList)/2); %numel(folderList);
folderListBefore = ((numel(folderList)/2)+1):numel(folderList);
ChannelstoUse = 1; %2 for E3 & RAC1, 1 for DMSO, ISO, BLEBB %FYVE
outfilename = {'Fish_1_Pos0_20211026',...
    'Fish_1_Pos1_20211026',...
    'Fish_2_Pos0_20211026',...
    'Fish_2_Pos1_20211026',...
    'Fish_3_Pos0_20211026',...
    'Fish_3_Pos1_20211026'}; %,...
%      'Fish_5_Pos0_20211019',...
%      'Fish_5_Pos2_20211019'};



mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/',fldname]);
mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'segmented/']);
mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/',fldname,'raw distribution/']);
mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'normalized to before movies/']);


outpath = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname];
%iterate througth folders
for i = foldersToUse
    stk = bfGetReader(fullfile(infolder,folderList(i).name));
    %extract paramaters
    sizeT = stk.getSizeT();
    
    %open before movies
    bb = folderListBefore(i);
    stk_before = bfGetReader (fullfile(infolder,folderList(bb).name));
    sizeT_before = stk_before.getSizeT();
    y_before_mat =[]; y_wound_mat = []; y_wound_normalized =[];
    
    %% iterate through time for 'before' movies to get baseline
    %     if i < 8
    %         jj = 1:2:sizeT_before;
    %         j = 1:2:sizeT;
    %     else
    %         jj = 1:sizeT_before;
    %         j = 1:sizeT;
    %     end
    for jj = 1:sizeT_before
        stk_frame = bf_getFrame(stk_before,1,ChannelstoUse, jj);
        %extra correction for blebb movies
%         blebby = stk_frame>10000;
%         stk_frame(blebby)=0;
        
        stk_flat = imflatfield(stk_frame, 30); % flat fielding to correct illumation and/or expression differences
        stk_filter = imtophat(stk_flat,strel('sphere',3)); % clear inter-spot haze
        level = graythresh(stk_filter);
        %change for blebb
%         level = level/2;
        stk_binary = imbinarize(stk_filter,level); % binarize low = strict
        % writing the segmented (before wounding) files
        imwrite(uint16(stk_binary), fullfile(outpath, 'segmented',['before_segmented_',outfilename{i}]),'tiff','Compression','none','Writemode','append');
        
        
        %% measuring properites of the endosomes (BEFORE WOUNDING)
        stk_endo = regionprops('table',stk_binary, 'ConvexArea');
        %%% binning the data
        y_before = zeros(length(databin),1); %count of endosome of a particular convexarea bin
        ff = table2array(stk_endo);
        for k = 1:size(ff,1) %iterating through convexarea
            ffkth = ff(k,1); %kth endosome's convex area
            bin = ceil(ffkth/interval);
            if bin < (databin(end)/interval) %throw away stupendously large structures
                y_before(bin) = y_before(bin)+1; %storing the count/bin
            else
                continue
            end
        end
        y_before_mat = [y_before_mat, y_before];
    end
    %%Histogram of the endosome size distribtion before wounding
    baseline_vector = mean(y_before_mat,2);
    
    %% Saving the binned output as XLSX (RAW DISTRIBUTION BEFORE WOUNDING)
    writematrix(y_before_mat,fullfile(outpath, 'raw distribution', ['Before_Raw_ConvexArea_Binned_', outfilename{i},'.xlsx']))
    
    
    %% iterate through time for wounded movie
    stk_fish = table();
    for j = 1:sizeT
        stk_frame = bf_getFrame(stk,1,ChannelstoUse, j);
%         blebby = stk_frame>10000;
%         stk_frame(blebby)=0;
        stk_flat = imflatfield(stk_frame, 30); % flat fielding to correct illumation and/or expression differences
        stk_filter = imtophat(stk_flat,strel('sphere',3)); % clear inter-spot haze
        level = graythresh(stk_filter);
%         %change for blebb
%         level = level/2;
        stk_binary = imbinarize(stk_filter,level); % binarize low = strict
        
        % writing the segmented (wounded) files
        imwrite(uint16(stk_binary), fullfile(outpath, 'segmented',['after_segmented_',outfilename{i}]),'tiff','Compression','none','Writemode','append');
        
        %% measuring properites of the endosomes (AFTER WOUNDING)
        stk_endo = regionprops('table',stk_binary, 'ConvexArea'); % 'Area', 'MajorAxisLength'
        %%% binning the data
        y_wound = zeros(length(databin),1); %count of endosome of a particular convexarea bin
        ff = table2array(stk_endo);
        for k = 1:size(ff,1) %iterating through convexarea
            ffkth = ff(k,1); %kth endosome's convex area
            bin = ceil(ffkth/interval);
            if bin < (databin(end)/interval)
                y_wound(bin) = y_wound(bin)+1; %storing the count/bin%throw away stupendously large structures
            else
                continue
            end
        end
        
        %subtract the baseline and store
        y_wound_normalized  = [y_wound_normalized, (y_wound - baseline_vector)];
        % store the raw matrix
        y_wound_mat = [y_wound_mat, y_wound];
        
        disp (['Fish ' num2str(i) ' Frame ' num2str(j)])
    end
    
    %% Saving the binned output as XLSX (NORMALIZED TO BEFORE WOUNDING)
    writematrix(y_wound_mat,fullfile(outpath, 'raw distribution', ['After_Raw_ConvexArea_Binned_', num2str(interval), '_', outfilename{i},'.xlsx']))
    
    %% Saving the binned output as XLSX (RAW DISTRIBUTION AFTER WOUNDING)
    writematrix(y_wound_normalized,fullfile(outpath, 'normalized to before movies', ['Normalized_to_Before_ConvexArea_Binned_', num2str(interval), '_', outfilename{i},'.xlsx']))
    
end
%%






















%Extra Lines of code
%% Display small endosomes
%     savename1 = ['SmallEndosomeArea_', outfilename{i}];
%     times = (1:sizeT) * 0.5; %time resolution is 30s
%     pixelsize = 0.3394^2; %xy resolution of the image
%     kymograph_small = y_wound_normalized(1:5,:);
%     figure; imagesc('XData', times(:,1), 'YData', (databin(1:5)+(interval/2))*pixelsize, 'CData', kymograph_small);
%     ax = gca;
%     ax.YDir = 'normal'; % default Values increase from bottom to top (2-D view) or front to back (3-D view).
%     colormap(ax, redblue(256)); %this was downloaded from mathworks https://www.mathworks.com/matlabcentral/fileexchange/51986-perceptually-uniform-colormaps?s_tid=mwa_osa_a
%     overalllimit = max(abs(max(kymograph_small(:))),abs(min(kymograph_small(:))));
%     caxis([-overalllimit,overalllimit]); %Set dynamin range for colours
%     ylim([databin(1)*pixelsize, databin(5)*pixelsize])
%     xlabel('Time (min)','FontSize',16)
%     ylabel('Convex Area Bins �m^2 [Small Endosomes] ','FontSize',16)
%     title(['ConvexArea_Binned ', outfilename{i}], 'FontSize',12);
%     colorbar;
%     saveas(gca,[outpath,'normalized to before movies',savename1]);
%
%     %% Display big endosomes
%     savename2 = ['LargeEndosomeArea_', outfilename{i}];
%     kymograph_large = y_wound_normalized(6:20,:);
%     figure; imagesc('XData', times(:,1), 'YData', (databin(6:20)+(interval/2))*pixelsize, 'CData', kymograph_large);
%     ax = gca;
%     ax.YDir = 'normal'; % default Values increase from bottom to top (2-D view) or front to back (3-D view).
%     colormap(ax, redblue(256)); %this was downloaded from mathworks https://www.mathworks.com/matlabcentral/fileexchange/51986-perceptually-uniform-colormaps?s_tid=mwa_osa_a
%     overalllimit = max(abs(max(kymograph_large(:))),abs(min(kymograph_large(:))));
%     caxis([-overalllimit,overalllimit]); %Set dynamin range for colours %Set dynamin range for colours
%     ylim ([databin(6)*pixelsize, databin(20)*pixelsize])
%     xlabel('Time (min)','FontSize',16)
%     ylabel('Convex Area Bins �m^2 [Large Endosomes] ','FontSize',16)
%     title(['ConvexArea_Binned ', outfilename{i}], 'FontSize',12);
%     colorbar;
%     saveas(gca,[outpath,'normalized to before movies',savename2]);




%Storing the files
%            writetable(stk_endo, fullfile(outpath, ['frame_', num2str(j), '_', outfilename{i}, '.csv']));
%Summarizing the frame results
%         stk_frame_summary = table(size(stk_endo,1), mean(stk_endo.Area), mean(stk_endo.ConvexArea), mean(stk_endo.MajorAxisLength),...
%             'VariableNames',{'Endosome Number','Mean Endosome Area', 'Mean ConvexArea', 'Mean MajorAxisLength'});
%         stk_fish = [stk_fish;stk_frame_summary];



% folderList = {['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
%     '20211019_Separation_40xW/',...
%     '20211019_p63-gal4_UAS-2x-FYVE_UAS-mCh_60minpreincubate-Control_3dpf_laceration_after_1_MaxZ.tif'],...
%     ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
%     '20211019_Separation_40xW/',...
%     '20211019_p63-gal4_UAS-2x-FYVE_UAS-mCh_60minpreincubate-RAC1inhibitor_3dpf_laceration_after_1_MaxZ.tif'],...
%     ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
%     '20211019_Separation_40xW/',...
%     '20211019_p63-gal4_UAS-2x-FYVE_UAS-mCh_60minpreincubate-RAC1inhibitor_3dpf_laceration_after_2_MaxZ.tif'],...
%     ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
%     '20211019_Separation_40xW/',...
%     '20211019_p63-gal4_UAS-2x-FYVE_UAS-mCh_60minpreincubate-RAC1inhibitor_3dpf_laceration_after_3_MaxZ.tif'],...
%     ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
%     '20211019_Separation_40xW/',...
%     '20211019_p63-gal4_UAS-2x-FYVE_UAS-mCh_60minpreincubate-RAC1inhibitor_3dpf_laceration_after_4_MaxZ.tif'],...
%     ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
%     '20211019_Separation_40xW/',...
%     '20211019_p63-gal4_UAS-2x-FYVE_UAS-mCh_60minpreincubate-RAC1inhibitor_3dpf_laceration_after_5_MaxZ.tif']};
%
% folderListBefore = {['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
%     '20211019_Separation_40xW/',...
%     '20211019_p63-gal4_UAS-2x-FYVE_UAS-mCh_60minpreincubate-Control_3dpf_laceration_before_1_MaxZ.tif'],...
%     ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
%     '20211019_Separation_40xW/',...
%     'MAX_20211019_p63-gal4_UAS-2x-FYVE_UAS-mCh_60minpreincubate-RAC1inh...reincubate-RAC1inhibitor_3dpf_laceration_before_1_MaxZ.tif'],...
%     ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
%     '20211019_Separation_40xW/',...
%     'MAX_20211019_p63-gal4_UAS-2x-FYVE_UAS-mCh_60minpreincubate-RAC1inh...reincubate-RAC1inhibitor_3dpf_laceration_before_2_MaxZ.tif'],...
%     ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
%     '20211019_Separation_40xW/',...
%     'MAX_20211019_p63-gal4_UAS-2x-FYVE_UAS-mCh_60minpreincubate-RAC1inh...reincubate-RAC1inhibitor_3dpf_laceration_before_3_MaxZ.tif'],...
%     ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
%     '20211019_Separation_40xW/',...
%     'MAX_20211019_p63-gal4_UAS-2x-FYVE_UAS-mCh_60minpreincubate-RAC1inhibitor_3dpf_laceration_before_4_MaxZ.ome.tif'],...
%     ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/',...
%     '20211019_Separation_40xW/',...
%     'MAX_20211019_p63-gal4_UAS-2x-FYVE_UAS-mCh_60minpreincubate-RAC1inh...reincubate-RAC1inhibitor_3dpf_laceration_before_5_MaxZ.tif']};

