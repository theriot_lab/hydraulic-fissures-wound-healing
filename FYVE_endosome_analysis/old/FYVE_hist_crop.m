% FYVE_hist_crop
clear all;
close all;
%load the files
fldname ='DMSO/';
fldname1 = 'DMSO'; %for graphing
infolder = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'segmented/'];
infolder1 = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'segmented_crop/'];
folderList = dir(infolder);
if ~(isfolder(folderList(3).name))
    folderList = folderList(3:end);
else
    folderList = folderList(4:end);
end
% Endosome parameters
pixelsize = 0.3394^2; %xy resolution of the image
interval = 0.5/pixelsize; % 0.5�m2 interval
%% old bin
databin = 0:interval:130; %15 �m biggest endosome possible
% %% new bin
% databin1 = interval;
% databin2 = 5/pixelsize; % 5 �m2 interval
% databin3 = 10/pixelsize; % 10�m2 interval
% databin4 = 14.5/pixelsize; %14.5 �m2 interval
% databin = [databin1, databin2, databin3, databin4];

% histogra parameters
databin_um = databin.*pixelsize;
bininterval = 1:length(databin);
times = 1:30;

foldersToUse = 1:(numel(folderList)/2); %numel(folderList);
folderListBefore = ((numel(folderList)/2)+1):numel(folderList);

% mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/',fldname]);

mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'segmented_crop/']);
mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/',fldname,'raw distribution_crop/']);
mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/',fldname,'raw distribution_crop_notwounded/']);

outpath = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname];

%iterate througth folders
for i = foldersToUse
    stk= bfGetReader(fullfile(infolder,folderList(i).name));
    %extract paramaters
    sizeT = stk.getSizeT();
    
    %% iterate through time for wounded movie
    y_wound_mat = [];
    stk_fish = table();
    for j = 1:sizeT
        stk_frame = bf_getFrame(stk,1,1,j);
                woundcrop = imread(fullfile(infolder1,['notwoundcrop',folderList(i).name]));
        
        %% Crop out the wounded half
%         if j == 1
%             woundcrop = roipoly(imadjust(bf_getFrame(stk,1,1,1)));
%             notwound = ~(woundcrop);
%         end
        
        stk_binary = logical(immultiply(stk_frame, woundcrop));
        
        % writing the segmented (wounded) files
%         imwrite(uint16(woundcrop), fullfile(outpath, 'segmented_crop',['woundcrop',folderList(i).name]),'tiff','Compression','none','Writemode','overwrite');
%         imwrite(uint16(notwound), fullfile(outpath, 'segmented_crop',['notwoundcrop',folderList(i).name]),'tiff','Compression','none','Writemode','overwrite');
        
        %% measuring properites of the endosomes (AFTER WOUNDING)
        stk_endo = regionprops('table',stk_binary, 'ConvexArea');
        %%% binning the data
        y_wound = zeros(length(databin),1); %count of endosome of a particular convexarea bin
        ff = table2array(stk_endo);
        for k = 1:size(ff,1) %iterating through convexarea
            ffkth = ff(k,1); %kth endosome's convex area
            bin = ceil(ffkth/interval);
            %% old bin
            if bin < (databin(end)/interval)
                y_wound(bin) = y_wound(bin)+1; %storing the count/bin%throw away stupendously large structures
            else
                continue
            end
            
            %% new bins
            
            %             if bin < (databin(end)/interval) && bin < databin(1) %throw away stupendously large structures
            %                 y_wound(1) = y_wound(1)+1; %storing the count/bin
            %             elseif bin < databin(2)
            %                 y_wound(2) = y_wound(2)+1; %storing the count/bin
            %             elseif bin < databin(3)
            %                 y_wound(3) = y_wound(3)+1; %storing the count/bin
            %             elseif bin < databin(4)
            %                 y_wound(4) = y_wound(4)+1;%storing the count/bin
            %             else
            %                 continue
            %             end
        end
        
        % store the raw matrix
        y_wound_mat = [y_wound_mat, y_wound];
         disp (['Fish ' num2str(i) ' Frame ' num2str(j)])
    end
    
    %% Saving the binned output as XLSX (NORMALIZED TO BEFORE WOUNDING)
%     writematrix(y_wound_mat,fullfile(outpath, 'raw distribution_crop', ['After_woundcrop_oldbin', '_', folderList(i).name,'.xlsx']))
    writematrix(y_wound_mat,fullfile(outpath, 'raw distribution_crop_notwounded', ['After_notwoundcrop_oldbin', '_', folderList(i).name,'.xlsx']))
    
    
    %% open before movies
    bb = folderListBefore(i);
    stk_before = bfGetReader (fullfile(infolder,folderList(bb).name));
    sizeT_before = stk_before.getSizeT();
    y_before_mat =[];
    
    
    for jj = 1:sizeT_before
        stk_frame = bf_getFrame(stk_before,1,1,jj);
        
        %% Crop out the wounded half
        stk_binary = logical(immultiply(stk_frame, woundcrop));
        
        %% measuring properites of the endosomes (BEFORE WOUNDING)
        stk_endo = regionprops('table',stk_binary, 'ConvexArea');
        %%% binning the data
        y_before = zeros(length(databin),1); %count of endosome of a particular convexarea bin
        ff = table2array(stk_endo);
        for k = 1:size(ff,1) %iterating through convexarea
            ffkth = ff(k,1); %kth endosome's convex area
            bin = ceil(ffkth/interval);
            %% old bin
            if bin < (databin(end)/interval) %throw away stupendously large structures
                y_before(bin) = y_before(bin)+1; %storing the count/bin
            else
                continue
            end
            
            %% new bin
            %             if bin < (databin(end)/interval) && bin < databin(1) %throw away stupendously large structures
            %                 y_before(1) = y_before(1)+1; %storing the count/bin
            %             elseif bin < databin(2)
            %                 y_before(2) = y_before(2)+1; %storing the count/bin
            %             elseif bin < databin(3)
            %                 y_before(3) = y_before(3)+1; %storing the count/bin
            %             elseif bin < databin(4)
            %                 y_before(4) = y_before(4)+1;%storing the count/bin
            %             else
            %                 continue
            %             end
            
        end
        % Storing the raw Before matrix
        y_before_mat = [y_before_mat, y_before];
        disp (['Fish ' num2str(i) ' Frame ' num2str(jj)])
    end
    %% Saving the binned output as XLSX (RAW DISTRIBUTION BEFORE WOUNDING)
%     writematrix(y_before_mat,fullfile(outpath, 'raw distribution_crop', ['Before_woundcrop_oldbin', '_', folderList(i).name,'.xlsx']))
     writematrix(y_before_mat,fullfile(outpath, 'raw distribution_crop_notwounded', ['Before_notwoundcrop_oldbin', '_', folderList(i).name,'.xlsx']))
end
