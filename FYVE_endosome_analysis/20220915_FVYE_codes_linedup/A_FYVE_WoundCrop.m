%FVYE_WoundCropping
clear all;
%% Define the folders to work on
fldname ='RAC1/';
infolder = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/20211019_Separation_40xW_Rac1_inhibitor/',  fldname]; % has the raw images
folderList = dir(infolder);
if (isfolder(folderList(3).name))
    folderList = folderList(3:end);
else
    folderList = folderList(4:end);
end

foldersToUse = 1:(numel(folderList)/2); %numel(folderList);
folderListBefore = ((numel(folderList)/2)+1):numel(folderList);

mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'segmented_crop/wound/']);
mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'segmented_crop/notwound/']);
outpath = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname];

%iterate througth folders
for i = foldersToUse
    stk= bfGetReader(fullfile(infolder,folderList(i).name));
    stk_frame = bf_getFrame(stk,1,1,1);
        
    %% Crop out the wounded half
    woundcrop = roipoly(imadjust(bf_getFrame(stk,1,1,1)));
    notwound = ~(woundcrop);
    stk_binary = logical(immultiply(stk_frame, woundcrop));
    
    % writing the segmented (wounded) files
    imwrite(uint16(woundcrop), fullfile(outpath, 'segmented_crop/wound/',['woundcrop',folderList(i).name]),'tiff','Compression','none','Writemode','overwrite');
    imwrite(uint16(notwound), fullfile(outpath, 'segmented_crop/notwound/',['notwoundcrop',folderList(i).name]),'tiff','Compression','none','Writemode','overwrite');
    
end