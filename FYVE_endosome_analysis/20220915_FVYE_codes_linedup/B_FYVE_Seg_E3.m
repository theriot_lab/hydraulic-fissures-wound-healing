%%%% GFP-FYVE 40x wounding endosome segmentation for E3_pooled
%% initialize
clear all; close all;

%%%Notes
% E3 Fish 8 is 1 min interval, Rest of the E3 is 0.5m interval
%%% add bfmatlab in your MATLAB path and

%% Define the folders to work on
fldname ='RAC1/';
infolder = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/20211019_Separation_40xW_Rac1_inhibitor/',  fldname]; % has the raw images
folderList = dir(infolder);
if (isfolder(folderList(3).name))
    folderList = folderList(3:end);
else
    folderList = folderList(4:end);
end
% folderList = folderList(3:end);
pixelsize = 0.3394^2; %xy resolution of the image
interval = 0.5/pixelsize; % 0.5�m interval
databin = 0:interval:130; %15 �m biggest endosome possible
databin_um = databin.*pixelsize;

%read the wound segmented folder
infolder1 = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'segmented_crop/wound/']; %has the wound segmented
woundfldlist = dir(infolder1);
woundfldlist = woundfldlist(3:end);
%%% read the file

%list of folders
foldersToUse = 1:(numel(folderList)/2); %numel(folderList);
folderListBefore = ((numel(folderList)/2)+1):numel(folderList);
ChannelstoUse = 2; %2 for E3 & RAC1, 1 for DMSO, ISO, BLEBB %FYVE

%create output directories
mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/',fldname]);
mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'segmented/']);
mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/',fldname,'raw distribution_crop/']);
% mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'normalized to before movies/']);

%folder for saving outputs
outpath = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname];


%iterate througth folders
for i = foldersToUse
    
    
    stk = bfGetReader(fullfile(infolder,folderList(i).name));
    
    %create outfilename
    fishid = strfind(folderList(i).name, '_p63');
    fishid = fishid(1);
    outfilename = folderList(i).name(1:fishid-1);
    
    %extract paramaters
    
    %     %E3 has variable time so correcting here
    %     if i == 8
    %         sizeT = stk.getSizeT();
    %         zz = 1:sizeT;
    %     else
    %         sizeT = 60; % first 3 movies have sizeT = 80 so taking only the first 30 minutes
    %         zz = 1:2:sizeT; %since the time interval is 0.5m take alternate frame
    %     end
    sizeT = stk.getSizeT();
    zz = 1:sizeT;
    
    %open before movies
    bb = folderListBefore(i);
    stk_before = bfGetReader(fullfile(infolder,folderList(bb).name));
    sizeT_before = stk_before.getSizeT();
    y_before_mat =[]; y_wound_mat = []; y_wound_normalized =[];
    
    %     % get the wound segmented file
    %     woundcrop = imread(fullfile(infolder1,woundfldlist(i).name));
    
    for jj = 1:sizeT_before
        stk_frame = bf_getFrame(stk_before,1,ChannelstoUse, jj);
        
        if strcmpi (fldname, 'BLEBB')
            %extra correction for blebb movies
            blebby = stk_frame>10000;
            stk_frame(blebby)=0;
        else
            %regular correction for blebb movies
            stk_flat = imflatfield(stk_frame, 30); % flat fielding to correct illumation and/or expression differences
            stk_filter = imtophat(stk_flat,strel('sphere',3)); % clear inter-spot haze
        end
        
        % threshold to get a binary image
        level = graythresh(stk_filter);
        if strcmpi (fldname, 'BLEBB')
            % change for blebb
            level = level/2;
        end
        
        stk_binary = imbinarize(stk_filter,level); % binarize low = strict
        
        %         % multiply the segmented image
        %         stk_binary = logical(immultiply(stk_binary, woundcrop));
        
        % writing the segmented (before wounding) files
        imwrite(uint16(stk_binary), fullfile(outpath, 'segmented',['before_segmented_',outfilename]),'tiff','Compression','none','Writemode','append');
        
        
        %% measuring properites of the endosomes (BEFORE WOUNDING)
        stk_endo = regionprops('table',stk_binary, 'ConvexArea');
        %%% binning the data
        y_before = zeros(length(databin),1); %count of endosome of a particular convexarea bin
        ff = table2array(stk_endo);
        for k = 1:size(ff,1) %iterating through convexarea
            ffkth = ff(k,1); %kth endosome's convex area
            bin = ceil(ffkth/interval);
            if bin < (databin(end)/interval) %throw away stupendously large structures
                y_before(bin) = y_before(bin)+1; %storing the count/bin
            else
                continue
            end
        end
        y_before_mat = [y_before_mat, y_before];
    end
    
    %     %%Histogram of the endosome size distribtion before wounding
    %     baseline_vector = mean(y_before_mat,2);
    
    %% Saving the binned output as XLSX (RAW DISTRIBUTION BEFORE WOUNDING)
    writematrix(y_before_mat,fullfile(outpath, 'raw distribution_crop', ['Before_Raw_ConvexArea_Binned_', outfilename,'.xlsx']))
    %
    %
    % % % % % % % % % % % % % % % % % % % %
    
    %% iterate through time for wounded movie
    stk_fish = table();
    
    for j = zz
        stk_frame = bf_getFrame(stk,1,ChannelstoUse, j);
        
        if strcmpi (fldname, 'BLEBB')
            % change for blebb
            blebby = stk_frame>10000;
            stk_frame(blebby)=0;
        else
            stk_flat = imflatfield(stk_frame, 30); % flat fielding to correct illumation and/or expression differences
            stk_filter = imtophat(stk_flat,strel('sphere',3)); % clear inter-spot haze
        end
        
        % threshold to get a binary image
        level = graythresh(stk_filter);
        if strcmpi (fldname, 'BLEBB')
            % change for blebb
            level = level/2;
        end
        
        stk_binary = imbinarize(stk_filter,level); % binarize low = strict
%         stk_binary = logical(immultiply(stk_binary, woundcrop)); % multipy by the mask
%         stk_binary = bwareaopen(stk_binary, 10);% throw the background particles that were picked up due to the low threshhold
        % writing the segmented (wounded) files
        imwrite(uint16(stk_binary), fullfile(outpath, 'segmented',['after_segmented_',outfilename]),'tiff','Compression','none','Writemode','append');
        
        %% measuring properites of the endosomes (AFTER WOUNDING)
        stk_endo = regionprops('table',stk_binary, 'ConvexArea'); % 'Area', 'MajorAxisLength'
        %%% binning the data
        y_wound = zeros(length(databin),1); %count of endosome of a particular convexarea bin
        ff = table2array(stk_endo);
        for k = 1:size(ff,1) %iterating through convexarea
            ffkth = ff(k,1); %kth endosome's convex area
            bin = ceil(ffkth/interval);
            if bin < (databin(end)/interval)
                y_wound(bin) = y_wound(bin)+1; %storing the count/bin%throw away stupendously large structures
            else
                continue
            end
        end
        
        %         %subtract the baseline and store
        %         y_wound_normalized  = [y_wound_normalized, (y_wound - baseline_vector)];
        % store the raw matrix
        y_wound_mat = [y_wound_mat, y_wound];
        
        disp (['Fish ' num2str(i) ' Frame ' num2str(j)])
    end
    
    %% Saving the binned output as XLSX (NORMALIZED TO BEFORE WOUNDING)
    writematrix(y_wound_mat,fullfile(outpath, 'raw distribution_crop', ['After_Raw_ConvexArea_Binned_', num2str(interval), '_', outfilename,'.xlsx']))
    
    %     %% Saving the binned output as XLSX (RAW DISTRIBUTION AFTER WOUNDING)
    %     writematrix(y_wound_normalized,fullfile(outpath, 'normalized to before movies', ['Normalized_to_Before_ConvexArea_Binned_', num2str(interval), '_', outfilename{i},'.xlsx']))
    
end