% FYVE_Grahing_Paper
% takes the binned endosome convex area files and divide the endosome area
% distributions into four categories and plots change in number over time.
% i have taken avg number across 5 minute intervals
% clear all;
% close all;
%load the files
fldname ='RAC1/';
fldname1 = 'RAC1'; %for graphing
infld = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'raw distribution_crop/'];
infldlist = dir(infld);
if ~(isfolder(infldlist(3).name))
    infldlist = infldlist(3:end);
else
    infldlist = infldlist(4:end);
end

% Endosome parameters
% Endosome parameters
pixelsize = 0.3394^2; %xy resolution of the image
interval = 0.5/pixelsize; % 0.5�m interval
databin = 0:interval:130; %15 �m biggest endosome possible
databin_um = databin.*pixelsize;
bininterval = 1:length(databin);
times = 1:30;

%%  Further dividing the area bins into 4 broad category
databin1 = 1:3; % 0-1 �m2
databin2 = 4:11; % 1-5 �m2
databin3 = 12:21; % 5-10 �m2
databin4 = 22:30; % >10 �m2

%%%Notes
% E3 Fish 8 is 1 min interval, Rest of the E3 is 0.5m interval so i
% sub-sampled the first 7 E3 fishes every other frame

%load the normalized excel files
cntr = 0; before_mat =[]; fish_avg = [];after_mat_0 =[]; after_mat_5 =[]; after_mat_10 =[];
after_mat_15 =[]; after_mat_20 =[]; after_mat_25 =[];

for i =1:length(infldlist)/2 %[1:3,8]
    cntr = cntr+1;
    
    %% POST WOUNDING
    fish_after = readmatrix(fullfile(infld,infldlist(i).name));
    %take avg for 5 min window for all size bins
    fish_after0 = mean(fish_after(bininterval,1:5),2); % Avg over  0-5 minutes for all the bins
    fish_after5 = mean(fish_after(bininterval,6:10),2); % Avg over  5-10 minutes for all the bins
    fish_after10 = mean(fish_after(bininterval,11:15),2); % Avg over  11-15 minutes for all the bins
    fish_after15 = mean(fish_after(bininterval,16:20),2); % Avg over 16-20 minutes for all the bins
    fish_after20 = mean(fish_after(bininterval,21:25),2); % Avg over  21-25 minutes for all the bins
    fish_after25 = mean(fish_after(bininterval,25:30),2); % Avg over  25-30 minutes for all the bins
    
    %storing all the data fishwise for all size bins
    after_mat_0 = [after_mat_0, fish_after0];
    after_mat_5 = [after_mat_5, fish_after5];
    after_mat_10 = [after_mat_10,fish_after10];
    after_mat_15 = [after_mat_15,fish_after15];
    after_mat_20 = [after_mat_20,fish_after20];
    after_mat_25 = [after_mat_25,fish_after25];   
    
    %% BEFORE WOUNDING
    fish_before = readmatrix(fullfile(infld,infldlist(i+6).name));
    fish_before = fish_before(bininterval,:); % before movie frames with first 30 bins
    fish_before_mean = mean(fish_before,2);
    before_mat  = [before_mat, fish_before_mean];
    %     after_over_before = fish_after./fish_before_mean;
    %     fish_foldchange(bininterval,1:length(times),cntr) =  after_over_before;
    
end


%% Graphing Endosome number histogram with different size and time bins
% Databin1 for before and all the after time bins (fish is rows)
firstsizebin = [sum(before_mat(databin1,:),1); sum(after_mat_0(databin1,:),1); sum(after_mat_5(databin1,:),1); sum(after_mat_10(databin1,:),1);....
sum(after_mat_15(databin1,:),1); sum(after_mat_20(databin1,:),1); sum(after_mat_25(databin1,:),1)];
firstsizebin_mean = mean(firstsizebin, 2);

%Databin2 for all the after time bins
secondsizebin = [sum(before_mat(databin2,:),1);sum(after_mat_0(databin2,:),1);sum(after_mat_5(databin2,:),1);sum(after_mat_10(databin2,:),1);....
sum(after_mat_15(databin2,:),1); sum(after_mat_20(databin2,:),1); sum(after_mat_25(databin2,:),1)];
secondsizebin_mean = mean(secondsizebin, 2);

%Databin3 for all the after time bins
thirdsizebin = [sum(before_mat(databin3,:),1); sum(after_mat_0(databin3,:),1);sum(after_mat_5(databin3,:),1);sum(after_mat_10(databin3,:),1);....
sum(after_mat_15(databin3,:),1); sum(after_mat_20(databin3,:),1); sum(after_mat_25(databin3,:),1)];
thirdsizebin_mean = mean(thirdsizebin, 2);

%Databin4 for all the after time bins
fourthsizebin = [sum(before_mat(databin4,:),1); sum(after_mat_0(databin4,:),1);sum(after_mat_5(databin4,:),1);sum(after_mat_10(databin4,:),1);....
sum(after_mat_15(databin4,:),1); sum(after_mat_20(databin4,:),1); sum(after_mat_25(databin4,:),1)];
fourthsizebin_mean = mean(fourthsizebin,2);

%% Box Scatter plots with normalization
% DATABIN 1
figure;
subplot(2,2,1);
cm = colormap(viridis(8));

%dummy variable to plot new graph for each databins no normalization
dummybin = firstsizebin(1:end,:); %now columns are the fishes
dummymean = firstsizebin_mean(1:end);
d = 10;
%plotting each time point
plot(1, dummybin(1,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(1,:), 'MarkerEdgeColor', 'k', 'LineWidth',2), hold on
plot(2, dummybin(2,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(2,:), 'MarkerEdgeColor', 'k', 'LineWidth',2),
plot(3, dummybin(3,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(3,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(4, dummybin(4,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(4,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(5, dummybin(5,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(5,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(6, dummybin(6,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(6,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(7, dummybin(7,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(7,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
%plotting mean trend line
plot(dummymean','h-','MarkerSize',d, 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'k', 'LineWidth', 3)
hold off
%graph beautification
% Set the axes properties
set(0,'DefaultAxesFontName', 'Cambria Math');
xlim(gca, [0.5 7.5])
set(gca,'FontSize',30,'LineWidth',2,'XTick',[0 1 2 3 4 5 6 7 8 9],...
    'XTickLabel',...
      {'','-5','5','10','15','20','25','30',''});
% Create ylabel
ylabel('Endosome Number','FontSize',30,'FontName','Cambria Math');

% Create xlabel
xlabel('Minutes Post Wounding');

% Create title
title({'<1 �m^2 Endosome Number ','Over Time (E3 Medium)'},'FontSize',30);


% DATABIN 2 1-5 �m2
subplot(2,2,2); cm = colormap(viridis(8));

%dummy variable to plot new graph for each databins no normalization
dummybin = secondsizebin(1:end,:); %now columns are the fishes
dummymean = secondsizebin_mean(1:end);
d = 20;
%plotting each time point
plot(1, dummybin(1,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(1,:), 'MarkerEdgeColor', 'k', 'LineWidth',2), hold on
plot(2, dummybin(2,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(2,:), 'MarkerEdgeColor', 'k', 'LineWidth',2),
plot(3, dummybin(3,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(3,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(4, dummybin(4,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(4,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(5, dummybin(5,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(5,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(6, dummybin(6,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(6,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(7, dummybin(7,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(7,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
%plotting mean trend line
plot(dummymean','h-','MarkerSize',d, 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'k', 'LineWidth', 3)
hold off
%graph beautification
% Set the axes properties
set(0,'DefaultAxesFontName', 'Cambria Math');
xlim(gca, [0.5 7.5])
set(gca,'FontSize',30,'LineWidth',2,'XTick',[0 1 2 3 4 5 6 7 8],...
    'XTickLabel',...
      {'','(-5)','5','10','15','20','25','30',''});
% Create ylabel
ylabel('Endosome Number','FontSize',30,'FontName','Cambria Math');

% Create xlabel
xlabel('Minutes Post Wounding');

% Create title
title({'1-5 �m^2 Endosome Number ','Over Time (E3 Medium)'},'FontSize',30);

% DATABIN 3
subplot(2,2,3); cm = colormap(viridis(8));

%dummy variable to plot new graph for each databins no normalization
dummybin = thirdsizebin(1:end,:); %now columns are the fishes
dummymean = thirdsizebin_mean(1:end);
d = 30;
%plotting each time point
plot(1, dummybin(1,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(1,:), 'MarkerEdgeColor', 'k', 'LineWidth',2), hold on
plot(2, dummybin(2,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(2,:), 'MarkerEdgeColor', 'k', 'LineWidth',2),
plot(3, dummybin(3,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(3,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(4, dummybin(4,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(4,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(5, dummybin(5,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(5,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(6, dummybin(6,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(6,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(7, dummybin(7,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(7,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
%plotting mean trend line
plot(dummymean','h-','MarkerSize',d, 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'k', 'LineWidth', 3)
hold off
%graph beautification
% Set the axes properties
set(0,'DefaultAxesFontName', 'Cambria Math');
xlim(gca, [0.5 7.5])
set(gca,'FontSize',30,'LineWidth',2,'XTick',[0 1 2 3 4 5 6 7 8],...
    'XTickLabel',...
     {'','(-5)','5','10','15','20','25','30',''});
% Create ylabel
ylabel('Endosome Number','FontSize',30,'FontName','Cambria Math');

% Create xlabel
xlabel('Minutes Post Wounding');

% Create title
title({'5-10^2 Endosome Number ','Over Time (E3 Medium)'},'FontSize',30);

% DATABIN 4
subplot(2,2,4); cm = colormap(viridis(8));

%dummy variable to plot new graph for each databins no normalization
dummybin = fourthsizebin(1:end,:); %now columns are the fishes
dummymean = fourthsizebin_mean(1:end);
d = 40;
%plotting each time point
plot(1, dummybin(1,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(1,:), 'MarkerEdgeColor', 'k', 'LineWidth',2), hold on
plot(2, dummybin(2,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(2,:), 'MarkerEdgeColor', 'k', 'LineWidth',2),
plot(3, dummybin(3,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(3,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(4, dummybin(4,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(4,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(5, dummybin(5,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(5,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(6, dummybin(6,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(6,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
plot(7, dummybin(7,:),'s', 'MarkerSize',d, 'MarkerFaceColor', cm(7,:), 'MarkerEdgeColor', 'k', 'LineWidth',2)
%plotting mean trend line
plot(dummymean','h-','MarkerSize',d, 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'k', 'LineWidth', 3)
hold off
%graph beautification
% Set the axes properties
set(0,'DefaultAxesFontName', 'Cambria Math');
xlim(gca, [0.5 7.5])
set(gca,'FontSize',30,'LineWidth',2,'XTick',[0 1 2 3 4 5 6 7 8],...
    'XTickLabel',...
    {'','(-5)','5','10','15','20','25','30',''});
% Create ylabel
ylabel('Endosome Number','FontSize',30,'FontName','Cambria Math');

% Create xlabel
xlabel('Minutes Post Wounding');

% Create title
title({'>10 �m^2 Endosome Number ','Over Time (E3 Medium)'},'FontSize',30);




