% FYVE_colorcodingendosomes

%inti
clear all; 
close all;

%Segmented endosomes files
conditions = {'E3', 'RAC1', 'ISO', 'BLEBB'};
%iterate through all the conditions
for c = 1 %:length(conditions)
    fldname =[conditions{c}, '/']; %for going to the correct folder
    fldname1 = conditions{c}; %for graphing
    %read the folder that has segmented endosome images
    infolder = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'segmented/'];
    folderList = dir(infolder);
    if (isfolder(folderList(3).name))
        folderList = folderList(3:end);
    else
        folderList = folderList(4:end);
    end
    % list of files post wounding
    foldersToUse = 1:(numel(folderList)/2); %numel(folderList);
    % list of files before wounding
    folderListBefore = ((numel(folderList)/2)+1):numel(folderList);
    
    %read the wounded area segmented folder
    infolder1 = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'segmented_crop/wound/']; %has the wound segmented
    woundfldlist = dir(infolder1);
    woundfldlist = woundfldlist(3:end);
    
    % Endosome parameters
    pixelsize = 0.3394^2; %xy resolution of the image
    
    % locations  to store the output
    mkdir (['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname,'segmented_color/']);
    outpath = ['/Users/mugdhasathe/Documents/DATA_2021/Team Fish/FYVE maxZs/endosome_quantification/', fldname, 'segmented_color/'];
    
    %% Count and Size Classify endosomes in the Pre-Wounding Movies
    fish_before_mat =[]; cntr = 0;
    for k = 9 %folderListBefore
        cntr = cntr+1;
        stk= bfGetReader(fullfile(infolder,folderList(k).name));
        sizeT = stk.getSizeT();
        
        % get the wound segmented file
        woundcrop = imread(fullfile(infolder1,woundfldlist(cntr).name));
        woundcroparea = regionprops(woundcrop,'Area');
        woundcroparea = woundcroparea.Area;
        
        %iterate through time
        before_mat =[];
        for j = 1:sizeT
            stk_binary = bf_getFrame(stk,1,1,j); %load the binary segmented image
            stk_binary_colour = zeros(size(stk_binary));
            % multiply the segmented image
            stk_binary = logical(immultiply(stk_binary, woundcrop));
            
            %% measuring properites of the endosomes (BEFORE WOUNDING)
            stk_endo = regionprops(stk_binary, 'ConvexArea','PixelIdxList', 'Eccentricity');
            
            %store the count of
            sizebin1 = 0; sizebin2 = 0; sizebin3 = 0;
            % iterate through all the segmented structures
            for ee = 1:size(stk_endo,1)
                areatemp = stk_endo(ee).ConvexArea; % area of an given endosome
                ecctemp = stk_endo(ee).Eccentricity;
                eccthresh = 0.8;
                
                %sizebin1 less than 1 micron square area
                if areatemp < 13
                    pixidtemp = stk_endo(ee).PixelIdxList;
                    stk_binary_colour(pixidtemp) =100;
                    sizebin1 = sizebin1 + 1;
                    %sizebin2 1-5 micron square area
                elseif areatemp > 13 && areatemp < 47
                    pixidtemp = stk_endo(ee).PixelIdxList;
                    stk_binary_colour(pixidtemp) = 500;
                    sizebin2 = sizebin2 + 1;
                    
                    %sizebin3 greater than 5 but less than 57 micron square
                    %area and the structure is not wiggly lines
                elseif areatemp > 47 && areatemp < 500 && ecctemp < eccthresh  %< 90 && ecctemp < eccthresh
                    pixidtemp = stk_endo(ee).PixelIdxList;
                    stk_binary_colour(pixidtemp) = 1000;
                    sizebin3 = sizebin3 + 1;
                end
                
            end
            % write the image file
%             imwrite(uint16(stk_binary_colour), fullfile(outpath,['before_color_',folderList(cntr).name]),'tiff','Compression','none','Writemode','append');
            
            %store the final counts in each size bin in the matrix for
            %each frame
            before_mat = [before_mat; sizebin1 sizebin2 sizebin3];
            disp (['Fish Before ' num2str(k) ' Frame ' num2str(j)])
        end
        %store data fish wise
        fish_before_mat = [fish_before_mat; mean(before_mat,1)];
    end
    
    clear sizeT stk stk_binary stk_binary_colour stk_endo sizebin1 sizebin2 sizebin3
    
    
    %% Count and Size Classify endosomes in the Post-Wounding Movies
    after_mat_0 =[]; after_mat_5 =[]; after_mat_10 =[];
    after_mat_15 =[]; after_mat_20 =[]; after_mat_25 =[];
    %iterate througth post wounding files
    for i = foldersToUse
        stk= bfGetReader(fullfile(infolder,folderList(i).name));
        
        %extract paramaters
        %E3 has variable time so correcting here
        if strcmp(fldname1, 'E3')
            if i == 8
                sizeT = stk.getSizeT(); %the 8th fish has 1m time interval
                zz = 1:sizeT;
            else
                sizeT = 60; % first 3 movies have sizeT = 80 so taking only the first 30 minutes
                zz = 1:2:sizeT; %since the time interval is 0.5m take alternate frame
            end
        else
            sizeT = 30;
            zz = 1:30;
        end
        
        %% iterate through time for wounded movie
        after_mat =[];
        for j = zz
            %load the binary segmented image
            stk_binary = bf_getFrame(stk,1,1,j);
            stk_binary_colour = zeros(size(stk_binary));
            % multiply the segmented image
            stk_binary = logical(immultiply(stk_binary, woundcrop));
            
            %% measuring properites of the endosomes (AFTER WOUNDING)
            stk_endo = regionprops(stk_binary, 'ConvexArea','PixelIdxList', 'Eccentricity');
            
            %store the count of
            sizebin1 = 0; sizebin2 = 0; sizebin3 = 0;
            for ee = 1:size(stk_endo,1)
                areatemp = stk_endo(ee).ConvexArea; % area of an given endosome
                ecctemp = stk_endo(ee).Eccentricity;
                eccthresh = 0.8;
                
                %sizebin1 less than 1 micron square area
                if areatemp < 13
                    pixidtemp = stk_endo(ee).PixelIdxList;
                    stk_binary_colour(pixidtemp) =100;
                    sizebin1 = sizebin1 + 1;
                    %sizebin2 1-5 micron square area
                elseif areatemp > 13 && areatemp < 47
                    pixidtemp = stk_endo(ee).PixelIdxList;
                    stk_binary_colour(pixidtemp) = 500;
                    sizebin2 = sizebin2 + 1;
                    
                    %sizebin3 greater than 5 but less than 57 micron square
                    %area and the structure is not wiggly lines
                elseif areatemp > 47 && areatemp < 500 && ecctemp < eccthresh  %< 90 && ecctemp < eccthresh
                    pixidtemp = stk_endo(ee).PixelIdxList;
                    stk_binary_colour(pixidtemp) = 1000;
                    sizebin3 = sizebin3 + 1;
                end
                                
                disp (['Fish ' num2str(i) ' Frame ' num2str(j)])
            end
            % write the image file
%             imwrite(uint16(stk_binary_colour), fullfile(outpath,['registered_after_color_',folderList(i).name]),'tiff','Compression','none','Writemode','append');
            
            %store the final counts in each size bin in the matrix
            after_mat = [after_mat; sizebin1 sizebin2 sizebin3];
            
        end
        %% now split the dataset for all the fishes into 5 minute chuncks
        fish_after0 = mean(after_mat(1:5,:),1); % Avg over  0-5 minutes for all the bins
        fish_after5 = mean(after_mat(6:10,:),1); % Avg over  5-10 minutes for all the bins
        fish_after10 = mean(after_mat(11:15,:),1); % Avg over  11-15 minutes for all the bins
        fish_after15 = mean(after_mat(16:20,:),1); % Avg over 16-20 minutes for all the bins
        fish_after20 = mean(after_mat(21:25,:),1); % Avg over  21-25 minutes for all the bins
        fish_after25 = mean(after_mat(26:30,:),1); % Avg over  25-30 minutes for all the bins
        
        
        %storing all the data fishwise for all size bins
        % here rows are different fishes
        after_mat_0 = [after_mat_0; fish_after0];
        after_mat_5 = [after_mat_5; fish_after5];
        after_mat_10 = [after_mat_10; fish_after10];
        after_mat_15 = [after_mat_15; fish_after15];
        after_mat_20 = [after_mat_20; fish_after20];
        after_mat_25 = [after_mat_25; fish_after25];
        
    end
    
    
    %% plotting the results
    
    %% size bin 3 all the fishes
    figure('Name',fldname1,'NumberTitle','off');
    set(gcf, 'WindowState', 'maximized')
    subplot(1,3,1)
    % cm = colormap(viridis(6));
    bincol = 3;
    dummybin = [fish_before_mat(:,bincol),after_mat_0(:,bincol), after_mat_5(:,bincol),after_mat_10(:,bincol),after_mat_15(:,bincol),after_mat_20(:,bincol), after_mat_25(:,bincol)];
    dummymean = mean(dummybin,1);
    d = 30;
    %plotting each time point
    plot(1, dummybin(:,1),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'b', 'LineWidth',2), hold on
    plot(2, dummybin(:,2),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(3, dummybin(:,3),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(4, dummybin(:,4),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(5, dummybin(:,5),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(6, dummybin(:,6),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(7, dummybin(:,7),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    
    
    %plotting mean trend line
    plot(dummymean,'g-', 'LineWidth', 3)
    hold off
    %graph beautification
    % Set the axes properties
    set(0,'DefaultAxesFontName', 'Arial');
    xlim(gca, [0.5 7.5])
    ylim(gca, [0 60])
    set(gca,'FontSize',20,'LineWidth',2,'XTick',[0 1 2 3 4 5 6 7 8],...
        'XTickLabel',...
        {'','(-5)-0','0-5','5-10','10-15','15-20','20-25','25-30',''});
    % Create ylabel
    ylabel('Endosome Number','FontSize',30,'FontName','Arial');
    
    % Create xlabel
    xlabel('Minutes Post Wounding');
    
    % Create title
    title('>5 �m^2 Endosome Number ','FontSize',30);
    clear bincol dummybin dummymean
    
    %% size bin 2 all the fishes
    subplot(1,3,2)
    % cm = colormap(viridis(6));
    bincol = 2;
    dummybin = [fish_before_mat(:,bincol), after_mat_0(:,bincol), after_mat_5(:,bincol),after_mat_10(:,bincol),after_mat_15(:,bincol),after_mat_20(:,bincol), after_mat_25(:,bincol)];
    dummymean = mean(dummybin,1);
    d = 20;
    %plotting each time point
    plot(1, dummybin(:,1),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'b', 'LineWidth',2), hold on
    plot(2, dummybin(:,2),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(3, dummybin(:,3),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(4, dummybin(:,4),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(5, dummybin(:,5),'o', 'MarkerSize',d,   'MarkerEdgeColor','k', 'LineWidth',2),
    plot(6, dummybin(:,6),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(7, dummybin(:,7),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    
    %plotting mean trend line
    plot(dummymean,'g-', 'LineWidth', 3)
    hold off
    %graph beautification
    % Set the axes properties
    set(0,'DefaultAxesFontName', 'Arial');
    xlim(gca, [0.5 7.5])
    ylim(gca, [0 1200])
    set(gca,'FontSize',20,'LineWidth',2,'XTick',[0 1 2 3 4 5 6 7 8],...
        'XTickLabel',...
        {'','(-5)-0','0-5','5-10','10-15','15-20','20-25','25-30', ''});
    % Create ylabel
    ylabel('Endosome Number','FontSize',30,'FontName','Arial');
    
    % Create xlabel
    xlabel('Minutes Post Wounding');
    
    % Create title
    title('1-5 �m^2 Endosome Number ','FontSize',30);
    
    clear bincol dummybin dummymean
    
    %% size bin 1 all the fishes
    subplot(1,3,3)
    bincol = 1;
    dummybin = [fish_before_mat(:,bincol), after_mat_0(:,bincol), after_mat_5(:,bincol),after_mat_10(:,bincol),after_mat_15(:,bincol),after_mat_20(:,bincol), after_mat_25(:,bincol)];
    dummymean = mean(dummybin,1);
    d = 10;
    %plotting each time point
    plot(1, dummybin(:,1),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'b', 'LineWidth',2), hold on
    plot(2, dummybin(:,2),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(3, dummybin(:,3),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(4, dummybin(:,4),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(5, dummybin(:,5),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(6, dummybin(:,6),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    plot(7, dummybin(:,7),'o', 'MarkerSize',d,  'MarkerEdgeColor', 'k', 'LineWidth',2),
    
    %plotting mean trend line
    plot(dummymean,'g-', 'LineWidth', 3)
    hold off
    %graph beautification
    % Set the axes properties
    set(0,'DefaultAxesFontName', 'Arial');
    xlim(gca, [0.5 7.5])
    ylim(gca, [0 6000])
    set(gca,'FontSize',20,'LineWidth',2,'XTick',[0 1 2 3 4 5 6 7 8],...
        'XTickLabel',...
       {'','(-5)-0','0-5','5-10','10-15','15-20','20-25','25-30', ''});
    % Create ylabel
    ylabel('Endosome Number','FontSize',30,'FontName','Arial');
    
    % Create xlabel
    xlabel('Minutes Post Wounding');
    
    % Create title
    title('<1 �m^2 Endosome Number ','FontSize',30);
    clear bincol dummybin dummymean
    
end





