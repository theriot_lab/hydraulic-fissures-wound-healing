//Use this script to choose a relatively stationary region in an image to use to register
//and remove drift in X and Y (NOT in Z). 
//Usage:
//1. BEFORE running this script, make sure to review the initialization variables in the first 
//   section of this code
//2. Also make sure to set up a *_MoviesToProcess.csv file that contains the folders for each
//   dataset you want to process. 
//      For example, suppose your base directory is /Users/akennard/Analysis/ and you have the following
//      3 dataset to process, from two different experiments:
//        /Users/akennard/Analysis/expt1/dataset1/dataset1_maxZ.tif
//        /Users/akennard/Analysis/expt1/dataset2/dataset2_maxZ.tif
//        /Users/akennard/Analysis/expt2/dataset1/dataset1_maxZ.tif
//     Then you could make a file such as yyyymmdd_MoviesToProcess.csv with the following three lines:
//        expt1/dataset1/
//        expt1/dataset2/
//        expt2/dataset1/
//     NOTE: the trailing slash is CRITICAL! 
//3. Once the csv is ready and the initial variables are specified, run the script. It will start
//   opening images
//4. The first image will open and a box will appear in the image. Move the box as needed to find
//   a stationary region of the image to use to calculate the registration transformations. When
//   you are satisfied, hit the spacebar.
//5. A window will popup that says "Find a frame to start tracking". Use this moment to identify
//   the first frame that you want to start registering. This could be 0mpw (ignoring pre-wound
//   frames), or it could be because the first few timepoints after wounding were messed up
//   and you don't want to include them.
//6. Hit 'ok', then enter the frame you want to start tracking. (1-based indexing, so it should match up
//   with the frame index shown at the top of the image window in Fiji).
//7. Once you hit 'enter', the current image will close and the next image will be loaded, go to step 4.


//------ Review these variables first -------
// all input/output files will be in subdirectories of this directory
base_dir = "/Users/akennard/Analysis/"; 
//Define the name of the target image (the one you want to register) from the folder name plus this suffix. 
//E.g. if the target image is located at base_dir/testimage/testimage_surfcut.ome.tif, then this variable
//should be '_surfcut.ome.tif'
target_image_suffix = "_surfcut.ome.tif";

line_separator = "\n";
//This csv file speficies all the folders of datasets you want to look at. This file consists of a series
//of paths, one per line, that have the experiment_folder/dataset_folder, where the experiment_folder
//are all located in the base_dir specified above, and the dataset_folder is different on each line
//(there may be multiple datasets in one experiment folder). The dataset_folder will be extracted from 
//each string and used to identify the exact image in that folder to process.
data_path_list = split(File.openAsString(base_dir + 
                                         "DataRecords/20201022_MoviesToProcess.csv"), line_separator);
//The width of the subimage to crop. Generally good to have the subregion be at least 300px wide, though it can be 
//manually resized to be taller than 300 px.                                         
crop_image_size = 300;
//This is the filename for the output csv file that will contain further details for registration that
//will be passed to the registration algorithm
registration_detail_file_name = base_dir + "DataRecords/20201022_DetailsForRegistration_2.csv";

//-------- Script continues from here --------------------
if (File.exists(registration_detail_file_name)) {
	File.delete(registration_detail_file_name);
}
//Set up the output csv file and print the first line
f = File.open(registration_detail_file_name);
print(f,"subImageToRegister," + 
        "fullImageToRegister," + 
        "firstFrame," + 
        "lastFrame," + 
        "xPosition," + 
        "yPosition");

for (i=0; i<lengthOf(data_path_list); i++) {
	record_number = i+1;
	print("Processing " + record_number + " of " + 
	       lengthOf(data_path_list) + " records...");
	//Compute the image name from the folder name
	tmp = File.getName(data_path_list[i]);
	target_image_name = tmp + target_image_suffix;
	file_name = base_dir + data_path_list[i] + target_image_name;
	//Open the target image
	open(file_name);
	image_to_crop_title = getTitle();
	//Crop the image (wait for the user's input)
	selection = cropImageUserInput(image_to_crop_title,crop_image_size);
	
	//Switch the slices and frames (only tested to work when one of them is 1).
	ensureProperDimensions();
	
	waitForUser("Choose Start frame", "Find frame to start tracking");
	start_frame = getNumber("Enter the frame you want to start tracking (1-based indexing)",1);
	start_frame_zero_idx = start_frame - 1;
	cropped_save_name = makeCroppedImageSaveName(file_name, 
	                                             selection, 
	                                             crop_image_size);
	detail_string = printDetailsForRegistration(base_dir, 
	                                            data_path_list[i],
	                                            target_image_suffix,
	                                            cropped_save_name,
	                                            selection,
	                                            start_frame_zero_idx);
	print(f,detail_string);
	saveAs("Tiff", cropped_save_name);
	run("Close All");
	wait(50);
	
}

File.close(f);

function makeCroppedImageSaveName(file_name,selection,crop_image_size) {
	// Generate the save name for a cropped image based on the name of the original file.
	// Take off the .tif, and add _x<X>_y<Y>_<CROP> where X,Y,CROP are the x and y coordinates
	// and size of the cropped image.
	L = lengthOf(file_name);
	base_name = substring(file_name,0,L-4);
	save_name = base_name + 
	            "_x" + IJ.pad(selection[0],3) + 
	            "_y" + IJ.pad(selection[1],3) + 
	            "_" + IJ.pad(crop_image_size,3) + 
	            ".tif";
	return save_name;
}

function printDetailsForRegistration(base_dir,
                                     data_path,
                                     max_z_suffix,
                                     cropped_save_name,
                                     selection,
                                     start_frame) {
    // Create a comma-separated line of details about each file.
    // Will be printed to a csv to use for registration
	L = lengthOf(data_path);
	idx_last_portion = lastIndexOf(substring(data_path,0,L-1), "/");
	full_image_file_name = base_dir + 
	                       data_path + 
	                       substring(data_path,idx_last_portion+1,L-1) + 
	                       max_z_suffix;
	line_array = newArray(cropped_save_name, 
	                      full_image_file_name, 
	                      start_frame, 
	                      -1, 
	                      selection[0], 
	                      selection[1]);
	line = arrayToCommaSeparatedString(line_array);
	return line;
}

function arrayToCommaSeparatedString(array_to_print) {
	// concatenate an array into a comma-separated string
	L = lengthOf(array_to_print);
	array_as_string = array_to_print[0];
	for (i=1; i<L; i++) {
		array_as_string = array_as_string + "," + array_to_print[i];
	}
	return array_as_string;
}

function cropImageUserInput(image_to_crop_title,crop_image_size) {
	// Generate a square window in the image and allow the user to 
	// move it to select a region for cropping.
	// Save the x and y coordinates of the cropped region.
	selectImage(image_to_crop_title);
	makeRectangle(100, 100, crop_image_size, crop_image_size);
	
	while (!isKeyDown("space")) {
		wait(10);
	}

	getSelectionBounds(selection_x, selection_y, w, h);
	selection_array = newArray(selection_x, selection_y);
	// Crop by duplicating
	run("Duplicate...", "duplicate");
	return selection_array;
}

function ensureProperDimensions() {
	//Make sure active image has z as singleton dimension, not t.
	getDimensions(w, h, num_channels, num_slices, num_frames);
	if (num_slices > 1 && num_frames == 1) {
		run("Properties...", "slices=" + num_frames + 
		                     " frames=" + num_slices);
	}
}
