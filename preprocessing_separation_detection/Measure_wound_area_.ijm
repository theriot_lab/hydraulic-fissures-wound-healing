//Use this script to manually measure the wound area at time = 0 and time = 25 min, outputs to .csv

//Usage:
//1. BEFORE running this script, make sure to review the initialization variables in the first 
//   section of this code
//2. Also make sure to set up a Annotations*.csv file that contains the previously annotated info for each
//   dataset you want to process. The critical columns are:
//			pathToImage
//			TrueFirstFrame
//			time_interval_s
//			label (condition)
//3. Once the csv is ready and the initial variables are specified, run the script. 

//------ Review these variables first -------
// all input/output files will be in subdirectories of this directory
base_dir = "/Users/enorby/Thesis/Analysis/InputOutput/"; 

line_separator = "\n";
//This csv file speficies all the folders of datasets you want to look at. This file consists of a series
//of paths, one per line, that have the experiment_folder/dataset_folder, where the experiment_folder
//are all located in the base_dir specified above, and the dataset_folder is different on each line
//(there may be multiple datasets in one experiment folder). The dataset_folder will be extracted from 
//each string and used to identify the exact image in that folder to process.
data_path_list = split(File.openAsString(base_dir + 
                                         "Annotations_for_AK_2020-02-08.csv"), line_separator);

//savedir = "/Users/enorby/Thesis/Analysis/InputOutput/Surfcut_wound_rois/";

//This is the filename for the output csv file that will contain measured areas
date_str = getTodaysDateStr();
annotation_detail_file_name = base_dir + "Wound_Areas_forAK02-08_measured" + date_str + ".csv";

//-------- Script continues from here --------------------
if (File.exists(annotation_detail_file_name)) {
	File.delete(annotation_detail_file_name);
}
//Set up the output csv file and print the first line
f = File.open(annotation_detail_file_name);
print(f,"pathToImage," + 
		"0min_frame," +
        "0min_upper_wound_area," +
        "0min_lower_wound_area," +
        "25min_frame," +
        "25min_upper_wound_area," +
        "25min_lower_wound_area," + 
        "condition");

//for (i=1; i<lengthOf(data_path_list); i++) {
for (i=1; i<2; i++) {
	print("Processing " + i + " of " + 
	       lengthOf(data_path_list) + " records...");

	time_interval_min = getTimeFromListEntry(data_path_list[i])/60;
	timepoint1 = getStartFromListEntry(data_path_list[i])+1;
	timepoint2 = 25/time_interval_min+timepoint1;

	//get image path (weirdly didn't work as a separate function)
	cell_separator = ",";
	cell_array = split(data_path_list[i],cell_separator);
	image_path = cell_array[1];
	condition = cell_array[14];
	file_name = image_path;
	       	
	//Open the target image
	open(file_name);
	original_image_title = getTitle();
	run("Enhance Contrast", "saturated=0.35");

	//go to the first timepoint after wounding
	setSlice(timepoint1);
	
	//get 2 wound regions (time 1)
	setTool("polygon");
	waitForUser("Wound Area Instructions", "Draw a polygon around one wound area. Press space when done. \nRepeat for second wound area");
	while (!isKeyDown("space")) {
		wait(100);
	}
	area1_upper = getValue("Area");
	//this provides a way to 'skip' the wound (i.e. if it intersects the border)
	if (area1_upper > 100000) {
		area1_upper = 0;
	}
	print(area1_upper);
	
	setTool("polygon");
	setKeyDown("none");
	while (!isKeyDown("space")) {
		wait(100);
	}
	area1_lower = getValue("Area");
	//this provides a way to 'skip' the wound (i.e. if it intersects the border)
	if (area1_lower > 100000) {
		area1_lower = 0;
	}
	print(area1_lower);

	//Go to the timepoint 25 min after wounding
	setSlice(timepoint2);

	//get 2 wound regions (time 2)
	setTool("polygon");
	waitForUser("Wound Area Instructions", "Draw a polygon around one wound area. Press space when done. \nRepeat for second wound area");
	while (!isKeyDown("space")) {
		wait(100);
	}
	area2_upper = getValue("Area");
	//this provides a way to 'skip' the wound (i.e. if it intersects the border)
	if (area2_upper > 100000) {
		area2_upper = 0;
	}
	print(area2_upper);
	
	setTool("polygon");
	setKeyDown("none");
	while (!isKeyDown("space")) {
		wait(100);
	}
	area2_lower = getValue("Area");
	//this provides a way to 'skip' the wound (i.e. if it intersects the border)
	if (area2_lower > 100000) {
		area2_lower = 0;
	}
	print(area2_lower);

    // Create a comma-separated line of details about each file to print to csv
	line_array = newArray(file_name,
	                      timepoint1,
	                      area1_upper,
	                      area1_lower,
	                      timepoint2,
	                      area2_upper,
	                      area2_lower,
	                      condition);
	           
	detail_string = arrayToCommaSeparatedString(line_array);
	
	print(f,detail_string);
	
	run("Close All");
	wait(50);
	
}

File.close(f);

function getStartFromListEntry(line) {
	cell_separator = ",";
	cell_array = split(line,cell_separator);
	starting_frame = cell_array[3];
	return starting_frame
}

function getTimeFromListEntry(line) {
	cell_separator = ",";
	cell_array = split(line,cell_separator);
	time_interval_s = cell_array[19];
	return time_interval_s
}

function getTodaysDateStr(){
	//Return a string yyyymmdd for today's date
	getDateAndTime(year, month, dow, day, h,m,s,ms);
	//month and day are zero-indexed
	date_str =  d2s(year,0) + IJ.pad(month + 1, 2) + IJ.pad(day, 2);
	return date_str
}

function arrayToCommaSeparatedString(array_to_print) {
	// concatenate an array into a comma-separated string
	L = lengthOf(array_to_print);
	array_as_string = array_to_print[0];
	for (i=1; i<L; i++) {
		array_as_string = array_as_string + "," + array_to_print[i];
	}
	return array_as_string;
}
