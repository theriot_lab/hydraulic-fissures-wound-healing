#@ File (style="directory") input_folder
//Use this script to convert a folder containing a sequence of images (XY or XYC) split by timepoints from
//many different experiments into individual .ome.tif files for each experiment. Right now this does NOT 
//support z-stacks! Only series of XY or XYC, with the assumption that each separate file is a different 
//timepoint.
//Usage:
// 1. BEFORE running the script, change the matchingList variable below. For each distinct dataset, you should
//    include a substring that uniquely identifies all files coming from that dataset within the folder. 
//    If a simple string does not uniquely identify the file, you may also want to use a regular expression, 
//    which you can do in Fiji by surrounding the string with parentheses, like so "(regexp)". For more
//    info on regular expressions, see p. 11 of this presentation or google Regular Expressions:
//    http://dev.mri.cnrs.fr/attachments/download/1276/slides_imagej_macro_programming.pdf\
//    Another way to test your regular expression is to open the "Open Image Sequence" option in 
//    ImageJ and put the regular expression in the "File name contains" box and see how many images it says match.
//
// 2. When you run this script, it will ask you to navigate to the directory containing all these images.
//    After that it should run automatically. Make sure to check the output, esp. if running with multichannel
//    images. I am not sure I can guarantee that it will work for those!

//matchingList is a list of matching expressions to single out specific series in a folder with lots of them.
// Items in parens are regular expressions
matchingList = newArray("Wounding_1",
						"Wounding_2",
						"Wounding_3",
						"Wounding_4",
						"Wounding_5",
						"Wounding_6",
						"Wounding_7",
						"Wounding_8",
						"Wounding_9");
setBatchMode("hide");       
for (i=0; i<lengthOf(matchingList); i++) {   //Loop thru the matchingList

//open the images as an Image Sequence
run("Image Sequence...",
    "open=" + input_folder +
    " file="+ matchingList[i] + " sort");
dir = File.directory;
name = File.name;
//Convert the image to the proper dimensions (XY(C)T), no Z
getDimensions(sizeX, sizeY, sizeC, sizeZ, sizeT);
getVoxelSize(width, height, depth, unit);
if (sizeZ!=1) {
	sizeT = sizeZ;
	sizeZ = 1;
}
run("Properties...","channels=" + sizeC + " slices=" + sizeZ + 
    " frames=" + sizeT + " pixel_width=" + width + 
    " pixel_height=" + height + " voxel_depth=1");
//Determine the save name from the path and save it in the proper location
nameSplit = split(name,"(_t[0-9][0-9][0-9])"); //assume timepoint is in the filename, i.e. _t031
//Save in 
savepath = File.getDirectory(dir) + nameSplit[0] + "/" + nameSplit[0] + "_surfcut.ome.tif";
run("Bio-Formats Exporter", "save=" + savepath + " use compression=Uncompressed");
close("*");
}
setBatchMode("exit and display");
print("Done!");