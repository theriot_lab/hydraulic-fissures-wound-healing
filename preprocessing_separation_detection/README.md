# Instructions for preprocessing
This folder contains several scripts in ImageJ macro and Python for preparing the data for ridge detection. 
These scripts are run AFTER deconvolution, and selective plane projection and BEFORE the ridge detection and kymograph analysis. 
The instructions below walk you through each step.

## Convert output from selective projection to OME TIFFs
* _Input_
    - A folder containing 2D projections split by timepoint (image sequence of XY files indexed by T) from 
several different experiments (i.e. different datasets)
* _Output_
    - folders containing 3D (XYT) OME TIFF stacks, one folder and file per experiment

The scripts on the Neuron/GPU workstation to do deconvolution and selective plane projection work on 
individual timepoints, converting XYZ files to XY files (projection). So when you're done you
have a ton of 2D images split by timepoint, generally with all the files from different experiments
mixed together in one folder. This script combines these image sequences into single tiff stacks (XYZ)
in OME TIFF format.

### Instructions
1. Open the `convertImSequenceToOMETIFF_.ijm` macro in ImageJ.
2. BEFORE running the script, change the `matchingList` variable. For each distinct dataset in the folder you want to process, you should
include a substring that uniquely identifies all files coming from that dataset within the folder. 
    - If a simple string does not uniquely identify the dataset, you may also want to use a regular expression, 
which you can do in Fiji by surrounding the string with parentheses, like so `"(regexp)"`. For more
info on regular expressions, see p. 11 of this presentation or Google Regular Expressions:
http://dev.mri.cnrs.fr/attachments/download/1276/slides_imagej_macro_programming.pdf
Another way to test your regular expression is to open the "Open Image Sequence" option in 
ImageJ and put the regular expression in the "File name contains" box and see how many images it says match.
3. When you run this macro, it will ask you to navigate to the directory containing all these images.
After that it should run automatically. Make sure to check the output, esp. if running with multichannel
images. I am not sure I can guarantee that it will work for those!

## Prepare annotations for registration, ridge detection, and kymograph analysis
* _Input_
    - XYT OME TIFF stacks
    - a csv file containing filenames for the TIFF stacks you want to process
* _Output_
    - A second CSV with information needed by the registration and kymograph analysis steps
    - (the image data itself is not changed)

Prior to the next steps, there are few pieces of information that need to be collected about each dataset.
For example, if you want to register the movies to remove XY drift, you need to specify a region of the image
that is more or less stationary to register first. And for the kymographs, the location of the wound and the 
anterior-posterior (AP) axis needs to be defined. These are best done manually, but I have an ImageJ macro to help
make it as painless as possible.

### Instructions
1. Prepare a csv file containing the folder names for each dataset you want to process. 
    - I like to date these in the name and give it a similar suffix for different runs (on different data), e.g. `20210210_MoviesToProcess.csv`. 
    - In order to make this csv easy to make and understand, you only include part of the filepath for each dataset
        - For example, suppose all your data is in `/Users/akennard/Data/`, and you have the following 3 datasets to 
        process, from two different experiments:
            - `/Users/akennard/Data/expt1/dataset1/dataset1_maxZ.tif`
            - `/Users/akennard/Data/expt1/dataset2/dataset2_maxZ.tif`
            - `/Users/akennard/Data/expt2/dataset1/dataset1_maxZ.tif`
        - Then you should make a file `20210210_MoviesToProcess.csv` with the following three lines:
            - `expt1/dataset1/`
            - `expt1/dataset2/`
            - `expt2/dataset1/`
        - **NOTE: The trailing slash is CRITICAL!**
2. Once the csv file is ready, open `drawAPAxis_selectRegionForRegistration_.ijm` in ImageJ.
3. specify initial variables, just after the long comment with instructions. 
These include the location of that csv file, and other info to help ImageJ find your files. There are comments in the code
to guide you.
4. Run the script. It will start opening images.
5. The first image will open and the line drawing tool will be selected. A window will pop up with
instructions. Draw the line starting at the wound and extending along the anterior-posterior
axis (in the anterior direction). Press space when done and the coordinates of the line will be saved.
6. Nex a box will appear in the image. Move the box as needed to find
a stationary region of the image to use to calculate the registration transformations. When
you are satisfied, hit the spacebar.
7. A window will popup that says "Find a frame to start/end tracking". Use this moment to identify
    - The frame corresponding to "0mpw" (that's the first frame after wounding)
    - The first frame you want to start registering or analyzing in a kymograph. This could be
    the same as 0mpw, or it could be later if the first few timepoints were messed up.
    - The last frame you want to include for further analysis. If you are fine with the rest of the 
    movie being tracked, leave the default of "-1".
 Note that these are all 1-based indexing, i.e. they should match up with the number shown in 
 the top left corner of the Image window in ImageJ.
8. Hit 'ok', then enter the frames you just identified.
9. Once you hit 'enter', the current image will close and the next image will be loaded, go to step 4.
10. When you are done with this process, open the new csv that was just made and add a column called `label`
which includes some label for each dataset. There is no particular guidance, it just has to be
descriptive and easy for you to keep track. (it's also used in the registration code)

## Register images to correct XY drift
* _Input_
    - XYT OME TIFF stacks
    - the second CSV that was prepared by the `drawAPAxis_selectRegionForRegistration_.ijm` macro.
* _Output_
    - A folder of XY images (split by timepoint) that have been registered.

This step is not required, but you will probably want to register your movies to remove sample drift, so
it's a good idea to do this. This is done with Python code.

There are two python files, `registration_onechannel.py` and `registration_twochannel.py`. The only difference
is that one expects there to be only one channel in your images (so they are XY), while the other expects 2 channels
(each images is XYC) but it will only register the 2nd channel. Let me know if this is too restrictive, but it should be
possible to work around these constraints. You can also use the registration from one channel to register a second channel; 
let me know if you want to do this.

Before you run the script, you need to make sure you have the right dependencies installed. I also recommend using a
[virtual environment](https://realpython.com/python-virtual-environments-a-primer/) when you start so you don't 
mess up any other python code you might want to run later.
### Python setup for Anaconda users
1. Open a terminal and type `conda create -n registration` to create a new virtual environment called `registration`
2. To activate (or enter) this virtual environment, type `conda activate registration` Now the word `registration` should
appear on your terminal prompt. Any packages you install now will only be in this virtual environment, and will not affect
any of your other python. 
3. Change directory to this folder in the repository using the `cd` command
4. Type `conda install -n registration --file 20190310_python_requirements.txt`. This should install all the packages you need into
the appropriate virtual environment.
5. When you are done doing anything with this python code, just type `conda deactivate` to leave the virtual environment. 
6. Every time you want to use this code, you will first have to enter the virtual environment by typing `conda activate registration`
and then moving on to the python script.

### Python setup using pip (not anaconda based)
1. If you don't already have a folder where you are storing your virtual environments, make one (I like to keep mine in a 
folder I made called `~/.venvs/`, and I'll assume you have the same folder in what follows).
2. Open a terminal and type `python3 -m venv ~/.venvs/registration` to make a virtual environment called `registration`
3. Activate the virtual environment with `source ~/.venvs/registration/bin/activate`
4. Update pip (python package installer) with `pip3 install --upgrade pip3`
5. Navigate using `cd` to get to the preprocessing folder in this repository
6. Type `pip3 install -r 20190310_python_requirements.txt` to install all the packages specified in that file. Your virtual environment
should be ready to go.
7. When you are done using the registration code, deactivate the environment by typing `deactivate`.
8. If you want to use the registration code again, each time you will need to activate the virtual environment with `source ~/.venvs/registration/bin/activate`

### Registration Instructions
1. Set up your environment as specified (depending on if you do or do not use Anaconda)
2. Make sure you have the csv file from the previous step, and that you added the `label` column as described in step 10 of the instructions
for the previous ImageJ script.
3. Determine if you want to use the `onechannel` or `twochannel` script. The instructions are the same, but for concreteness I'm going to assume
you're using the `onechannel` script. 
4. Type `python3 registration_onechannel.py <csv_file>` where `<csv_file>` is replaced with the path to the csv file. From this point
the code should just run. 
5. If it worked you should have new folders with the word "registered" in the name, containing images,
and you can open them as an Image Sequence in ImageJ to see if it looks registered.
