//Use this script to generate several annotations related to an image:
//  1) Draw a line that specifies the wound location and the orientation of the anterior-posterior axis
//  2) Choose a relatively stationary region in an image to use to register
//     and remove drift in X and Y (NOT in Z)
//The sub-region is cropped, and information about the location of that region, as well
//as the A-P axis line are saved to a csv for use by other programs.
//Usage:
//1. BEFORE running this script, make sure to review the initialization variables in the first 
//   section of this code
//2. Also make sure to set up a *_MoviesToProcess.csv file that contains the folders for each
//   dataset you want to process. 
//      For example, suppose your base directory is /Users/akennard/Analysis/ and you have the following
//      3 dataset to process, from two different experiments:
//        /Users/akennard/Analysis/expt1/dataset1/dataset1_maxZ.tif
//        /Users/akennard/Analysis/expt1/dataset2/dataset2_maxZ.tif
//        /Users/akennard/Analysis/expt2/dataset1/dataset1_maxZ.tif
//     Then you could make a file such as yyyymmdd_MoviesToProcess.csv with the following three lines:
//        expt1/dataset1/
//        expt1/dataset2/
//        expt2/dataset1/
//     NOTE: the trailing slash is CRITICAL! 
//3. Once the csv is ready and the initial variables are specified, run the script. It will start
//   opening images
//4. The first image will open and the line drawing tool will be selected. A window will pop up with
//   annotation instructions. Draw the line starting at the posterior edge of the tail (as much as is
//   visible) and extending along the anterior-posterior axis (in the posterior direction). 
//   Press space when done and the coordinates of the line will be saved.
//5. Click on the anterior edge of the wound area (along the A-P line) and hit the space bar again. The 
//   coordinates of this point will be saved.
//6. Next, instructions about registration will display. After clicking "ok", a box will appear in the image. 
//   Move the box as needed to find a stationary region of the image to use to calculate the registration 
//   transformations. When you are satisfied, hit the spacebar.
//7. A window will popup that says "Find a frame to start/end tracking". Use this moment to identify
//     - The frame corresponding to "0mpw" (that's the first frame after wounding)
//     - The first frame you want to start registering or analyzing in a kymograph. This could be
//       the same as 0mpw, or it could be later if the first few timepoints were messed up.
//     - The last frame you want to include for further analysis. If you are fine with the rest of the 
//       movie being tracked, leave the default of "-1".
//    Note that these are all 1-based indexing, i.e. they should match up with the number shown in 
//    the top left corner of the Image window in ImageJ.
//8. Hit 'ok', then enter the frames you just identified.
//9. Once you hit 'enter', the current image will close and the next image will be loaded, go to step 4.


//------ Review these variables first -------
// all input/output files will be in subdirectories of this directory
base_dir = "/Users/enorby/Thesis/Analysis/InputOutput/20220126_fromMS-CP/"; 
//Define the name of the target image (the one you want to register) from the folder name plus this suffix. 
//E.g. if the target image is located at base_dir/testimage/testimage_surfcut.ome.tif, then this variable
//should be '_surfcut.ome.tif'
target_image_suffix = "_SurfCut.ome.tif";

line_separator = "\n";
//This csv file speficies all the folders of datasets you want to look at. This file consists of a series
//of paths, one per line, that have the experiment_folder/dataset_folder, where the experiment_folder
//are all located in the base_dir specified above, and the dataset_folder is different on each line
//(there may be multiple datasets in one experiment folder). The dataset_folder will be extracted from 
//each string and used to identify the exact image in that folder to process.
data_path_list = split(File.openAsString(base_dir + 
                                         "20220126_MoviesToProcess.csv"), line_separator);
//The width of the subimage to crop. Generally good to have the subregion be at least 300px wide, though it can be 
//manually resized to be taller than 300 px.                                         
crop_image_size = 300;
//This is the filename for the output csv file that will contain further details for registration that
//will be passed to the registration algorithm
annotation_detail_file_name = base_dir + "Annotations_for_MS-CP_added20220126.csv";

//-------- Script continues from here --------------------
if (File.exists(annotation_detail_file_name)) {
	File.delete(annotation_detail_file_name);
}
//Set up the output csv file and print the first line
f = File.open(annotation_detail_file_name);
print(f,"subImageToRegister," + 
        "pathToImage," + 
        "registration_outputSuffix," +
        "firstTrueFrame," + 
        "firstTrackedFrame," +
        "lastFrame," + 
        "roi_xPosition," + 
        "roi_yPosition," +
        "apaxis_x0," + 
        "apaxis_y0," + 
        "apaxis_x1," + 
        "apaxis_y1," +
        "wound_anterior_x," +
        "wound_anterior_y");

for (i=0; i<lengthOf(data_path_list); i++) {
	record_number = i+1;
	print("Processing " + record_number + " of " + 
	       lengthOf(data_path_list) + " records...");
	//Compute the image name from the folder name
	tmp = File.getName(data_path_list[i]);
	target_image_name = tmp + target_image_suffix;
	file_name = base_dir + data_path_list[i] + target_image_name;
	
	//Open the target image
	open(file_name);
	original_image_title = getTitle();
	run("Enhance Contrast", "saturated=0.35");
	
	//get the A-P axis
	setTool("line");
	waitForUser("Axis Instructions", "Draw a line along the Anterior-Posterior axis,\nstarting at the anterior side of the field of view. Press space when done.\n\nThen select the anterior edge of the wound\narea (on the A-P axis, midway between the two lacerations). Press space when done.");
	while (!isKeyDown("space")) {
		wait(100);
	}
	getLine(x0, y0, x1, y1, line_width);
	line_selection = newArray(x0, y0, x1, y1);
	
	//get the extent of the wound
	setTool("point");
	setKeyDown("none");
	while (!isKeyDown("space")) {
		wait(100);
	}
	getSelectionCoordinates(x_wound, y_wound);
	print(x_wound[0]);
	wound_anterior_edge = newArray(x_wound[0], y_wound[0]);
	
	//Crop the image (wait for the user's input)
	waitForUser("Registration Instructions", "Move the box that appears to a stationary\narea for registration. Press space when done.");
	roi_selection = cropImageUserInput(original_image_title,crop_image_size);
	//Switch the slices and frames (only tested to work when one of them is 1).
	ensureProperDimensions();
	cropped_save_name = makeCroppedImageSaveName(file_name, 
                                             roi_selection, 
                                             crop_image_size);
    saveAs("Tiff", cropped_save_name);
	selectImage(original_image_title);

	waitForUser("Choose Start frame", "Find frame to start/end tracking");
	Dialog.createNonBlocking("Choose start and end times");
	Dialog.addNumber("First frame after wounding (1-based): ", 1);
	Dialog.addNumber("First frame to start tracking (1-based): ", 1);
	Dialog.addNumber("Last frame to track (enter -1 for last frame): ", -1);
	Dialog.show();
	initial_frame = Dialog.getNumber();
	start_frame = Dialog.getNumber();
	last_frame = Dialog.getNumber();
	initial_frame_zero_idx = initial_frame - 1;
	start_frame_zero_idx = start_frame - 1;
	if (last_frame > 0) {
		last_frame_zero_idx = last_frame - 1;
	}
	else {
		last_frame_zero_idx = last_frame;
	}
	detail_string = printDetailsForRegistration(base_dir, 
	                                            data_path_list[i],
	                                            target_image_suffix,
	                                            cropped_save_name,
	                                            roi_selection,
	                                            line_selection,
	                                            wound_anterior_edge,
	                                            initial_frame_zero_idx,
	                                            start_frame_zero_idx,
	                                            last_frame_zero_idx);
	print(f,detail_string);
	
	run("Close All");
	wait(50);
	
}

File.close(f);

function getTodaysDateStr(){
	//Return a string yyyymmdd for today's date
	getDateAndTime(year, month, dow, day, h,m,s,ms);
	//month and day are zero-indexed
	date_str =  d2s(year,0) + IJ.pad(month + 1, 2) + IJ.pad(day, 2);
	return date_str
}

function makeCroppedImageSaveName(file_name,selection,crop_image_size) {
	// Generate the save name for a cropped image based on the name of the original file.
	// Take off the .tif, and add _x<X>_y<Y>_<CROP> where X,Y,CROP are the x and y coordinates
	// and size of the cropped image.
	L = lengthOf(file_name);
	base_name = substring(file_name,0,L-4);
	save_name = base_name + 
	            "_x" + IJ.pad(selection[0],3) + 
	            "_y" + IJ.pad(selection[1],3) + 
	            "_" + IJ.pad(crop_image_size,3) + 
	            ".tif";
	return save_name;
}

function printDetailsForRegistration(base_dir,
                                     data_path,
                                     max_z_suffix,
                                     cropped_save_name,
                                     roi_selection,
                                     line_selection,
                                     wound_anterior_edge,
                                     initial_frame,
                                     start_frame,
                                     last_frame) {
    // Create a comma-separated line of details about each file.
    // Will be printed to a csv to use for registration
	L = lengthOf(data_path);
	idx_last_portion = lastIndexOf(substring(data_path,0,L-1), "/");
	full_image_file_name = base_dir + 
	                       data_path + 
	                       substring(data_path,idx_last_portion+1,L-1) + 
	                       max_z_suffix;
	date_str = getTodaysDateStr();
	//Add this to the end of registered files
	registration_output_suffix = "registered_" + date_str;
	line_array = newArray(cropped_save_name, 
	                      full_image_file_name,
	                      registration_output_suffix,
	                      initial_frame, 
	                      start_frame, 
	                      last_frame, 
	                      roi_selection[0], 
	                      roi_selection[1],
	                      line_selection[0],
	                      line_selection[1],
	                      line_selection[2],
	                      line_selection[3],
	                      wound_anterior_edge[0],
	                      wound_anterior_edge[1]);
	line = arrayToCommaSeparatedString(line_array);
	return line;
}

function arrayToCommaSeparatedString(array_to_print) {
	// concatenate an array into a comma-separated string
	L = lengthOf(array_to_print);
	array_as_string = array_to_print[0];
	for (i=1; i<L; i++) {
		array_as_string = array_as_string + "," + array_to_print[i];
	}
	return array_as_string;
}

function cropImageUserInput(image_to_crop_title,crop_image_size) {
	// Generate a square window in the image and allow the user to 
	// move it to select a region for cropping.
	// Save the x and y coordinates of the cropped region.
	selectImage(image_to_crop_title);
	makeRectangle(100, 100, crop_image_size, crop_image_size);
	
	while (!isKeyDown("space")) {
		wait(10);
	}

	getSelectionBounds(selection_x, selection_y, w, h);
	selection_array = newArray(selection_x, selection_y);
	// Crop by duplicating
	run("Duplicate...", "duplicate");
	return selection_array;
}

function ensureProperDimensions() {
	//Make sure active image has z as singleton dimension, not t.
	getDimensions(w, h, num_channels, num_slices, num_frames);
	if (num_slices > 1 && num_frames == 1) {
		run("Properties...", "slices=" + num_frames + 
		                     " frames=" + num_slices);
	}
}
