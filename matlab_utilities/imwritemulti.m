function imwritemulti(im,writename)
%%% Write image to file for multi-page TIFFs especially. If the file
%%% doesn't exist, start a new one, otherwise append to the existing file.
if ~exist(writename,'file')
    imwrite(im,writename);
else
    imwrite(im,writename,'writemode','append');
end