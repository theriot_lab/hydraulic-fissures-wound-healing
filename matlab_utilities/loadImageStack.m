function imStack = loadImageStack(fName)
%%% Load a 3D image stack into Matlab, either from a folder of images
%%% or a single file Tiff stack.
%%% Input:
%%% - fName: The absolute path to the folder or filewhere the images are
%%%   located. For a Tiff Stack file, this name should end in .tif. For a folder,
%%% this should be the absolute path to a folder containing only the tifs
%%% for this stack, split by timepoint (each a 2D image).
%%% Output:
%%% - imStack: a stack with the same datatype as the images in the folder,
%%% with dimensions [Y,X,T], where Y is the # of rows, X is the # of
%%% columns, and T is the # of images in the stack.

if endsWith(fName,'.tif') %Open as a stack
    reader = bfGetReader(fName);
    sizeT = reader.getSizeT();
    %Load the first image
    im = bf_getFrame(reader, 1, 1, 1);
else %make a list of tif files to combine
    fileList = dir(fullfile(fName,'*.tif'));
    fileList = {fileList.name}; %get just the names into a cell array
    sizeT = numel(fileList);
    im = imread(fullfile(fName,fileList{1}));
end

%Use first image to initialize the stack (datatype and size)
imStack = zeros([size(im), sizeT], class(im));
imStack(:, :, 1) = im;
%Load the rest of the image stack
for iT = 2:sizeT
    if endsWith(fName, '.tif')
        imStack(:, :, iT) = bf_getFrame(reader, 1, 1, iT);
    else
        imStack(:, :, iT) = imread(fullfile(fName, fileList{iT}));
    end
end

%Clear reader from memory
if endsWith(fName, '.tif')
    reader.close()
end
end