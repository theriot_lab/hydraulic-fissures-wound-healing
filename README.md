# Post-injury hydraulic fracturing drives fissure formation in the zebrafish basal epidermal cell layer 

Shared code etc for project studying cell separation during wound healing in zebrafish larvae.

This is code written for this publication by Andrew Kennard, Mugdha Sathe, and Ellen Labuz. It is not intended as a ready to use package for other data sets.

# Separation analysis by ridge detection

## Folders:
*  `matlab_utilities`: some functions for file I/O mostly
*  `preprocessing_deconvolution`: MATLAB scripts to prepare raw data for deconvolution in Huygens
*  `preprocessing_separation_detection`: a set of ImageJ macros to prepare data for further analysis. 
These involve some active participation from the user to draw boxes, lines, etc. and
ImageJ stores the results of those actions in CSV files that are used for subsequent
processing.
*  `deconvolution_notes`: A README about doing batch deconvolution in Huygens, and some utility scripts to
help with that (e.g. processing PSFs for use in Huygens)
*  `surface_projection`: ImageJ macro to run GPU-accelerated SurfCut-based surface projection using CLIJ2. This 
replaces the older implementation based on MATLAB scripts from the `epitools` library.
*  `separation_detection`: (old) MATLAB scripts to separate the top and bottom surfaces
(using code from `epitools`) and then to apply a ridge-based feature detector to find
"ridge-y" regions which are likely to be cell separation. NOTE: the epitools portion is out of date; now this
step in the pipeline is accomplished by the code in the `surface_projection` folder.
*  `kymograph_analysis`: a set of scripts to generate kymographs of "ridge-y" regions
over time, in MATLAB.
* `FastAccurateBilateralFilter`: from Matlab FileExchange, an edge-preserving
smoothing filter. Alternative to gaussian smoothing that keeps edges, but requires
more computation time.

## Pipeline:

0.  pre-preprocessing to prepare raw data for deconvolution in Huygens, and 
transfer data to Neuron/GPU workstation. A more detailed pipeline for these steps is available in the README in the 
[`preprocessing_deconvolution` folder](preprocessing_deconvolution).
    * _Input_: raw data (big `.ome.tif` files)
    * _Output_: raw data split into timepoints (XYZ(C) stacks)
    * [README](preprocessing_deconvolution)
1.  Deconvolution (on Neuron/GPU with Huygens)
    * _Input_: raw data split into timepoints (XYZ(C))
    * _Output_: deconvolved data split into timepoints and channels (XYZ)
    * incomplete [README](deconvolution_notes)
2.  Selective plane projection (e.g. SurfCut) also on Neuron, which yields 2D images. Previously
this was done with the `run_selectivePlaneProjection.m` script in the `separation_detection` folder,
but that method doesn't work as well and is slow, so I would not recommend.
    * _Input_: raw or deconvolved data split into timepoints and channels (XYZ)
    * _Output_: 2D projections over time, split into timepoints and channels (folders of XY images)
    * [README](surface_projection)
3.  Transfer from Neuron to a workstation running MATLAB and Python
4.  Run the ImageJ scripts in the `preprocessing_separation_detection` folder. As an optional substep you might
also register the images using the Python scripts in this folder.
    * _Input_: Folder full of 2D (XY) images from different timepoints/channels and different experiments
    * _Output_: 3D stacks (XYT) as `.ome.tif` files, plus csv files with information about
    annotations that have been generated for each dataset (like the position of the A-P axis)
    * [README](preprocessing_separation_detection)
5.  Detect the separation by running `enrichSeparation.m` in the `separation_detection` folder
    * _Input_: 3D stacks of 2D projections (XYT), or folders of 2D image split by timepoint (XY)
    * _Output_: `.tif` and `.mat` files containing the ridge detection result (XYT)
    * [README](separation_detection)
6.  Analyze the ridge detection result by running `scriptKymographAnalysis.m` in the `kymograph_analysis`
folder
    * _Input_: `.mat` file of the ridge detection result (XYT)
    * _Output_: `.mat` files containing kymographs and accessory information, as well as plots, which
    you can save manually.
    * [README](kymograph_analysis)

## Some initial data
40x movies of wounding of ∆Np63:Gal4 x UAS:mCherry fish, 3dpf, wounded with a needle.
Scales, timestamps, should be in the metadata (at least of the raw data).

All locations are on Theriot Nas 2:
Raw Data:
*  `/homes/akennard/Data/2020-02-08_3dpf_Np63Gal4_UASmCherry`
   *  9 movies. Should include RFP stacks and brightfield (one image). Shoudl also
include before and after wounding in the same stack. If it looks wounded in the
first stack, it's probably a continuation of the previous movie.
* `/homes/akennard/Data/2020-01-29_3dpfNp63Gal4_UASmCherry`
   * 1 dataset, similar properties to the previous dataset

Maximum intensity projections, and potentially also selective plane projections:
*  `/homes/akennard/Analysis/2020-02-08_3dpf_Np63Gal4_UASmCherry`
*  `/homes/akennard/Data/2020-01-29_3dpfNp63Gal4_UASmCherry`

The file trees within `Analysis` and `Data` parallel each other.
