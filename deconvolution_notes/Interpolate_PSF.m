%%%Quick and dirty script to interpolate a psf which is sampled at a
%%%particular spacing in z. This only does interpolation in z. It also
%%%requires some trial and error to make sure you specify the interpolated
%%%coordinates correctly (to be symmetric and matching the range of the
%%%original data as close as possible).
%%% NOTE: This script does NOT currently normalize your PSF appropriately.
%%% For Huygens, I think you need to divide the PSF by the sum of the whole
%%% 3d matrix (so that it sums to 1) and then save it as a single or
%%% double. I have found it easiest to do this in ImageJ (hard to save
%%% 32-bit images in MATLAB).

%Set the xy grid (spacing is arbitrary, just match the number of pixels)
xv = 1:22;
yv = 1:22;
%Set the original z spacing in microns. Test out this range to make sure it
%is symmetric about 0 (or matches the asymmetry in the original psf)
zv = -5:.2:5;
%Specify the 3d coordinates of the original psf
[X,Y,Z] = meshgrid(xv,yv,zv);
%Choose the new spacing for the psf. Test out to make sure this is
%symmetric about 0, is the desired spacing in microns, and includes as much
%of the original psf data as possible (i.e. if original extents 5�m in
%either direction, try to collect as much but no more than 5�m in the
%interpolation)
zvq = -4.8:0.6:4.8;
%Specify the 3d coordinates of the new psf (note X and Y coordinates are
%the same!)
[Xq,Yq,Zq] = meshgrid(xv,yv,zvq);
%Use interp3 to interpolate the PSF
newPSF = interp3(X,Y,Z,PSF3D,Xq,Yq,Zq);

%Save the unnormalized psf as a .mat file; make sure to specify the new z-spacing in the
%title
save('20201011_40x_488_z600nm.mat','newPSF');

% Save the PSF as a 16-bit image for normalization in ImageJ
%Adjust the scaling so that there is plenty of dynamic range in the PSF
%(this scaling to a max of 60000 is arbitrary and will be corrected in
%IMageJ by normalization later).
PSF3Dint16 = uint16(newPSF*60000/double(max(newPSF(:))));
disp(['The summed value of your PSF is ' num2str(sum(PSF3Dint16(:)))]); %divide by this number in ImageJ
fname = '20201011_40x_488_z600nm';

% Write the first plane
imwrite(PSF3Dint16(:,:,1),[fname '.tif'],'tif')

% Write the others by appending
for i = 2:size(PSF3Dint16,3)
    imwrite(PSF3Dint16(:,:,i), [fname '.tif'],'writemode', 'append');
end
