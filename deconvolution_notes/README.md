# Using Huygens for batch deconvolution
Using Huygens is a pain. I did a lot of this via ssh which was not fun. I think you need to do `ssh -Y <username>@<Neuron IP address>` and then type `huygenspro` and it will launch a GUI window on your home computer to do this. The GUI window is very slow and not responsive. If you can set things up directly on Neuron that is the best.

I am also forgetting exactly what the interface is like, and without a valid license I can't test run it remotely. Probably easiest to walk through it via Zoom to cover all the bases. 

What I do know you need is a point-spread function that is properly normalized (I think all the voxels have to sum to 1) which I typically did in ImageJ. And you need to make sure you have a point-spread function for each channel you will process. And you need to make sure the PSF is sampled to match the z-spacing of your z-stacks. I've included a script here to do that interpolation. It is very rough, use the comments to guide you through, and you'll have to do a little testing at the MATLAB console to get the coordinates right for the interpolation.
