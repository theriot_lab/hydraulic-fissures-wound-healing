/* 
  ****** GPU ENABLED SURFCUT SURFACE PROJECTION - DIFFERENT CHANNEL *********
  This script can use a previously calculated mask of surface projection of a 
  given channel to compute a different surface projection based on that mask,
  potentially of a different channel. This is useful if the channel you are 
  interested in would not be amenable for the direct surface projection (i.e. 
  it is patchy, sparse, or punctate) but has a defined geometric relationship
  to a more amenable channel (e.g. two fluorescent markers expressed in the
  same cell type). Use surfcut_clij_.ijm to compute a surface projection prior
  to using this script. 
  "Above" the surface is a region of no signal; "below" the surface there 
  could be more signal (i.e. the rest of the fish, or other
  layers of tissue you are interested in). 
  
  Example usage
  -------------
  Command line:
       Fiji.app asdfasdfadfad
  In Fiji: 
    Run this script, and a window will pop up and ask for the same parameters
    described below
  
  Parameters
  ----------
  - input_list: a list of paths, one per line, pointing to image files
    to be processed
  - mask_folder: A path to a folder where the surfcut projectsion will be saved.
      Mask files will have the same filename as the corresponding input file,
      with the only difference being the suffix of the filename, i.e. the text
      occurring betweeen the last underscore and the file extension. Mask
      files will end with _surfMask.tif.
  - cut1, cut2: integers, # of slices above OR below the detected surface to 
      start/stop including in the projection, respectively. This allows you
      to use a projection of one tissue layer to compute a new surface projection
      of a different cell layer (expressing the same or different signal). i
      cut1 can be positive OR negative: negative values correspond to
      slices that are "above" the detected surface, and positive values 
      correspond to slices "below". cut2 must be nonnegative and >= cut1
  - save_suffix: the suffix to add to the end of the filename for this surface
      projection (to distinguish it from the other projection you may have 
      already made, which will have a suffix of 'surfcut'. The suffix will go
      after an underscore and before the file extension.
  - make_maxz: if 1, this script will also save a maxz projection of each
      image. Default: 0
  - flip_stack: (0/1) Select 1 if your desired surface is towards the back of the 
      stack, because the algorithm looks for the surface closest to the beginning.

  Output
  ------
  - surfcut: a 2D image of the surface projection. Will be saved with the suffix
      specified by save_suffix
  - maxz: (if make_maxz is set to 1 or True)
      a 2D image of the regular maximum-intensity projection. Will be saved
      with the suffix <save_suffix>_maxZ
  - mask: if save_mask_stack = 1, a 3d mask which is 1 for areas of the original
      3d image that are "below" the detected surface and 0 for areas that are
      "above" the projected surface. This can be used to compute a surface
      projection of a different channel, or with different cut1 and cut2 values
*/

//========== Scripting Parameters ==============
//input_list: a list of filenames, one per line, to be projected
#@ String (label="File containing paths to process:") input_list
//mask_folder: folder containing the surface masks to use to generate this new
//projection. Masks are assumed to have the same filename as the corresponding
//input file, except for a different suffix (between the last 
//underscore and the file extension e.g. <base_filename>_<suffix>.tif
#@ String (label="Folder with surface masks:") mask_folder 
//save_mask_stack: if 1, save the projection and the 3D stack of the mask
//(for use by other programs)
#@ Integer (label="start slice (rel. to surface):") cut1 
//cut2: # of slices above OR below detected surface to START including in the 
//surface projection. Negative values will select regions above the detected 
//surface; positive will select below. NOTE: cut2 must be > cut1.
#@ Integer (label="stop slice (rel. to surface):") cut2 
#@ String (visibility=MESSAGE, value="(NOTE: slices can be positive or negative integers. stop slice must be greater than start slice)", required=false) msg1
//save_suffix: string, the suffix that will be put on the newly computed 
//surface projection. Do NOT include the file extension in this name!
#@ String (label="suffix for saving:") save_suffix
#@ String (visibility=MESSAGE, value="(NOTE: do NOT include file extension in save suffix!)", required=false) msg2
//make_maxz: if 1, save a maxZ image of this channel in addition to the surface
//projection
#@ Boolean (label="save maxZ projection?", default=0) make_maxz
//flip_stack: if 1, flip the raw stacks (before detecting the 
//surface closest to the front)
#@ Boolean (label="Flip stack?", default=0) flip_stack 

//=========== Main Script =====================
//Initialize the connection to the GPU. Commands starting with 'Ext.CLIJ2_' 
//will be run on the GPU.
run("CLIJ2 Macro Extensions", "cl_device=Titan Xp");
//Process list of files (split by newlines into an array)
filestring = File.openAsString(input_list);
filelist = split(filestring,"\n"); 

//Validate inputs
if (!File.exists(mask_folder)) { exit("mask_folder does not exist!"); }
if (cut2<=cut1) {exit("cut2 must be greater than cut1");}
if (cut2<0) {exit("cut2 must be nonnegative");}
if (save_suffix == "surfcut") {exit("save suffix cannot be 'surfcut' (that is already in use!");}
if (!endsWith(mask_folder, File.separator)) {mask_folder = mask_folder + File.separator;}
if (!startsWith(save_suffix, "_")) {save_suffix = save_suffix + "_";}

setBatchMode("hide");

//Loop through files
for (i=0; i<lengthOf(filelist); i++) {
	//Start timing this round and setup file names
	print(filelist[i]);
	t1 = getTime();
	dir = File.getDirectory(filelist[i]);
	name = File.getNameWithoutExtension(filelist[i]);
	surfcut_savename = mask_folder + name + save_suffix + ".tif";
	maxz_savename = mask_folder + name + save_suffix + "_maxZ.tif";

	//Clear out the GPU before starting	
	Ext.CLIJ2_clear();
	
	//Open the existing mask and input image
	mask_name = name + "_surfMask.tif";
	open(mask_folder + mask_name);
	mask = getTitle();
	open(dir + name + ".tif");
	input = getTitle();
	getVoxelSize(px_x, px_y, px_z, unit);
	dimensions = newArray(px_x, px_y, px_z, unit);
	
	//Load the new image onto the GPU and define the names of the outputs
	//these names will be used as handles to initiate containers for the
	//outputs on the GPU
	Ext.CLIJ2_push(input);
	Ext.CLIJ2_push(mask);
	surfcut = "surfcut";
	max_z = "max_z";

	//Flip the z (need the side closest to coverslip at the beginning)
	getDimensions(width, height, channels, slices, frames);
	Ext.CLIJ2_create3D(im_flipped, width, height, slices, 16);
	if (flip_stack) {
		Ext.CLIJ2_flip3D(input, im_flipped, 0, 0, 1);
	} else {
		im_flipped = input;
	}

	//Run Surfcut using an existing mask	
	getSurfCutFromEdgeMask(cut1, cut2, im_flipped, mask, max_z, surfcut);
	wait(100);
	selectWindow(input);//close the input image
	close();
	selectWindow(mask); //close the mask
	close();
	save_with_metadata_and_close(surfcut, surfcut_savename, dimensions);
	if (make_maxz) {
		save_with_metadata_and_close(max_z, maxz_savename, dimensions);
	
	//Display runtime for this file
	t2 = getTime();
	elapsed_time = (t2 - t1) / 1000;
	print("Time spent on image listed above: " + elapsed_time + " seconds");
}

setBatchMode("exit and display");
Ext.CLIJ2_reportMemory();

// ============== Function Definitions ==================
function save_with_metadata_and_close(window_name, save_name, dimensions) {
	//Pull image from the GPU, add pixel dimension metadata, and save it
	//window_name: string referring to name of an object on GPU to be pulled
	//             back onto GPU
	//save_name: the path to save the image to
	//dimensions: 4-member array, (x,y,z, unit) in units of unit/pixel
	Ext.CLIJ2_pull(window_name);
	selectWindow(window_name);
	setVoxelSize(dimensions[0], dimensions[1], dimensions[2], dimensions[3]);
	save(save_name);
	close()
}

function newImageToGPU(title, type, width, height, depth) {
	//Initialize a dummy image to be pulled off GPU later. Make sure to delete the image 
	//after it is pushed to GPU to save ImageJ CPU memory
	//Parameters are the same as macro function newImage
	newImage(title, type, width, height, depth);
	Ext.CLIJ2_push(title);
	selectWindow(title);
	close();
}

function getSurfCutFromEdgeMask(cut1, cut2, im_flipped, mask, max_z, surfcut){
	//Compute a surface projection using an existing mask from a different
	//run of surfcut
	selectWindow(input);
	getDimensions(width, height, channels, slices, frames);
	
	//Initialize some objects to hold intermediate processing on GPU 
	Ext.CLIJ2_create3D(im_masked_1,         width, height, slices,       16);
	Ext.CLIJ2_create3D(im_masked_2,         width, height, slices,       16);
	Ext.CLIJ2_create3D(im_surface,          width, height, slices,       16);
	
	//Generate padding based on cut2 slices
	Ext.CLIJ2_create3D(start_padding, width, height, cut2, 8);
	Ext.CLIJ2_set(start_padding, 255);
	if (cut1 < 0) { //projection includes area above detected surface
		//Pad the stack on both sides
		Ext.CLIJ2_create3D(end_padding, width, height, -cut1, 8);
		Ext.CLIJ2_create3D(m1, width, height, cut2 + slices, 8);
		Ext.CLIJ2_create3D(master_mask, width, height, cut2 -cut1 + slices, 8);
		Ext.CLIJ2_set(end_padding, 255);
		Ext.CLIJ2_concatenateStacks(start_padding, mask, m1);
		Ext.CLIJ2_concatenateStacks(m1, end_padding, master_mask);
	}
	else {
		Ext.CLIJ2_create3D(master_mask, width, height, cut2 + slices, 8);
		Ext.CLIJ2_concatenateStacks(start_padding, mask, master_mask);
	}
	//Generate containers to hold outputs on GPU
	newImageToGPU(surfcut, "16-bit black", width, height, 1);
	newImageToGPU(max_z, "16-bit black", width, height, 1);
	//Get the regular maximum intensity projection done up front
	//(so these arrays can be re-used later)
	Ext.CLIJ2_maximumZProjection(im_flipped, max_z);

	//Crop a substack of the mask starting at the first slice (one of the padded slices)
	Ext.CLIJ2_crop3D(master_mask, mask, 0, 0, 0, width, height, slices);
	//Mask the original image with this mask
	Ext.CLIJ2_mask(im_flipped, mask, im_masked_1);
	//Now make a second substack of the master mask starting at cut2 + 1 - cut1 going to end-cut1
	//and create a second masked copy of the original image
	Ext.CLIJ2_crop3D(master_mask, mask, 0, 0, cut2 - cut1, width, height, slices);
	Ext.CLIJ2_mask(im_flipped, mask, im_masked_2);
	//Subtract the first masked image from the second
	Ext.CLIJ2_subtractImages(im_masked_2, im_masked_1, im_surface);
	//Create the surfcut
	Ext.CLIJ2_maximumZProjection(im_surface, surfcut);
	//Clear images
	Ext.CLIJ2_release(input);
	Ext.CLIJ2_release(im_flipped);
	Ext.CLIJ2_release(start_padding);
	if (cut1 < 0) {
		Ext.CLIJ2_release(end_padding);
		Ext.CLIJ2_release(m1);
	}
	Ext.CLIJ2_release(mask);
	Ext.CLIJ2_release(master_mask);
	Ext.CLIJ2_release(im_masked_1);
	Ext.CLIJ2_release(im_masked_2);
	Ext.CLIJ2_release(im_surface);
}
