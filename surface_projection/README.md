# Running GPU-enabled Surface Projection
I have two scripts to perform surface projections. Both are massively sped-up
by using [CLIJ2](https://clij.github.io), an extension to Fiji that allows 
for GPU computing on pretty much any hardware. I reimplemented the 
[SurfCut](https://github.com/sverger/SurfCut)
algorithm to make use of CLIJ2 commands, which affords a significant speed up.

There are two scripts: 
  * `surfcut_clij_.ijm` is for computing surface projections of bright, relatively
uniform signals (e.g. cytoplasmic FP). You give it 3D stacks, specify how many
slices above and below the detected surface you want to include in your projection,
and it will compute a surface projection.
  * `surfcut_from_existing_mask_.ijm` is for computing surface projections
of one channel when you already have a surface projection of a different channel.
This can be helpful to e.g. get a surface projection of ecadherin or vesicles 
(neither of which would make a good surface projection on their own) using the 
information from the surface projection of a cytoplasmic marker.

Each of the scripts is documented with their inputs and outputs.

## Setting up Fiji from the command line
This is helpful if you are doing this via ssh. If you can actually physically
sit at the computer, then you can probably do this by following the directions
on the CLIJ2 website.
  1. Download Fiji executables for Linux 64-bit
  2. Unzip Fiji
```
~$ unzip <path_to_fiji.zip>
```
(note that you don't actually type `~$`; it's a stand-in for the terminal 
prompt -- yours will look different!)

  3. Run Fiji and specifically update it (Remember that Fiji is just ImageJ!) in "headless" mode (no GUI). Have messages posted to the
terminal using the `--console` flag. Suppose Fiji is installed to 
`/home/andrew/Fiji.app/ImageJ-linux64`:
```
~$ /home/andrew/Fiji.app/ImageJ-linux64 --headless --console --update update
```

  * To avoid writing the part up to `--console` each time you want to do something
with Fiji at the command line, you can define a nickname or "alias" that will
stand in for all that text. Open the file `~/.bashrc` with your favorite
terminal text editor (mine is `vim`, so e.g. type `vim ~/.bashrc`). You can
define the alias in this standard bash configuration file (you can google bashrc
if you are curious). In this open file type

```
alias ImageJ="/home/andrew/Fiji.app/ImageJ-linux64 --headless --console"
```
where the path is changed to match where Fiji is installed for you. Note that the 
quotes and lack of spaces around the equals sign are important! Save and
close this file, and the next time you open the terminal and type `ImageJ` it will
be like you typed everything you put in quotes!

  4. Add two CLIJ update sites to Fiji and update again (for brevity I'm going to
assume you made the alias described above)
```
~$ ImageJ --update add-update-site clij https://sites.imagej.net/clij/
~$ ImageJ --update add-update-site clij2 https://sites/imagej.net/clij2/
~$ ImageJ --update update
```

## How-to run the surface projection
  1. You first need to have 3D stacks (XYZ, one channel, one timepoint) ready
to go. 
  2. You need a list of files for the algorithm to process, one filename per line.
I typically do this using the command line. For  example I would use a command
like
```
~$ find $(pwd) -maxdepth 1 -regex <pattern> > files_to_process.txt
```
Breaking that down:
  * `~$` you don't actually type this, it just indicates the start of the terminal
prompt. Your prompt may look different!
  * `find` is a command that lists files within a given directory that
match a pattern. It is very powerful but that means sometimes you need to
give it more information than you might think it needs.
  * `$(pwd)` will be substituted with the working directory (pwd stands for 
print working directory) by your terminal. This tells `find` to look in the 
current working directory for files. You can change to a different directory
if you want; if you want to specify an explicit directory you would not use the 
`$` or parentheses which do the substitution. For example `find /usr/andrew` would
look in the `/usr/andrew` directory.
  * `-maxdepth 1` tells `find` not to look in subfolders of the directory.
This is important because if you accidently tell `find` to look in a high-level
folder (like your `home` folder) and you don't specify not to look in subfolders,
it might find a TON of images and it will take ages to finish. If you do this 
by mistake and find is taking a long time, you can always hit control-C and 
terminate the process!
  * `-regex <pattern>` tells find to only list filenames matching the
regular expression `<pattern>`. You can look up the manual for `find` to do
things other than regular expressions, but this is a flexible option to
select only tif files, or onyl tif files with a certain suffix, etc.
  * `>` tells the terminal to write the output of `find` (a list of files)
to a new file, with the filename specified immediately after (in this case
`files_to_process.txt`).

As a concrete example, the following code:
```
~$ find $(pwd) -regex ".*ch[0-9]{2}.*\.tif" > my_files.txt
```
would make a new file `my_files.txt` that contained a list of files in the
current working directory whose filename contained the text `chXX` somewhere,
where XX is any two-digit number from 00 - 99, and also ended with the suffix
`.tif`. Sorry this is a lot of command line! But I think that writing out 
hundreds of filenames would drive me even more insane.

  3. Once you have your list of filenames you want to process, run the
`surfcut_clij_.ijm` file. If you are sitting at the computer, you can probably
just do this in ImageJ. If you are doing this remotely via ssh, you will 
need to use headless mode.
To run the file, make sure you are in the same directory where the script is 
located then type
```
~$ /home/andrew/Fiji.app/ImageJ-linux64 --ij2 --headless --console --run surfcut_clij_.ijm 'input_list="/home/my_file.txt",output_folder="/home/andrew/output",save_mask_stack=1,cut1=0,cut2=10'
```
Breaking that down:
  * `/home/andrew/Fiji/app/ImageJ-linux64 --ij2 --headless --console --run` 
is the command to run scripts. Of course this is different than for adding update sites as above.
You can follow the instructions above to add another alias for this command (I 
renamed this whole mess to `IJ2`)
  * `surfcut_clij_.ijm` is the name of the file
  * All the parameters of the file are included in single quotes, separated
by commas. Strings are delineated by double quotes. You can see the names
of all the parameters by looking in the code!

## Notes on using and interpreting the code
  * CLIJ2 is under very active development. It is possible that some of the code
is already obsolute and may not work; furthermore the documentation for these 
functions is not always the best, so it can be hard to tell why they failed.
The good news is that the developer, Robert Haase is
very responsive on the ImageJ forum if you have questions, and is very kind to 
newbies
  * The main thing in GPU programming is that allocating memory and transferring
data from the CPU to GPU are often the slowest steps. Optimizing for speed can 
mean reusing arrays that hold intermediate computational steps, and minimizing
the number of images you transfer back and forth. This can make the code a bit
harder to read, since the variable names may not accurately represent what is 
contained within the array at that moment. I have tried to comment extensively
to help for debugging purposes. 
  * The SurfCut algorithm is not super intuitive. Essentially you threshold
a stack slice by slice, then you add slices to the stack to offset the 
detected region up or down and then subtract the offset masks from each other,
which should leave you with a thin band of mask which selects just the region
you want to use for your projection (maximum intensity projection in the masked
region). If you need to dive into the algorithm itself, I strongly recommend
using pen and paper and working it out step by step to help you understand what
is going on.
