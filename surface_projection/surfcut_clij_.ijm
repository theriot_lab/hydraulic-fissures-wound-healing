/* 
  ****** GPU ENABLED SURFCUT SURFACE PROJECTION *********
  This script will generate a surface projection from a 3D dataset, following
  the contours of the signal in the image. This works well for detecting 
  bright, relatively uniform signals, and finds an "edge" in the z-direction,
  which is your surface. To detect different types of signal (e.g. punctate
  signals) it is best to detect a uniform bright signal with this script and 
  then the script XXX can take the mask from this script and use it to make
  a surface projection of the other less amenable signal. 
  "Above" the surface is a region of no signal; "below" the surface there 
  could be more signal (i.e. the rest of the fish, or other
  layers of tissue you are interested in). 
  
  Example usage
  -------------
  Command line:
       Fiji.app asdfasdfadfad
  In Fiji: 
    Run this script, and a window will pop up and ask for the same parameters
    described below
  
  Parameters
  ----------
  - input_list: a list of paths, one per line, pointing to image files
    to be processed
  - output_folder: A path to a folder where the surfcut projectsion will be saved
  - save_mask_stack: (0/1) if 1, save the projection and the 3d stack of the
      mask that defines the surface. This is useful to generate a surface
      projection of another channel
  - flip_stack: (0/1) Select 1 if your desired surface is towards the back of the 
      stack, because the algorithm looks for the surface closest to the beginning.
  - cut1, cut2: nonnegative integers, # of slices below the detected surface to 
      start/stop including in the projection, respectively. This allows you
      to account for the thickness of the cell layer you are interested in 
      projecting, or if you want to project signal from a deeper tissue layer
      based on signal in a more superficial tissue layer (of the same signal.
      To make a projection of a different channel, use the other script).

  Output
  ------
  - surfcut: a 2D image of the surface projection. Will be savd with the suffix
      _surfcut
  - maxz: a 2D image of the regular maximum-intensity projection. Suffix: _maxZ
  - mask: if save_mask_stack = 1, a 3d mask which is 1 for areas of the original
      3d image that are "below" the detected surface and 0 for areas that are
      "above" the projected surface. This can be used to compute a surface
      projection of a different channel, or with different cut1 and cut2 values
*/
//========== Scripting Parameters ==============
//input_list: a list of filenames, one per line, to be processed
#@ String (label="File containing paths to process:") input_list
//output_folder: where to save all the outputs
#@ String (label="Folder to save output:") output_folder 
//save_mask_stack: if 1, save the projection and the 3D stack of the mask
//(for use by other programs)
#@ Boolean (label="Save mask stack?", default=1) save_mask_stack 
//flip_stack: if 1, flip the raw stacks (before detecting the 
//surface closest to the front)
#@ Boolean (label="Flip stack?", default=0) flip_stack 
//cut1: # of slices below detected surface to START including in the surface
//projection. Must be >= 0
#@ Integer (label="start slice (rel. to surface):") cut1 
//cut2: # of slices below detected surface to STOP including in the surface
//projection. Must be >= 0
#@ Integer (label="stop slice (rel. to surface):") cut2 


//=========== Main Script =====================
//Initialize the connection to the GPU. Commands starting with 'Ext.CLIJ2_' 
//will be run on the GPU.
run("CLIJ2 Macro Extensions", "cl_device=Titan Xp");
//Process list of files (split by newlines into an array)
filestring = File.openAsString(input_list);
filelist = split(filestring,"\n"); 
if (!File.exists(output_folder)) {
	File.makeDirectory(output_folder);
}

setBatchMode("hide");

//Loop through files
for (i = 0; i < lengthOf(filelist); i++) {
	//Setup
	print(filelist[i]);
	t1 = getTime();
	surfcut_savename = output_folder + File.getNameWithoutExtension(filelist[i]) + "_surfcut.tif";
	mask_savename = output_folder + File.getNameWithoutExtension(filelist[i]) + "_surfMask.tif";
	maxz_savename = output_folder + File.getNameWithoutExtension(filelist[i]) + "_maxZ.tif";
	
	//Clear out the GPU before starting
	Ext.CLIJ2_clear();

	//Open image
	open(filelist[i]);
	input = getTitle();
	getVoxelSize(px_width, px_height, px_depth, unit);
	dimensions = newArray(px_width, px_height, px_depth, unit);

	//Load the new image onto the GPU and define the names of the outputs
	//these names will be used as handles to initiate containers for the
	//outputs on the GPU
	Ext.CLIJ2_push(input);
	surfcut = "surfcut";
	max_z = "max_z";
	mask = "mask";

	//Flip the z (need the side closest to coverslip at the beginning)
	getDimensions(width, height, channels, slices, frames);
	Ext.CLIJ2_create3D(im_flipped, width, height, slices, 16);
	if (flip_stack) {
		Ext.CLIJ2_flip3D(input, im_flipped, 0, 0, 1);
	} else {
		im_flipped = input;
	}

	//Run SurfCut twice with and without logarithm
	surfCutGPUTwoPass(cut1, cut2, im_flipped, surfcut, max_z, mask);
	selectWindow(input);//close the original (3D) image
	wait(100);
	close();

	//Pull the surface projection (2D) from the GPU, set metadata, and save
	save_with_metadata_and_close(surfcut, surfcut_savename, dimensions);
	
	//Pull the maxZ projection (2D) from the GPU, set metadata, and save
	save_with_metadata_and_close(max_z, maxz_savename, dimensions);	
	
	//Pull the 3D mask used to make surface projection, set metadata, close
	if (save_mask_stack) {
		save_with_metadata_and_close(mask, mask_savename, dimensions);
	}
	wait(100);

	//Display runtime for this file
	t2 = getTime();
	elapsed_time = (t2 - t1)/1000;
	print("Time elapsed on the above file: " + elapsed_time + " seconds");
}

setBatchMode("exit and display");
Ext.CLIJ2_reportMemory();

// ============== Function Definitions ==================
function save_with_metadata_and_close(window_name, save_name, dimensions) {
	//Pull image from the GPU, add pixel dimension metadata, and save it
	//window_name: string referring to name of an object on GPU to be pulled
	//             back onto GPU
	//save_name: the path to save the image to
	//dimensions: 4-member array, (x,y,z, unit) in units of unit/pixel
	Ext.CLIJ2_pull(window_name);
	selectWindow(window_name);
	setVoxelSize(dimensions[0], dimensions[1], dimensions[2], dimensions[3]);
	save(save_name);
	close();
}

function newImageToGPU(title, type, width, height, depth) {
	//Initialize a dummy image to be pulled off GPU later. Make sure to delete the image 
	//after it is pushed to GPU to save ImageJ CPU memory
	//Parameters are the same as macro function newImage
	newImage(title, type, width, height, depth);
	Ext.CLIJ2_push(title);
	selectWindow(title);
	close();
}

function surfCutGPUTwoPass(cut1, cut2, im_flipped, surfcut, max_z, mask) {
	selectWindow(input);
	getDimensions(width, height, channels, slices, frames);
	//Initialize some objects to hold intermediate steps on the GPU
	Ext.CLIJ2_create3D(im_as_float,         width, height, slices,       32);
	Ext.CLIJ2_create3D(tmp_float,           width, height, slices,       32);
	Ext.CLIJ2_create3D(im_median,           width, height, slices,       16);
	Ext.CLIJ2_create3D(binary,              width, height, slices,        8);
	Ext.CLIJ2_create2D(edge_slice,          width, height,                8);
	Ext.CLIJ2_create3D(edge_stack_regular,  width, height, slices,        8);
	Ext.CLIJ2_create3D(edge_stack_log,      width, height, slices,        8);
	Ext.CLIJ2_create3D(edge_padding,        width, height, cut2,          8);
	Ext.CLIJ2_create3D(master_mask,         width, height, cut2 + slices, 8);
	Ext.CLIJ2_create3D(im_masked_1,         width, height, slices,       16);
	Ext.CLIJ2_create3D(im_masked_2,         width, height, slices,       16);
	Ext.CLIJ2_create3D(im_surface,          width, height, slices,       16);

	//Initialize blank images for surfcut (log and regular), and max_z
	surfcut_regular = "surfcut_regular";
	surfcut_log = "surfcut_log";
	newImageToGPU(surfcut_regular, "16-bit black", width, height, 1);
	newImageToGPU(surfcut_log, "16-bit black", width, height, 1);
	newImageToGPU(max_z, "16-bit black", width, height, 1);
	
	//Get the regular maximum intensity projection done up front
	//(so these arrays can be re-used later)
	Ext.CLIJ2_maximumZProjection(im_flipped, max_z);
	//Apply a median filter to the flipped image to remove hot pixels
	Ext.CLIJ2_median3DBox(im_flipped, im_median, 1.5, 1.5, 1.5);
	//Get the surface (no logarithm applied prior to thresholding)
	getSurface(cut1, cut2, im_median, im_as_float, tmp_float, binary, edge_slice, edge_stack_regular, //
               edge_padding, master_mask, im_masked_1, im_masked_2, im_surface, 0);
   	//Project the surface to get the regular surfcut projection
   	Ext.CLIJ2_maximumZProjection(im_surface, surfcut_regular);
   	//Re-run the surface finding algorithm with the logarithmic transformation
   	getSurface(cut1, cut2, im_median, im_as_float, tmp_float, binary, edge_slice, edge_stack_log, //
               edge_padding, master_mask, im_masked_1, im_masked_2, im_surface, 1);
    	Ext.CLIJ2_maximumZProjection(im_surface, surfcut_log);
    
   	 //Release some arrays that are no longer needed
    	Ext.CLIJ2_release(input);
    	Ext.CLIJ2_release(im_flipped);
	Ext.CLIJ2_release(im_as_float);
	Ext.CLIJ2_release(tmp_float);
	Ext.CLIJ2_release(im_median);
	Ext.CLIJ2_release(binary);
	Ext.CLIJ2_release(edge_slice);
	Ext.CLIJ2_release(edge_padding);
	Ext.CLIJ2_release(master_mask);
	Ext.CLIJ2_release(im_masked_1);
	Ext.CLIJ2_release(im_masked_2);
	Ext.CLIJ2_release(im_surface);
	
	//-------Part 2: combine the two surfcut versions and get a mask that reflects
	//-------this combination
	//Create some new arrays for the next part
	Ext.CLIJ2_create3D(surfcut_stack,       width, height, 2,            16);
	Ext.CLIJ2_create3D(argmax_stack,        width, height, slices,        8);
	Ext.CLIJ2_create2D(argmax_slice,        width, height,                8);
	Ext.CLIJ2_create3D(edge_regular_masked, width, height, slices,        8);
	Ext.CLIJ2_create3D(edge_log_masked, width, height, slices,            8);
	
	newImageToGPU(surfcut, "16-bit black", width, height, 1);
	newImageToGPU(mask, "8-bit black", width, height, slices);
	//Concatenate the two surfcut images into a stack (to be projected)
	Ext.CLIJ2_copySlice(surfcut_regular, surfcut_stack, 0);
	Ext.CLIJ2_copySlice(surfcut_log, surfcut_stack, 1);
	//Combine the two surfcut results with the maximum method. For each xy pixel store which
	//image it came from (0 - regular or 1 - log) in argmax_slice
	Ext.CLIJ2_argMaximumZProjection(surfcut_stack, surfcut, argmax_slice);
	//Copy this argmax slice into a stack that matches the size of the edge mask
	Ext.CLIJ2_imageToStack(argmax_slice, argmax_stack, slices);
	//Mask the regular edge mask to only show the parts of the edge stack that correspond to the
	//regular surfcut image from above
	Ext.CLIJ2_maskLabel(edge_stack_regular, argmax_stack, edge_regular_masked, 0);
	//Do the same to get the portions of the log edge stack that correspond to the log
	//part of the surfcut image
	Ext.CLIJ2_maskLabel(edge_stack_log, argmax_stack, edge_log_masked, 1);
	//Combine the two masked edge stacks to obtain a final mask that combines the portions
	//of the regular and edge stacks, so that the final mask corresponds to the final
	//surfcut image (to get an accurate surface).
	Ext.CLIJ2_addImages(edge_regular_masked, edge_log_masked, mask);
	//Clear the remaining variables from the GPU
	Ext.CLIJ2_release(surfcut_regular);
	Ext.CLIJ2_release(surfcut_log);
	Ext.CLIJ2_release(edge_stack_regular);
	Ext.CLIJ2_release(edge_stack_log);
	Ext.CLIJ2_release(surfcut_stack); 
	Ext.CLIJ2_release(argmax_stack);
	Ext.CLIJ2_release(argmax_slice);
	Ext.CLIJ2_release(edge_regular_masked);
	Ext.CLIJ2_release(edge_log_masked);
}

function getSurface(cut1, cut2, im_median, im_as_float, tmp_float, binary, edge_slice, edge_stack, //
                    edge_padding, mask, im_masked_1, im_masked_2, im_surface, do_logarithm) {
	//Run SurfCut on the GPU. Right now most parameters are hard-coded, willl change later.
	//SurfCut algorithm detects the surface of a 3D structure in a stack. When that surface is
	//detected, it will project between cut1 slices below the stack and cut1+cut2 slices below
	//the stack. cut1 and cut2 must be non-negative integers. Make sure to consider the actual
	//z-spacing of your stack when choosing values for cut1 and cut2! The original CPU surfcut
	//algorithm may be useful to try first to get a general impression of how it should work.
	//initialize images is a flag to tell if the images should be s
	Ext.CLIJ2_getDimensions(im_median, width, height, slices);
	//Create a 32-bit copy for making the binary mask
	Ext.CLIJ2_convertFloat(im_median, im_as_float);
	//Blur the image (low-pass with sigma=21px) and remove vignetting at the corners (high-pass sigma=300)
	Ext.CLIJ2_differenceOfGaussian3D(im_as_float, tmp_float, 21, 21, 2, 300, 300, 1);
	//Make sure the minimum value in the array is 1 after the DoG operation
	Ext.CLIJ2_getMinimumOfAllPixels(tmp_float, min_val);
	Ext.CLIJ2_addImageAndScalar(tmp_float, im_as_float, 1 - min_val);
	//Logarithm can help compress the foreground intensities for thresholding
	//(this step may be omitted or made optional for a slightly different segmentation)
	if (do_logarithm == 1){
		Ext.CLIJ2_logarithm(im_as_float, tmp_float);
		Ext.CLIJ2_copy(tmp_float, im_as_float); // needed in case logarithm is not applied
	}
	//Apply Otsu threshold to the filtered image
	Ext.CLIJ2_automaticThreshold(im_as_float, binary, "Otsu");
	//Iteratively max-z project to create an "edge mask" showing where the edge of any foreground
	//is in the current slice
	for (i = 0; i < slices; i++) {
		Ext.CLIJ2_maximumZProjectionBounded(binary, edge_slice, 0, i);
		Ext.CLIJ2_copySlice(edge_slice, edge_stack, i);
	}
	//Concatenate the edge stack with cut2 slices of solid white (255) to make the mask
	Ext.CLIJ2_set(edge_padding, 255);
	Ext.CLIJ2_concatenateStacks(edge_padding, edge_stack, mask);
	//Crop a substack of the mask starting at the first slice (one of the padded slices)
	Ext.CLIJ2_crop3D(mask, edge_stack, 0, 0, 0, width, height, slices);
	//Mask the original image with this mask
	Ext.CLIJ2_mask(im_flipped, edge_stack, im_masked_1);
	//Now make a second substack of the mask starting at cut2 + 1 - cut1 going to end-cut1
	//and create a second masked copy of the original image
	Ext.CLIJ2_crop3D(mask, edge_stack, 0, 0, cut2 + 1 - cut1, width, height, slices);
	Ext.CLIJ2_mask(im_flipped, edge_stack, im_masked_2);
	//Subtract the first masked image from the second
	Ext.CLIJ2_subtractImages(im_masked_2, im_masked_1, im_surface);
}
