%Specify the microns per pixel (same as 40x)
UM_PER_PIXEL = 0.3394;
%Initialize the random number generator so that the results are repeatable
rng(0, 'twister'); %to generate different results each time change 0 to the system time
% Specify the size of the image in pixels (choose 800x800 to approximate the size of a
%tailfin
imageSizeX = 800;
imageSizeY = 800;
% Specify the number and radius of different sized circles. Radii are in
% pixels
% XS is <1�m^2 (choose 0.4�m^2)
% Small is 1-5�m^2 (choose 3 pixel radius ~3.25�m^2)
% Medium is 5-10�m^2 (choose 4 pixel radius ~5.79�m^2)
% Large is >10�m^2 (choose 6 pixel radius ~13�m^2)
xsAreaMicronSquared = 0.4;
smallAreaMicronSquared = 3;
mediumAreaMicronSquared = 6;
largeAreaMicronSquared = 13;
objInfo = struct('name',[],'n',[],'r',[],'coordsX',[],'coordsY',[],'strel',[],'img',[]);
objInfo(1).name = 'x-small';
objInfo(1).n = 100;
objInfo(1).r = round(sqrt(xsAreaMicronSquared / pi) / UM_PER_PIXEL);

objInfo(2).name = 'small';
objInfo(2).n = 70;
objInfo(2).r = round(sqrt(smallAreaMicronSquared / pi) / UM_PER_PIXEL);

objInfo(3).name = 'medium';
objInfo(3).n = 30;
objInfo(3).r = round(sqrt(mediumAreaMicronSquared / pi) / UM_PER_PIXEL);

objInfo(4).name = 'large';
objInfo(4).n = 10;
objInfo(4).r = round(sqrt(largeAreaMicronSquared / pi) / UM_PER_PIXEL);

% Specify the structuring elements used to create the different sized
% objects
objInfo(1).strel = strel('disk', objInfo(1).r, 0);
objInfo(2).strel = strel('disk', objInfo(2).r, 0);
objInfo(3).strel = strel('disk', objInfo(3).r, 0);
objInfo(4).strel = strel('disk', objInfo(4).r, 0);
% To make a ring-like strel instead, use xor on two structuring elements
objInfo(4).strel = makeRingNhood(objInfo(4).r);

% Make 4 binary images for each size object. The center of each object is
% specified by a single 1, the rest of the image is 0s. Then we'll use
% morphological operations to expand each individual pixel into circles.
for k = 1:numel(objInfo)
    objInfo(k).coordsX = randi(imageSizeX, objInfo(k).n, 1);
    objInfo(k).coordsY = randi(imageSizeY, objInfo(k).n, 1);
    img = false([imageSizeY, imageSizeX]);
    for m = 1:objInfo(k).n
        img(objInfo(k).coordsY(m),objInfo(k).coordsX(m)) = true;
        %Use morphological dilation to convert single pixels to the shape
        %of the structuring element
        objInfo(k).img = imdilate(img, objInfo(k).strel);
    end
end

% Combine all the images into a single image:
testImage = false(imageSizeY, imageSizeX);
for k = 1:numel(objInfo)
    testImage = testImage | objInfo(k).img;
end
figure,imshow(testImage);
save('simulated_image_and_data_fewerObjects.mat','testImage','objInfo');