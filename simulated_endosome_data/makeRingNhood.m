function nhood = makeRingNhood(r)
%Make a ring neighborhood with outer radius r and thickness 1 to use to
%define a structuring element

s1 = strel('disk', r, 0);
s2 = strel('disk', r - 1, 0);
nhood = xor(s1.Neighborhood, padarray(s2.Neighborhood,[1,1],'both'));
end