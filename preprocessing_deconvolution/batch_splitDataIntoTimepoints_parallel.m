%%% This script is used to preprocess raw data (background subtract and
%%% flatfield correct) and split XYZCT datasets into individual XYZC
%%% files for each timepoint; this is needed for later deconvolution in
%%% Huygens. This version of the code uses PARALLEL PROCESSING to speed up
%%% this conversion; a separate script that does not use parallel for-loops
%%% is also within this folder.
%%% This code is also designed to retain as much metadata as possible. Some
%%% of this code is a bit finicky, and depends on compatibility between
%%% Bio-Formats and MATLAB.
%%%
%%% Input: 
%%% - background and flatfield images (if performing these corrections)
%%%   Should be XY(C), 32-bit
%%% - list of folders of raw data (big OME.TIFs from MicroManager). Should
%%%   be XYZ(C)T, 16-bit
%%% - The other parameters specified (and explained below)
%%% Output:
%%% - folders of corrected data, split into separate files for each
%%%   timepoint, with dimensions XYZ(C). The folder will be in the same
%%%   location as the raw data, with the suffix '_split' in the foldername.
%%% 
%%% --------- PARAMETERS -----------------
%%% --- Check this out before running! ---
%%% Set these flags if you want to do background correction or flatfield
%%% subtraction. Note that flatfield correction requires background
%%% subtraction first!
DO_BACKGROUND_CORRECT = 0;
DO_FLATFIELD_CORRECT = 0;
%%% Background Subtraction Parameters; these only matter if
%%%     DO_BACKGROUND_CORRECT is set to 1
%%%
%%%     Background image should have the same channel arrangement that you want to
%%%     END UP with, i.e. if your raw data has 4 channels and you only want
%%%     channels 1 and 3, then background and flatfield images should be
%%%     two-channel images with channel 1 corresponding to raw channel 1 and
%%%     channel3 2 corresponding to raw channel 3.
backgroundFile = ['/Users/andrewkennard/Analysis/2020-02-08_3dpf_Np63Gal4_UASmCherry/',...
                   '20200208_561_BackgroundSliceBySlice_32bit.tif'];
%%% Flatfield Correction Parameters (only relevant if DO_FLATFIELD_CORRECT
%%%     set to 1)
%%% 
%%%     Flatfield image should have the same dimensions/organization as
%%%     your background image
flatfieldFile = ['/Users/andrewkennard/Analysis/2020-09-20_3dpf_Np63Gal4UASmCherry_cdh1-cdh1sfGFP/',...
                  '20200920_40x_Flat_561_45mW_80msExp_620-60em_EMGain200.tif'];
%%%     MULT_FACTOR: Set the constant values to multiply the corrected 
%%%     images by so that the initial range is approximately preserved 
%%%     (with minimal clipping). Specify an array if correcting multiple
%%%     channels, e.g.
%MULT_FACTOR = [8500, 20000]; %Ecadherin-sfGFP, mCherry
MULT_FACTOR = 20000;

%%% Other parameters
%%%
%%% channelsToUse: Specify a list of channel indexes to process. The indexes are 1-indexed.
%%%     E.g. a 4 channel image where you want to look at the first and 3rd channes
%%%     would be channelsTouse = [1,3]. If this line is commented out all channels
%%%     will be processed by default. NOTE: the flat
channelsToUse = 1;
%%% nParWorkers: set the number of parallel workers (based on thue nubmer
%%%     of processors your computer has
nParWorkers = 6;
%%% Define the folders to work on
folderList = {['/Users/enorby/Thesis/to-split/',...
               '20211026_p63-gal4_UAS-2x-FYVE_UAS-mCh_15minpreincubate-Blebb_3dpf_laceration_after_1/',...
               '20211026_p63-gal4_UAS-2x-FYVE_UAS-mCh_15minpreincubate-Blebb_3dpf_laceration_after_1_MMStack_Pos0.ome.tif']...
               };
%%% Specify which of these folders to run. Sometimes you screw up and only
%%%      want to run a few of the folders you initially specified. Rather
%%%      than change the list, you can change this to a list of the indexes
%%%      of the folders you do want to run. The default would be
%%%      1:numel(folderList)
foldersToUse = 1:numel(folderList);
%%% ------------ END PARAMETERS ------------------

%Set up a parallel pool for parallel processing
p = gcp('nocreate');
if isempty(p)
    %parpool(nParWorkers);
    %If there are issues with recognizing bioformats objects, this
    %alternative initialization of the parpool may be needed
    parpool(nParWorkers, 'AttachedFiles', {'/Users/enorby/Thesis/Analysis/OthersFunctions/bfmatlab/bioformats_package.jar'});
end

dimOrder = 'XYZCT';
%Load background image
if DO_BACKGROUND_CORRECT
    backgroundReader = bfGetReader(backgroundFile);
    sizeX = backgroundReader.getSizeX();
    sizeY = backgroundReader.getSizeY();
    sizeC = backgroundReader.getSizeC();
    backgroundImage = zeros(sizeY, sizeX, sizeC, 'single');
    for iC = 1:sizeC
        backgroundImage(:,:,iC) = bf_getFrame(backgroundReader, 1, iC, 1);
    end
    backgroundReader.close();
end

%Load flatfield image
if DO_FLATFIELD_CORRECT
    assert(DO_BACKGROUND_CORRECT, "background correction must be enabled to do flatfield correction");

    flatfieldReader = bfGetReader(flatfieldFile);
    sizeX = flatfield.getSizeX();
    sizeY = backgroundReader.getSizeY();
    sizeC = backgroundReader.getSizeC();
    flatfieldImage = zeros(sizeY, sizeX, sizeC, 'single');
    for iC = 1:sizeC
        flatfieldImage(:,:,iC) = bf_getFrame(flatfieldReader, 1, iC, 1);
    end
    flatfieldReader.close();
end

if isempty(channelsToUse)
    useAllChannels = 1;
else
    useAllChannels = 0;
end

for f = foldersToUse %adjust to the number of datasets you want to process
    %Get metadata about these files
    %To save memory, open files with this Memoizer reader, so they can be
    %closed later
    r = loci.formats.Memoizer(bfGetReader(), 0);
    r.setId(folderList{f});
    nSeries = r.getSeriesCount();
    sizeT = r.getSizeT();
    sizeX = r.getSizeX();
    sizeY = r.getSizeY();
    sizeZ = r.getSizeZ();
    if useAllChannels
        sizeC = r.getSizeC();
    else
        sizeC = numel(channelsToUse);
    end
    originalMeta = r.getMetadataStore();
    pixelSizeX = originalMeta.getPixelsPhysicalSizeX(0).value();
    pixelSizeY = originalMeta.getPixelsPhysicalSizeY(0).value();
    pixelSizeZ = originalMeta.getPixelsPhysicalSizeZ(0).value();
    r.close();
    
    for position = 0:nSeries-1 %CAREFUL 0 indexed !!!
        %Create a new directory to store the split timepoints
        [oldDir, oldName, ~] = fileparts(folderList{f});
        suffix = sprintf('_Pos%d_split', position);
        newDir = [oldDir suffix];
        if ~exist(newDir,'dir')
            mkdir(newDir);
        end
        tic
        thisFolder = folderList{f};
        oldNamePrefix = sprintf('%s_Pos%d',oldName(1:end-17),position);%Remove the _MMStack_Pos0.ome portion of the filename
        parfor iT = 1 : sizeT
            %If there are issues with not recognizing bioformats objects, these
            %two lines may be needed along with the different initialization of
            %the parpool above
            folder = getAttachedFilesFolder;
            javaaddpath(folder);
            bfInitLogging('INFO');
            r2 = javaObject('loci.formats.Memoizer', bfGetReader(), 0);
            r2.setId(thisFolder);
            r2.setSeries(position);
            inStack = zeros(sizeY, sizeX, sizeZ, 'single');
            outStack = zeros(sizeY, sizeX, sizeZ, sizeC, 1, 'uint16');
            metadata = createMinimalOMEXMLMetadata(outStack);
            %Based on version of MATLAB/Bioformats, one of these lines
            %specifying dOrder and um may work and the other might not. If you
            %get errors about these objects, try changing which line is
            %commented out.
            dOrder = ome.xml.model.enums.DimensionOrder.fromString(dimOrder);
            %dOrder = javaObject('ome.xml.model.enums.DimensionOrder.fromString',dimOrder);
            um = ome.units.UNITS.MICROMETER;
            %um = javaObject('ome.units.UNITS.MICROMETER');
            psX = javaObject('ome.units.quantity.Length', pixelSizeX, um);
            psY = javaObject('ome.units.quantity.Length', pixelSizeY, um);
            psZ = javaObject('ome.units.quantity.Length', pixelSizeZ, um);

            metadata.setPixelsPhysicalSizeX(psX, 0);
            metadata.setPixelsPhysicalSizeY(psY, 0);
            metadata.setPixelsPhysicalSizeZ(psZ, 0);
            metadata.setPixelsDimensionOrder(dOrder, 0);
            for iC = 1:sizeC
                if useAllChannels
                    channelIndex = iC;
                else
                    channelIndex = channelsToUse(iC);
                end

                for iZ = 1:sizeZ
                    inStack(:,:,iZ) = single(bf_getFrame(r2, iZ, channelIndex, iT));
                end
                %Subtract background and flatfield correct. The MULT_FACTOR is
                %so that data can be converted back to uint16 with comparable
                %dynamic range. Must be the same number for all datasets!

                %{
                if DO_BACKGROUND_CORRECT
                    inStack = inStack - backgroundImage(:,:,iC); % should be broadcast to 3D
                    if DO_FLATFIELD_CORRECT %only happens if background correcting also
                        inStack = MULT_FACTOR(iC) .* (inStack ./ flatfieldImage(:,:,iC));
                    end
                end
                %}

                outStack(:,:,:,iC,1) = uint16(inStack);
            end

            outName = sprintf('%s_t%03d.ome.tif',oldNamePrefix, iT);
            outPath = fullfile(newDir, outName);
            bfsave(outStack, outPath, 'metadata', metadata);
            r2.close();
        end
        toc
    end
    
end